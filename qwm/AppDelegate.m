//
//  AppDelegate.m
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import "AppDelegate.h"
#import "QWMainTabBarController.h"
#import <IQKeyboardManager/IQKeyboardManager.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self initRootVC];//初始化页面
    [self addReachabilityNotification];//网络监控
    [self addIQKeyboard]; //调节键盘
    
    return YES;
}

#pragma mark- 进入前台
- (void)applicationDidBecomeActive:(UIApplication *)application {
    if ([AppProfile GetUserInfo]) {//每次返回前台获取用户信息
        AppProfile.qaUserInfo =[AppProfile GetUserInfo];
    }
}

#pragma mark- 初始化页面
- (void)initRootVC{
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = UIColor.whiteColor;
    QWMainTabBarController *tabBarVc = [[QWMainTabBarController alloc] init];
//    QWNavgationController *navgationController = [[QWNavgationController alloc] initWithRootViewController:tabBarVc];
    self.window.rootViewController = tabBarVc;
    [self.window makeKeyAndVisible];
}

#pragma mark- 网络监听
-(void)addReachabilityNotification{
//    // 监测网络情况
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(reachabilityChanged:)
//                                                 name: kReachabilityChangedNotification
//                                               object: nil];
    Reachability  *hostReach = [Reachability reachabilityWithHostName:@"www.apple.com"];//可以以多种形式初始化
    [hostReach startNotifier];//开始监听,会启动一个run loop
}

//- (void)reachabilityChanged:(NSNotification *)note {
//    // 连接改变
//    Reachability* curReach = [note object];
//    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
//    //处理连接改变后的情况
//    NetworkStatus status = [curReach currentReachabilityStatus];
//
//    if (status != NotReachable) {
//
//    }
//}

#pragma mark - 键盘库
- (void)addIQKeyboard{
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager]; // 获取类库的单例变量
    keyboardManager.enable = YES; // 控制整个功能是否启用
    keyboardManager.shouldResignOnTouchOutside = YES; // 控制点击背景是否收起键盘
    keyboardManager.shouldToolbarUsesTextFieldTintColor = NO; // 控制键盘上的工具条文字颜色是否用户自定义
    keyboardManager.toolbarManageBehaviour = IQAutoToolbarBySubviews; // 有多个输入框时，可以通过点击Toolbar 上的“前一个”“后一个”按钮来实现移动到不同的输入框
    keyboardManager.enableAutoToolbar = NO; // 控制是否显示键盘上的工具条
    keyboardManager.shouldShowToolbarPlaceholder = NO; // 是否显示占位文字
    keyboardManager.placeholderFont = [UIFont boldSystemFontOfSize:17]; // 设置占位文字的字体
    keyboardManager.keyboardDistanceFromTextField = 10.0f; // 输入框距离键盘的距离
       //修改工具条字体颜色
    [IQKeyboardManager sharedManager].shouldToolbarUsesTextFieldTintColor = NO;
    [IQKeyboardManager sharedManager].toolbarTintColor = [UIColor grayColor];
}


@end
