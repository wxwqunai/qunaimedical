//
//  QWHomeModel.h
//  qwm
//
//  Created by kevin on 2023/4/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWHomeModel : NSObject

@end

@interface QWOpenqaModel : NSObject
@property (nonatomic, assign) BOOL isShowNoticeDialog; // 总控制-是否展示
@property (nonatomic, copy) NSString *type; //1.悬浮按钮，2.大图
@property (nonatomic, copy) NSString *url; //跳转url
@property (nonatomic, copy) NSString *noticePicUrl; //公告图片
@property (nonatomic, assign) BOOL isCheckCode;
@property (nonatomic, copy) NSString *checkCode;
@end

@interface QABannerModel : NSObject
@property (copy, nonatomic) NSString* title;
@property (strong, nonatomic) NSString* image;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *vText;
@end

@interface QANewsModel : NSObject
@property (copy, nonatomic) NSString* uniquekey;
@property (copy, nonatomic) NSString* title;
@property (copy, nonatomic) NSString* date;
@property (copy, nonatomic) NSString* category;
@property (copy, nonatomic) NSString* author_name;
@property (copy, nonatomic) NSString* url;
@property (copy, nonatomic) NSString* thumbnail_pic_s;
@property (copy, nonatomic) NSString* thumbnail_pic_s02;
@property (copy, nonatomic) NSString* thumbnail_pic_s03;
@property (copy, nonatomic) NSString* is_content;
@property (copy, nonatomic) NSString* vText;
@end

NS_ASSUME_NONNULL_END
