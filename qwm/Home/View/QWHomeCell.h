//
//  QWHomeCell.h
//  qwm
//
//  Created by kevin on 2023/4/14.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWHomeCell : QWBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@end

NS_ASSUME_NONNULL_END
