//
//  QWHome2Cell.h
//  qwm
//
//  Created by kevin on 2023/4/14.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWHome2Cell : QWBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

NS_ASSUME_NONNULL_END
