//
//  QWHomeNewsCell.m
//  qwm
//
//  Created by kevin on 2023/4/18.
//

#import "QWHomeNewsCell.h"
@interface QWHomeNewsCell ()
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UIImageView *lineImageView;
@property (nonatomic, strong) UIView *centerView;

@property (nonatomic, strong) UIView *mensView;
@property (nonatomic, strong) UILabel *autherLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIButton *plButton;
@property (nonatomic, strong) UIButton *shareButton;
@property (nonatomic, strong) UIButton *tlButton;
@end

@implementation QWHomeNewsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.top.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView).offset(-10);
        }];
        
        [self topView];
        [self centerView];
        [self lineImageView];
        [self mensView];
    }
    return _bgView;
}



#pragma mark - 基本信息-view
- (UIView *)topView{
    if(!_topView){
        _topView = [[UIView alloc]init];
        _topView.backgroundColor = [UIColor clearColor];
        [_bgView addSubview:_topView];
        
        [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(14);
            make.right.equalTo(_bgView).offset(-14);
            make.top.equalTo(_bgView);
        }];
        
        [self nameLabel];
        [self picImageView];
    }
    return _topView;
}

#pragma mark - name
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.numberOfLines = 2;
        _nameLabel.font = [UIFont systemFontOfSize:20];
        _nameLabel.textColor = [UIColor blackColor];
        [_topView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_topView);
            make.right.equalTo(_topView);
            make.top.equalTo(_topView).offset(5);
            make.height.mas_greaterThanOrEqualTo(40.0);
        }];
    }
    return _nameLabel;
}

#pragma mark - image
- (UIImageView *)picImageView{
    if(!_picImageView){
        _picImageView = [[UIImageView alloc]init];
        _picImageView.backgroundColor = [UIColor clearColor];
        [_topView addSubview:_picImageView];
        
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_nameLabel.mas_bottom).offset(5);
            make.left.equalTo(_topView);
            make.right.equalTo(_topView);
//            make.height.mas_equalTo(_topView.mas_width).multipliedBy(9.0/16.0);
            make.bottom.equalTo(_topView);
        }];
    }
    return _picImageView;
}





#pragma mark - 作者/时间-view
- (UIView *)centerView{
    if(!_centerView){
        _centerView = [[UIView alloc]init];
        _centerView.backgroundColor = [UIColor clearColor];
        [_bgView addSubview:_centerView];
        
        [_centerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(14);
            make.right.equalTo(_bgView).offset(-14);
            make.top.equalTo(_topView.mas_bottom);
            make.height.mas_equalTo(30);
        }];
        [self autherLabel];
        [self timeLabel];
    }
    return _centerView;
}
//时间
- (UILabel *)timeLabel{
    if(!_timeLabel){
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.numberOfLines = 1;
        _timeLabel.font = [UIFont systemFontOfSize:14];
        _timeLabel.textColor = [UIColor colorFromHexString:@"#515151"];
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [_centerView addSubview:_timeLabel];
        
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_centerView);
            make.left.equalTo(_autherLabel.mas_right).offset(5);
            make.top.equalTo(_centerView);
            make.bottom.equalTo(_centerView);
            make.width.mas_equalTo(90);
        }];
    }
    return _timeLabel;
}
//作者
- (UILabel *)autherLabel{
    if(!_autherLabel){
        _autherLabel = [[UILabel alloc]init];
        _autherLabel.numberOfLines = 1;
        _autherLabel.font = [UIFont systemFontOfSize:14];
        _autherLabel.textColor = [UIColor colorFromHexString:@"#515151"];
        [_centerView addSubview:_autherLabel];
        
        [_autherLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_centerView);
            make.top.equalTo(_centerView);
            make.bottom.equalTo(_centerView);
        }];
    }
    return _autherLabel;
}





#pragma mark - lineImage
- (UIImageView *)lineImageView{
    if(!_lineImageView){
        _lineImageView = [[UIImageView alloc]init];
        _lineImageView.backgroundColor = Color_Line_Gray;
        [_bgView addSubview:_lineImageView];
        
        [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(14.0);
            make.right.mas_equalTo(-14.0);
            make.height.mas_equalTo(1);
            make.top.equalTo(_centerView.mas_bottom).offset(5);
        }];
    }
    return _lineImageView;
}





#pragma mark - 按钮-view
- (UIView *)mensView{
    if(!_mensView){
        _mensView = [[UIView alloc]init];
        _mensView.backgroundColor = [UIColor clearColor];
        [_bgView addSubview:_mensView];
        
        [_mensView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView);
            make.top.equalTo(_lineImageView.mas_bottom);
            make.bottom.equalTo(_bgView);
            make.height.mas_equalTo(35);
        }];
        
        [self plButton];
        [self shareButton];
        [self tlButton];
        
        
        
//        NSMutableArray *masonryViewArray = [NSMutableArray array];
//        [masonryViewArray addObject:[self plButton]];
//        [masonryViewArray addObject:[self tlButton]];
//        [masonryViewArray addObject:[self gzButton]];
//
//        // 实现masonry水平固定间隔方法
//        [masonryViewArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
//        // 设置array的垂直方向的约束
//        [masonryViewArray mas_makeConstraints:^(MASConstraintMaker *make) {
//               make.top.mas_equalTo(0);
//               make.height.mas_equalTo(35);
//        }];
    }
    return _mensView;
}

#pragma mark - 按钮-评论
- (UIButton *)plButton{
    if(!_plButton){
        _plButton =[UIButton buttonWithType:UIButtonTypeCustom];
//        [_plButton setTitle:@" 评论" forState:UIControlStateNormal];
        [_plButton setImage:[UIImage imageNamed:@"play_voice_news.png"] forState:UIControlStateNormal];

//        [_plButton setTitleColor:[UIColor colorFromHexString:@"#515151"] forState:UIControlStateNormal];
//        _plButton.titleLabel.font =[UIFont systemFontOfSize:14];
        [_plButton addTarget:self action:@selector(plButtoClick:) forControlEvents:UIControlEventTouchUpInside];
        [_mensView addSubview:_plButton];

        [_plButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_mensView);
            make.top.equalTo(_mensView);
            make.bottom.equalTo(_mensView);
            make.width.equalTo(_mensView.mas_width).multipliedBy(1.0/3.0);

        }];
    }
    return _plButton;
}
-(void)plButtoClick:(UIButton *)sender{
//    NSLog(@"动画语音播报");
    if(self.voicePlayBlock)self.voicePlayBlock();
}
#pragma mark - 按钮-讨论
- (UIButton *)tlButton{
    if(!_tlButton){
        _tlButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_tlButton setTitle:@" 讨论" forState:UIControlStateNormal];
        [_tlButton setImage:[UIImage imageNamed:@"c_taolun.png"] forState:UIControlStateNormal];
        
        [_tlButton setTitleColor:[UIColor colorFromHexString:@"#515151"] forState:UIControlStateNormal];
        _tlButton.titleLabel.font =[UIFont systemFontOfSize:14];
        [_tlButton addTarget:self action:@selector(tlButtoClick:) forControlEvents:UIControlEventTouchUpInside];
        [_mensView addSubview:_tlButton];
        
        [_tlButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_mensView);
            make.top.equalTo(_mensView);
            make.bottom.equalTo(_mensView);
            make.width.equalTo(_mensView.mas_width).multipliedBy(1.0/3.0);

        }];
        
    }
    return _tlButton;
}
-(void)tlButtoClick:(UIButton *)sender{
    NSLog(@"讨论");
    if(self.pinglunBlock)self.pinglunBlock();
}

#pragma mark - 按钮-关注
- (UIButton *)shareButton{
    if(!_shareButton){
        _shareButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_shareButton setTitle:@" 分享" forState:UIControlStateNormal];
        [_shareButton setImage:[UIImage imageNamed:@"c_share.png"] forState:UIControlStateNormal];
        
        [_shareButton setTitleColor:[UIColor colorFromHexString:@"#515151"] forState:UIControlStateNormal];
        _shareButton.titleLabel.font =[UIFont systemFontOfSize:14];
        [_shareButton addTarget:self action:@selector(shareButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_mensView addSubview:_shareButton];
        
        [_shareButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_mensView);
            make.top.equalTo(_mensView);
            make.bottom.equalTo(_mensView);
            make.width.equalTo(_mensView.mas_width).multipliedBy(1.0/3.0);

        }];
    }
    return _shareButton;
}
-(void)shareButtonClick:(UIButton *)sender{
    NSLog(@"分享");
    if(self.shareBlock)self.shareBlock();
}



#pragma mark - 数据处理
-(void)setNameStr:(NSString *)nameStr{
    _nameStr = nameStr;
    _nameLabel.text = _nameStr;
}

-(void)setPicStr:(NSString *)picStr{
    _picStr = picStr;
    [self.picImageView sd_setImageWithURL:[NSURL URLWithString:_picStr]];
}

- (void)setTimeStr:(NSString *)timeStr{
    if([timeStr containsString:@" "]){
        NSArray *arr = [timeStr componentsSeparatedByString:@" "];
        _timeStr = arr[0];
    }else {
        _timeStr = timeStr;
    }
    _timeLabel.text = _timeStr;
}
- (void)setAutherStr:(NSString *)autherStr{
    _autherStr = autherStr;
    _autherLabel.text = autherStr;
}

@end
