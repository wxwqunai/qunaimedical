//
//  QWHomeNewsCell.h
//  qwm
//
//  Created by kevin on 2023/4/18.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^VoicePlayBlock)(void);
typedef void (^PinglunBlock)(void);
typedef void (^ShareBlock)(void);

@interface QWHomeNewsCell : QWBaseTableViewCell
@property (nonatomic ,copy) NSString *nameStr;
@property (nonatomic,copy) NSString *picStr;
@property (nonatomic,copy) NSString *timeStr;
@property (nonatomic,copy) NSString *autherStr;

@property (nonatomic,copy) VoicePlayBlock voicePlayBlock;
@property (nonatomic,copy) PinglunBlock pinglunBlock;
@property (nonatomic,copy) ShareBlock shareBlock;

@end

NS_ASSUME_NONNULL_END
