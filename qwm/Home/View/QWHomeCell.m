//
//  QWHomeCell.m
//  qwm
//
//  Created by kevin on 2023/4/14.
//

#import "QWHomeCell.h"

@implementation QWHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
