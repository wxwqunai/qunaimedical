//
//  QWMenusCell.m
//  qwm
//
//  Created by kevin on 2023/4/17.
//

#import "QWMenusCell.h"

@interface QWMenusCell()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIButton *creatButton;
@property (nonatomic, strong) UIButton *comListButton;
@property (nonatomic, strong) UIButton *myComButton;
@end

@implementation QWMenusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSLog(@"initWithStyle");
        
        [self bgView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.top.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(100);
        }];
        
        [self creatButton];
    }
    return _bgView;
}

#pragma mark - 生成配文
- (UIButton *)creatButton{
    if(!_creatButton){
        _creatButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_creatButton setTitle:@"自动配文\n生成快评动画" forState:UIControlStateNormal];
        _creatButton.titleLabel.numberOfLines = 0;
        _creatButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_creatButton setTitleColor:[UIColor colorFromHexString:@"#515151"] forState:UIControlStateNormal];
        _creatButton.titleLabel.font =[UIFont systemFontOfSize:17];
        _creatButton.layer.borderWidth = 1.0;
        _creatButton.layer.borderColor = Color_Main_Green.CGColor;
        _creatButton.layer.cornerRadius = 6.0;
        _creatButton.layer.masksToBounds = YES;
        [_creatButton addTarget:self action:@selector(creatButtonClcik:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:_creatButton];
        
        [_creatButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(14);
            make.top.equalTo(self.bgView).offset(10);
            make.bottom.equalTo(self.bgView).offset(-10);
        }];
    }
    return _creatButton;
}
-(void)creatButtonClcik:(UIButton *)sender{
    NSLog(@"分享");
}



@end
