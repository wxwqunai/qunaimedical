//
//  QABannerView.m
//  assistant
//
//  Created by kevin on 2023/4/13.
//

#import "QABannerView.h"
#import "SMPageControl.h"
#import "QWHomeModel.h"

@interface QABannerView()<UIScrollViewDelegate>
@property (nonatomic,strong) UIScrollView *bannerScrollView;
@property (nonatomic,strong) NSMutableArray *showBannerImageArr;
@property (nonatomic,strong) NSTimer * timer;//定时器
@property (nonatomic,assign) NSInteger timerPage;
@property (strong, nonatomic) SMPageControl * pageControl;
@end

@implementation QABannerView

-(instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = Color_Main_Green;
        [self bannerScrollView];
        [self pageControl];
        _timerPage =0;//header重用的时候重新开始图片切换
    }
    return self;
}

#pragma mark- ScrollView
-(UIScrollView *)bannerScrollView
{
    if (!_bannerScrollView) {
        _bannerScrollView=[[UIScrollView alloc]init];
        _bannerScrollView.backgroundColor =[UIColor clearColor];
        _bannerScrollView.delegate =self;
        _bannerScrollView.pagingEnabled =YES;
        _bannerScrollView.showsVerticalScrollIndicator = NO;
        _bannerScrollView.showsHorizontalScrollIndicator = NO;
        [self addSubview:_bannerScrollView];
        [self sendSubviewToBack:_bannerScrollView];
        [_bannerScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
        
        //默认背景图
        UIImageView *imageView =[[UIImageView alloc]init];
        imageView.backgroundColor=[UIColor whiteColor];
        imageView.image=[UIImage imageNamed:@"WHeaderBack"];
        [self addSubview:imageView];
        [self sendSubviewToBack:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _bannerScrollView;
}

#pragma mark- 小圆圈
-(SMPageControl *)pageControl
{
    if (!_pageControl) {
        _pageControl =[[SMPageControl alloc]init];
        //[_pageControl setPageIndicatorImage:[UIImage imageNamed:@"WpageDot"]];
        //[_pageControl setCurrentPageIndicatorImage:[UIImage imageNamed:@"WpageCurrentDot"]];
        //_pageControl.indicatorMargin = 5.0f;
        //_pageControl.indicatorDiameter = 10.0f;
        _pageControl.currentPageIndicatorTintColor = Color_Main_Green;
        _pageControl.pageIndicatorTintColor = [UIColor whiteColor];
        [self addSubview:_pageControl];
        
        [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
            make.height.equalTo(@20);
        }];
    }
    return _pageControl;
}


#pragma mark - 数据
- (void)setShowArr:(NSArray *)showArr
{
    _showArr = showArr;
    if (_showArr.count==0) return ;
    
    if (_showArr.count==1) {
        [self.showBannerImageArr removeAllObjects];
        [self.showBannerImageArr addObjectsFromArray:_showArr];
        _pageControl.numberOfPages =-1;
        _bannerScrollView.scrollEnabled=NO;
        _pageControl.hidden =YES;
        [self stopTimer];
    }else
    {
        [self.showBannerImageArr removeAllObjects];
        [self.showBannerImageArr addObject:[_showArr lastObject]];
        
        [self.showBannerImageArr addObjectsFromArray:_showArr];
        [self.showBannerImageArr addObject:[_showArr firstObject]];
        
        _pageControl.numberOfPages =_showBannerImageArr.count-2;
        _bannerScrollView.scrollEnabled=YES;
        _pageControl.hidden =NO;
        [self beginTimer];
    }
    _bannerScrollView.contentSize =CGSizeMake(kScreenWidth*self.showBannerImageArr.count, 0);
    _pageControl.currentPage = 0;
    [self loadBannerImageScrollView];
}

-(void)loadBannerImageScrollView
{
    NSArray *arr =[_bannerScrollView subviews];
    for (UIView *view in arr) {
        [view removeFromSuperview];
    }
    
    if (self.showBannerImageArr.count) {
        UIView *lastView=nil;
        for (int i = 0; i<_showBannerImageArr.count; i++) {
            UIView *bgShadowView = [[UIView alloc]init];
            [_bannerScrollView addSubview:bgShadowView];
            
            [bgShadowView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(self.bannerScrollView.mas_width);
                make.top.equalTo(self.mas_top);
                make.height.equalTo(self.mas_height);
                
                if (lastView) {
                    make.left.equalTo(lastView.mas_right);
                } else {
                    make.left.equalTo(self.bannerScrollView);
                }
            }];
            lastView = bgShadowView;
            
            UIButton *imageButton =[UIButton buttonWithType:UIButtonTypeCustom];
            imageButton.backgroundColor=[UIColor whiteColor];
            QABannerModel *bannerModel =_showBannerImageArr[i];
//            [imageButton sd_setBackgroundImageWithURL:[NSURL URLWithString:bannerModel.image] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"WHeaderBack"]];
            [imageButton setBackgroundImage:[UIImage imageNamed:bannerModel.image] forState:UIControlStateNormal];
            imageButton.adjustsImageWhenHighlighted = NO;
            imageButton.tag=10000+i;
            [imageButton addTarget:self action:@selector(imageButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            [bgShadowView addSubview:imageButton];
            [imageButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(bgShadowView);
            }];
        }
        [_bannerScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
            //让scrollview的contentSize随着内容的增多而变化
            make.right.mas_equalTo(lastView.mas_right);
        }];
        _bannerScrollView.pagingEnabled=YES;
        _bannerScrollView.alwaysBounceHorizontal=YES;
        _bannerScrollView.contentSize = CGSizeMake(_showBannerImageArr.count*kScreenWidth, 0);
        [_bannerScrollView setContentOffset:CGPointMake(_showBannerImageArr.count==1?0:kScreenWidth , 0)];
    }
}
#pragma mark-scrollView 代理方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==_bannerScrollView &&_showBannerImageArr.count) {
        NSInteger page = scrollView.contentOffset.x/scrollView.frame.size.width-1;
        if (page<-1 ||page>_pageControl.numberOfPages) return;
        if (page == _pageControl.numberOfPages) {
            //如果是滑到最右侧，那么回到第一个。
            _pageControl.currentPage = 0;
            _timerPage =1;
            [_bannerScrollView setContentOffset:CGPointMake(_bannerScrollView.frame.size.width, 0) animated:NO];
        }else if (page == -1){
            //如果是滑到最左侧，那么回到最后一个。
            _pageControl.currentPage = _showBannerImageArr.count-3;
            _timerPage =_showBannerImageArr.count-2;
            [_bannerScrollView setContentOffset:CGPointMake(_bannerScrollView.frame.size.width*(_showBannerImageArr.count-2), 0) animated:NO];
        }else{
            _pageControl.currentPage = page;
            _timerPage =page+1;
        }
    }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (scrollView==_bannerScrollView &&_showBannerImageArr.count) {
        NSInteger page = scrollView.contentOffset.x/scrollView.frame.size.width-1;
        if (page >= _pageControl.numberOfPages) {
            //如果是滑到最右侧，那么回到第一个。
            _pageControl.currentPage = 0;
            _timerPage =1;
            [_bannerScrollView setContentOffset:CGPointMake(_bannerScrollView.frame.size.width, 0) animated:NO];
        }else if (page == -1){
            //如果是滑到最左侧，那么回到最后一个。
            _pageControl.currentPage = _showBannerImageArr.count-3;
            _timerPage =_showBannerImageArr.count-2;
            [_bannerScrollView setContentOffset:CGPointMake(_bannerScrollView.frame.size.width*(_showBannerImageArr.count-2), 0) animated:NO];
        }else{
            _pageControl.currentPage = page;
        }
    }
}
#pragma mark- 时间
-(NSTimer *)timer
{
    if (!_timer) {
        _timer =[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(timerChangeIamge) userInfo:nil repeats:YES];
        _timerPage =0;
        [_timer fire];
    }
    return _timer;
}
-(void)timerChangeIamge
{
    _timerPage++;
    [_bannerScrollView setContentOffset:CGPointMake(kScreenWidth*_timerPage, 0) animated:YES];
}
-(void)stopTimer
{
    [_timer invalidate];
    _timer =nil;
    _bannerScrollView.delegate =nil;
}
-(void)beginTimer
{
    [self timer];
    _bannerScrollView.delegate =self;
}
#pragma mark- 点击
-(void)imageButtonClick:(UIButton *)sender
{
    QABannerModel *bannerModel =_showBannerImageArr[sender.tag-10000];
    if(self.bannerClickBlock){
        self.bannerClickBlock(bannerModel);
    }
}

-(NSMutableArray *)showBannerImageArr
{
    if (!_showBannerImageArr) {
        _showBannerImageArr =[[NSMutableArray alloc]init];
    }
    return _showBannerImageArr;
}
@end

