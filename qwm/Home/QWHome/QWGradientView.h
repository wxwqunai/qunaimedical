//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <UIKit/UIKit.h>

@interface QWGradientView : UIView
@property (strong, nonatomic) CAGradientLayer* gradientLayer;
@end
