//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import "CacheUtil.h"
#import "QWCachedImage.h"
#import "QWDataKeys.m"
#import "DateUtil.h"
#import "GCDUtil.h"

NSString* const kCacheImagePath = @"images/";
NSString* const kDataPath = @"data.plist";

@interface CacheUtil ()
@property (copy, nonatomic) NSString* documentPath;
@property (strong, nonatomic) NSMutableDictionary* QWCachedImagesDic;

@property (copy, nonatomic) NSString* dataPath;
@property (copy, nonatomic) NSString* imagePath;
@end

@implementation CacheUtil
#pragma mark - init
+ (instancetype)cache
{
    static CacheUtil* util = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        util = [[CacheUtil alloc] init];
    });
    return util;
}
#pragma mark - dealloc
- (void)dealloc
{
    [self saveData];
}

#pragma mark - getters
- (NSString*)dataPath
{
    if (_dataPath == nil) {
        _dataPath = [self.documentPath stringByAppendingString:kDataPath];
    }
    return _dataPath;
}
- (NSString*)imagePath
{
    if (_imagePath == nil) {
        _imagePath = [self.documentPath stringByAppendingString:kCacheImagePath];
        if (![[NSFileManager defaultManager] fileExistsAtPath:_imagePath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:_imagePath withIntermediateDirectories:true attributes:nil error:nil];
        }
    }
    return _imagePath;
}
- (NSString*)documentPath
{
    if (_documentPath == nil) {
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
            NSUserDomainMask, true);
        _documentPath = [paths[0] stringByAppendingString:@"/"];
    }
    return _documentPath;
}
- (NSDictionary*)dataDic
{
    if (_dataDic == nil) {
        _dataDic = [NSMutableDictionary dictionary];
        if ([[NSFileManager defaultManager] fileExistsAtPath:self.dataPath]) {
            _dataDic = [NSMutableDictionary dictionaryWithContentsOfFile:self.dataPath];
        }
    }
    return _dataDic;
}
- (NSMutableDictionary*)QWCachedImagesDic
{
    if (_QWCachedImagesDic == nil) {
        _QWCachedImagesDic = self.dataDic[DATAKEY_QWCachedImageS];
        if (_QWCachedImagesDic == nil) {
            _QWCachedImagesDic = [NSMutableDictionary dictionary];
            [self.dataDic setObject:_QWCachedImagesDic forKey:DATAKEY_QWCachedImageS];
        }
    }
    return _QWCachedImagesDic;
}
#pragma mark - plist cache
- (void)saveData
{
    BOOL result = [self.dataDic writeToFile:self.dataPath atomically:true];
    if (result)
        NSLog(@"成功保存plist");
}

#pragma mark - image cache
- (void)cacheImageWithKey:(NSString*)key andUrl:(NSString*)url completion:(void (^)(UIImage* image))completion
{
    //异步加载图片并缓存到本地
    [[GCDUtil globalQueueWithLevel:DEFAULT] async:^{
        NSData* imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        UIImage* image = [UIImage imageWithData:imgData];

        NSString* QWCachedImagePath = [self.dataDic objectForKey:key];
        if (QWCachedImagePath == nil) {
            NSString* fileName = [NSString stringWithFormat:@"%@%04d.png", [DateUtil dateIdentifierNow], arc4random() % 10000];
            NSString* savePath = [self.imagePath stringByAppendingString:fileName];
            NSError* error = nil;
            BOOL result = [UIImagePNGRepresentation(image) writeToFile:savePath options:NSDataWritingAtomic error:&error];

            if (result) {
                [self.QWCachedImagesDic setObject:@{ DATAKEY_QWCachedImageS_URL : url,
                    DATAKEY_QWCachedImageS_FILENAME : fileName }
                                         forKey:key];
                if (completion != nil) {
                    //TODO: 一定要记住回到主线程啊。。。
                    [[GCDUtil mainQueue] async:^{
                        completion(image);
                    }];
                }
            }
            else
                NSLog(@"Fail to save image %@", error.localizedDescription);
        }
    }];
}
- (UIImage*)imageWithKey:(NSString*)key
{
    QWCachedImage* model = [self QWCachedImageWithKey:key];
    if (model == nil)
        return nil;

    NSString* path
        = [self.imagePath stringByAppendingString:model.fileName];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        return image;
    }
    return nil;
}
- (QWCachedImage*)QWCachedImageWithKey:(NSString*)key
{
    //早知道就用coredata了。。
    QWCachedImage* model = [QWCachedImage QWCachedImageWithDic:self.QWCachedImagesDic[key]];
    return model.fileName == nil ? nil : model;
}
@end
