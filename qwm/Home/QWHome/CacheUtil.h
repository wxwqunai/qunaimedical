//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString* const kCacheImagePath;
extern NSString* const kDataPath;

@class QWCachedImage;

/**
 *  缓存单例类
 */
@interface CacheUtil : NSObject
@property (strong, nonatomic) NSMutableDictionary* dataDic;
+ (instancetype)cache;

/**
 *  缓存图片
 *
 *  @param key        key
 *  @param url        图片地址
 *  @param completion 回调
 */
- (void)cacheImageWithKey:(NSString*)key andUrl:(NSString*)url completion:(void (^)(UIImage* image))completion;
/**
 *  根据key获取图片
 *
 *  @param key <#key description#>
 *
 *  @return <#return value description#>
 */
- (UIImage*)imageWithKey:(NSString*)key;
/**
 *  根据key获取QWCachedImages实例
 *
 *  @param key <#key description#>
 *
 *  @return <#return value description#>
 */
- (QWCachedImage*)QWCachedImageWithKey:(NSString*)key;
/**
 *  保存缓存
 */
- (void)saveData;
@end
