//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <UIKit/UIKit.h>

@protocol QWSliderViewDataSource<NSObject>
@required
/**
 *  指定Slider的项目数
 *
 *  @return return value description
 */
- (NSUInteger)numberOfItemsInQWSliderView;

@optional
/**
 *  指定索引位置的图片
 *
 *  @param index <#index description#>
 *
 *  @return <#return value description#>
 */
- (UIImage*)imageForSliderAtIndex:(NSInteger)index;
/**
 *  指定索引位置的标题
 *
 *  @param index <#index description#>
 *
 *  @return <#return value description#>
 */
- (NSString*)titleForSliderAtIndex:(NSInteger)index;
/**
 *  指定索引位置的副标题
 *
 *  @param index <#index description#>
 *
 *  @return <#return value description#>
 */
- (NSString*)subTitleForSliderAtIndex:(NSInteger)index;
/**
 *  指定索引位置的事件
 *
 *  @param index <#index description#>
 */
- (void)touchUpForSliderAtIndex:(NSInteger)index;
@end

@interface QWSliderView : UIView
@property (weak, nonatomic) id<QWSliderViewDataSource> dataSource;
@property (assign, nonatomic) NSTimeInterval interval;

@property (assign, nonatomic) NSInteger contentInsetY;
/**
 *  构建QWSliderView
 */
- (void)buildQWSliderView;

- (void)stopSliding;
- (void)startSliding;

/**
 *  设定索引位置的图片
 *
 *  @param index <#index description#>
 */
- (void)setImage:(UIImage*)image atIndex:(NSUInteger)index;
@end
