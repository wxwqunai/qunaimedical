//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import "QWCachedImage.h"

@implementation QWCachedImage
- (instancetype)initWithDic:(NSDictionary*)dic
{
  self = [super init];
  if (self) {
    [self setValuesForKeysWithDictionary:dic];
  }
  return self;
}

+ (instancetype)QWCachedImageWithDic:(NSDictionary*)dic
{
  return [[self alloc] initWithDic:dic];
}
@end
