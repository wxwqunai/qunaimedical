//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <Foundation/Foundation.h>

@interface DateUtil : NSObject
+ (NSDate*)stringToDate:(NSString*)dateString format:(NSString*)format;
+ (NSString*)dateString:(NSDate*)date withFormat:(NSString*)format;
+ (NSString*)dateIdentifierNow;
+ (NSString*)dateString:(NSString*)originalStr fromFormat:(NSString*)fromFormat toFormat:(NSString*)toFormat;
+ (NSString*)appendWeekStringFromDate:(NSDate*)date withFormat:(NSString*)format;
+ (NSString*)weekdayStringFromDate:(NSDate*)inputDate;
@end
