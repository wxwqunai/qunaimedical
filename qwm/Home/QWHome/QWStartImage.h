//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface QWStartImage : NSObject
@property (copy, nonatomic) NSString* img;
@property (copy, nonatomic) NSString* text;

- (instancetype)initWithDic:(NSDictionary*)dic;
+ (instancetype)QWStartImageWithDic:(NSDictionary*)dic;
@end
