//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import "CacheUtil.h"
#import "MainViewController.h"
#import "QWSliderView.h"
#import "SliderViewController.h"
#import "Stories.h"
#import "Story.h"
#import "QWStoryView.h"

@interface
QWSliderViewController ()<QWSliderViewDataSource, QWStoryViewDelegate>
@property (strong, nonatomic) NSArray<Stories*>* stories;
@property (strong, nonatomic) Story* story;

@property (assign, nonatomic) BOOL isAnimating;
@property (strong, nonatomic) QWStoryView* QWStoryView;
@end

@implementation QWSliderViewController
#pragma mark - initialization
- (instancetype)initWithFrame:(CGRect)frame andStory:(Story*)story
{
  if (self = [super init]) {
    Stories* stories = [[Stories alloc] init];
    stories.image = story.image;
    stories.title = story.title;

    self.stories = @[ stories ];
    self.story = story;

    self.QWSliderView = [[QWSliderView alloc] initWithFrame:frame];

    [self buildQWSliderView];
  }
  return self;
}
- (instancetype)initWithFrame:(CGRect)frame
                   andStories:(NSArray<Stories*>*)stories
{
  if (self = [super init]) {
    self.stories = stories;
    self.QWSliderView = [[QWSliderView alloc] initWithFrame:frame];

    [self buildQWSliderView];
  }

  return self;
}
- (void)buildQWSliderView
{
  self.QWSliderView.dataSource = self;
  self.QWSliderView.contentInsetY = [MainViewController sliderInsetY];
  self.view = self.QWSliderView;

  [self.QWSliderView buildQWSliderView];
  [self loadQWSliderViewImages];
}
- (void)loadQWSliderViewImages
{
  CacheUtil* util = [CacheUtil cache];
  [self.stories
    enumerateObjectsUsingBlock:^(Stories* stories, NSUInteger idx, BOOL* stop) {
      NSString* url = stories.image;
      if ([util imageWithKey:url] == nil)
        [util cacheImageWithKey:url
                         andUrl:url
                     completion:^(UIImage* image) {
                       [self.QWSliderView setImage:image atIndex:idx];
                     }];
      else
        [self.QWSliderView setImage:[util imageWithKey:url] atIndex:idx];
    }];
}
#pragma mark - QWSliderView datasource
- (NSUInteger)numberOfItemsInQWSliderView
{
  return self.stories.count;
}
- (NSString*)titleForSliderAtIndex:(NSInteger)index
{
  return self.stories[index].title;
}
- (NSString*)subTitleForSliderAtIndex:(NSInteger)index
{
  return self.story.imageSource;
}
- (void)touchUpForSliderAtIndex:(NSInteger)index
{
  if (self.story != nil)
    return;
  if (self.isAnimating)
    return;
  self.isAnimating = true;

  self.QWStoryView = [[QWStoryView alloc] init];
  self.QWStoryView.QWStoryViewDelegate = self;
  Stories* selectedStories = self.stories[index];
  self.QWStoryView.identifier = selectedStories.identidier;

  CGSize screenSize = [UIScreen mainScreen].bounds.size;
  //从右向左的滑动动画
  self.QWStoryView.frame =
    CGRectMake(screenSize.width, 0, screenSize.width, screenSize.height);
  [self.view.window insertSubview:self.QWStoryView aboveSubview:self.view];
  [UIView animateWithDuration:0.3
    animations:^{
      self.QWStoryView.frame =
        CGRectMake(0, 0, screenSize.width, screenSize.height);
    }
    completion:^(BOOL finished) {
      self.isAnimating = false;
    }];
}
#pragma mark QWStoryView delegate
- (void)releaseQWStoryView
{
  [self.QWStoryView removeFromSuperview];
  self.QWStoryView = nil;
}
#pragma mark release
- (void)dealloc
{
  [self removeFromParentViewController];
}
@end
