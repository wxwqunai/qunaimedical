//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import "QWDataSource.h"
#import "APIRequest.h"
#import "CacheUtil.h"
#import "DateUtil.h"
#import "MD5Util.h"
#import "MainViewController.h"
#import "QWSliderView.h"
#import "SliderViewController.h"
#import "Stories.h"
#import "QWStoriesHeaderCell.h"
#import "StoriesTableViewCell.h"
#import "QWStoryView.h"
#import "UINavigationBar+BackgroundColor.h"

#import "QWDragView.h"
#import "QWWebviewController.h"
#import "QWTipsAlertController.h"

static NSString* const kStoriesIdentifier = @"stories";
static NSString* const kStoriesHeaderIdentifier = @"storiesHeader";
static NSUInteger const kHeaderViewHeight = 44;
static NSUInteger const kRowHeight = 90;

@interface
MainViewController ()<QWStoryViewDelegate>
@property (strong, nonatomic) QWDataSource* dataSource;

@property (copy, nonatomic) NSArray<Stories*>* topStories;
@property (strong, nonatomic) NSMutableDictionary* stories;
@property (strong, nonatomic) NSMutableArray* dates;
@property (assign, nonatomic) BOOL isRequesting;
@property (assign, nonatomic) BOOL isAnimating;

@property (strong, nonatomic) QWSliderViewController* QWSliderViewController;

@property (assign, nonatomic) CGSize viewSize;

@property (nonatomic, readonly) UIColor* themeColorWithAdjustmentAlpha;
@property (assign, nonatomic) NSInteger secondSectionOffsetY;

@property (weak, nonatomic) QWSliderView* QWSliderView;

@property (strong, nonatomic) QWStoryView* storyVC;

@property (nonatomic, strong) QWDragView *logoView;//悬浮按钮
@property (nonatomic,strong) QWOpenqaModel *openqaModel;//悬浮按钮-数据
@end

@implementation MainViewController
#pragma mark - getters
- (QWSliderView*)QWSliderView
{
  if (_QWSliderView == nil) {
    _QWSliderView = self.QWSliderViewController.QWSliderView;
  }
  return _QWSliderView;
}
- (QWDataSource*)dataSource
{
  if (_dataSource == nil) {
    _dataSource = [QWDataSource dataSource];
  }
  return _dataSource;
}
- (NSMutableDictionary*)stories
{
  if (_stories == nil) {
    _stories = [NSMutableDictionary dictionary];
  }
  return _stories;
}
- (NSMutableArray*)dates
{
  if (_dates == nil) {
    _dates = [NSMutableArray array];
  }
  return _dates;
}

- (CGSize)viewSize
{
  return self.view.bounds.size;
}
+ (UIColor*)themeColor
{
  static UIColor* color = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    color = [UIColor colorWithRed:51.0 / 255
                            green:153.0 / 255
                             blue:230.0 / 255
                            alpha:1];
  });
  return color;
}
+ (NSInteger)sliderInsetY
{
  return -100;
}
+ (NSUInteger)sliderDisplayHeight
{
  return 736 == [[UIScreen mainScreen] bounds].size.height ? 230 : 200;
}
- (QWSliderViewController*)QWSliderViewController
{
  if (_QWSliderViewController == nil) {
    _QWSliderViewController = [[QWSliderViewController alloc]
      initWithFrame:CGRectMake(0, [self.class sliderInsetY],
                               self.viewSize.width,
                               [self.class sliderDisplayHeight] +
                                 labs([self.class sliderInsetY]))
         andStories:self.topStories];
  }
  return _QWSliderViewController;
}
- (UIColor*)themeColorWithAdjustmentAlpha
{
  CGFloat contentOffsetY = self.tableView.contentOffset.y;
  contentOffsetY = MAX(contentOffsetY, 0);
  CGFloat alpha = (contentOffsetY + [self.class sliderInsetY]) /
                  ([self.class sliderDisplayHeight] - 64);
  UIColor* color =
    [[MainViewController themeColor] colorWithAlphaComponent:alpha];
  return color;
}
- (NSInteger)secondSectionOffsetY
{
  NSInteger firstOffsetY =
    [self.class sliderDisplayHeight] + labs([self.class sliderInsetY]) + 20;

  _secondSectionOffsetY =
    firstOffsetY +
    [self tableView:self.tableView numberOfRowsInSection:0] * kRowHeight - 40;

  return _secondSectionOffsetY;
}
#pragma mark - initialization
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _logoView.hidden = IsStringEmpty(self.openqaModel.url);
    [self loadLatestData];
    [self logoData];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    _logoView.hidden = YES;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
    // 监测网络情况
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name: kReachabilityChangedNotification
                                               object: nil];
    
  [self buildMainPage];
    
    UIImageView *navBarHairlineImageView = [self findHairlineImageViewUnder:self.navigationController.navigationBar];
    // 隐藏导航栏下的黑线
    navBarHairlineImageView.hidden = YES;
    
    [self logoView];
}

#pragma mark -网络监听
- (void)reachabilityChanged:(NSNotification *)note {
    // 连接改变
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    //处理连接改变后的情况
    NetworkStatus status = [curReach currentReachabilityStatus];
 
    if (status != NotReachable) {
        [self loadLatestData];
    }
}

- (void)buildMainPage
{
  [self loadLatestData];
  [self buildNavigation];
  [self buildTableView];
}
- (void)buildQWSliderView
{
  [self addChildViewController:self.QWSliderViewController];
  self.tableView.tableHeaderView = self.QWSliderViewController.view;
}
- (void)buildNavigation
{
  [self setNavigationBarTransparent];
  [self.navigationController.navigationBar setTitleTextAttributes:@{
    NSForegroundColorAttributeName : [UIColor whiteColor]
  }];
}
- (void)buildTableView
{
  [self.tableView registerNib:[UINib nibWithNibName:@"StoriesTableViewCell"
                                             bundle:nil]
       forCellReuseIdentifier:kStoriesIdentifier];
  self.tableView.showsVerticalScrollIndicator = false;
  self.tableView.showsHorizontalScrollIndicator = false;
  self.tableView.rowHeight = kRowHeight;
}
#pragma mark - tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
  return self.stories.count;
}
- (NSInteger)tableView:(UITableView*)tableView
 numberOfRowsInSection:(NSInteger)section
{
  if (self.dates.count == 0)
    return 0;

  NSUInteger rowCountInSection = [self.stories[self.dates[section]] count];
  return rowCountInSection;
}
- (CGFloat)tableView:(UITableView*)tableView
  heightForHeaderInSection:(NSInteger)section
{
  if (section == 0)
    return 0;

  return kHeaderViewHeight;
}
- (UIView*)tableView:(UITableView*)tableView
  viewForHeaderInSection:(NSInteger)section
{
  QWStoriesHeaderCell* header = [[QWStoriesHeaderCell alloc]
    initWithFrame:CGRectMake(0, 0, self.viewSize.width, kHeaderViewHeight)];
  header.text = [DateUtil dateString:self.dates[section]
                          fromFormat:@"yyyyMMdd"
                            toFormat:@"MM月dd日 EEEE"];

  return header;
}
- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
  StoriesTableViewCell* cell =
    [tableView dequeueReusableCellWithIdentifier:kStoriesIdentifier
                                    forIndexPath:indexPath];
  NSArray<Stories*>* storiesOfADay =
    self.stories[self.dates[indexPath.section]];
  cell.stories = storiesOfADay[indexPath.row];

  return cell;
}
#pragma mark - tableview delegate
- (void)tableView:(UITableView*)tableView
  didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
  if (self.isAnimating)
    return;
  self.isAnimating = true;

  self.storyVC = [[QWStoryView alloc] init];
  self.storyVC.QWStoryViewDelegate = self;
  Stories* selectedStories =
    self.stories[self.dates[indexPath.section]][indexPath.row];
  self.storyVC.identifier = selectedStories.identidier;

  //从右向左的滑动动画
  self.storyVC.frame =
    CGRectMake(self.view.bounds.size.width, 0, self.view.bounds.size.width,
               self.view.bounds.size.height);
  [self.view.window insertSubview:self.storyVC aboveSubview:self.view];
  [UIView animateWithDuration:0.3
    animations:^{
      self.storyVC.frame = CGRectMake(0, 0, self.view.bounds.size.width,
                                      self.view.bounds.size.height);
    }
    completion:^(BOOL finished) {
      self.isAnimating = false;
    }];
}
#pragma mark - QWStoryViewController delegate
- (void)releaseQWStoryView
{
  [self.storyVC removeFromSuperview];
  self.storyVC = nil;
}
#pragma mark - api datasource
- (void)loadLatestData
{
//  if (self.isRequesting)
//    return;

  self.isRequesting = true;
  [self.dataSource newsLatest:^(BOOL needsToReload) {
    if (!needsToReload && self.topStories.count != 0)
      return;

    self.topStories = self.dataSource.topStories;
    [self.stories setObject:self.dataSource.stories
                     forKey:self.dataSource.date];
    [self.dates addObject:self.dataSource.date];

    [self buildQWSliderView];
    [self.tableView reloadData];

    self.isRequesting = false;
  }];
}
- (void)loadBeforeData
{
  if (self.isRequesting || self.topStories.count == 0)
    return;

  NSDate* dayBefore =
    [[DateUtil stringToDate:self.dates.lastObject format:@"yyyyMMdd"]
      dateByAddingTimeInterval:-24 * 60 * 60];
  if ([self.dates
        containsObject:[DateUtil dateString:dayBefore withFormat:@"yyyyMMdd"]])
    return;

  self.isRequesting = true;
  [self.dataSource
    newsBefore:self.dates.lastObject
    completion:^{
      [self.stories setObject:self.dataSource.stories
                       forKey:self.dataSource.date];
      [self.dates addObject:self.dataSource.date];

      [self.tableView beginUpdates];
      [self.tableView
          insertSections:[NSIndexSet indexSetWithIndex:self.stories.count - 1]
        withRowAnimation:UITableViewRowAnimationNone];
      [self.tableView endUpdates];

      self.isRequesting = false;
    }];
}

#pragma mark - scrollview delegate
- (void)scrollViewWillBeginDragging:(UIScrollView*)scrollView
{
  [self.QWSliderView stopSliding];
}

- (void)scrollViewDidEndDragging:(UIScrollView*)scrollView
                  willDecelerate:(BOOL)decelerate
{
  [self.QWSliderView startSliding];
}
- (void)scrollViewDidScroll:(UIScrollView*)scrollView
{
  //限制scrollview的bounce size
  if (scrollView.contentOffset.y <= 0) {
    CGPoint offset = scrollView.contentOffset;
    offset.y = 0;
    scrollView.contentOffset = offset;
  }
  if (scrollView.contentOffset.y >
      scrollView.contentSize.height - self.view.bounds.size.height) {
//    [self loadBeforeData];
  }

  [self adjustNavigationAlpha];
}
#pragma mark navigationbar styles
- (void)setNavigationBarTransparent
{
  self.navigationController.navigationBar.translucent = true;

  UIColor* color = [UIColor clearColor];
  CGRect rect = CGRectMake(0, 0, self.viewSize.width, 64);

  UIGraphicsBeginImageContext(rect.size);
  CGContextRef ref = UIGraphicsGetCurrentContext();
  CGContextSetFillColorWithColor(ref, color.CGColor);
  CGContextFillRect(ref, rect);
  UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  [self.navigationController.navigationBar
    setBackgroundImage:image
         forBarMetrics:UIBarMetricsDefault];
}

- (void)adjustNavigationAlpha
{
  if (self.tableView.contentOffset.y > self.secondSectionOffsetY) {
    self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
    //这里不能直接隐藏navigationbar，会导致tableview的insettop失控
    self.navigationItem.title = @"";
    //缩短背景视图避免其挡住section header
    [self.navigationController.navigationBar setBackgroundLayerHeight:20];
  } else {
    self.tableView.contentInset =
      UIEdgeInsetsMake([self.class sliderInsetY], 0, 0, 0);
    self.navigationItem.title = @"今日热闻";

    [self.navigationController.navigationBar setBackgroundLayerHeight:64];
  }

  [self.navigationController.navigationBar
    setNavigationBackgroundColor:self.themeColorWithAdjustmentAlpha];
}

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

#pragma mark - 悬浮按钮
- (QWDragView *)logoView{
    if(!_logoView){
        _logoView = [[QWDragView alloc] init];
        _logoView.backgroundColor = [UIColor clearColor];
        _logoView.isKeepBounds = YES;
        _logoView.freeRect = CGRectMake(0, [UIDevice navigationFullHeight], kScreenWidth, kScreenHeight-[UIDevice navigationFullHeight]-[UIDevice tabBarFullHeight]);
        _logoView.imageView.image = [UIImage imageNamed:@"drawIcon"];
        _logoView.hidden = YES;
        [[UIApplication sharedApplication].delegate.window addSubview:_logoView];

        [_logoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_logoView.superview);
            make.bottom.equalTo(_logoView.superview).offset(-[UIDevice tabBarFullHeight]-20);
            make.size.mas_equalTo(CGSizeMake(70, 70));
        }];
        
        __weak __typeof(self)weakSelf = self;
        _logoView.clickDragViewBlock = ^(QWDragView *dragView){
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if(strongSelf.openqaModel.isCheckCode){
                QWTipsAlertController *activityVC =[[QWTipsAlertController alloc]init];
                activityVC.tipType = TipsAlertType_checkCode;
                activityVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
                activityVC.modalPresentationStyle=UIModalPresentationOverFullScreen;
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:activityVC animated:NO completion:nil];
                
                activityVC.checkCodeView.sureBlock = ^(NSString * _Nonnull code) {
                    [activityVC.checkCodeView checkInput:[code isEqualToString:strongSelf.openqaModel.checkCode]];
                    if(![code isEqualToString:strongSelf.openqaModel.checkCode]) return;
                    [strongSelf jumToWebView];
                };
            }else{
                [strongSelf jumToWebView];
            }
        };
    }
    return _logoView;
}

#pragma mark - 悬浮按钮-请求
- (void)logoData{
    
    NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
    if([AppProfile isLogined]){
        [dic setValue:AppProfile.qaUserInfo.phone forKey:@"userName"];
    }
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_Resfile_Openqa argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        if(success && [result[@"code"] intValue] == 0){
            self.openqaModel = [QWOpenqaModel mj_objectWithKeyValues:result[@"data"]];
            self.logoView.hidden = IsStringEmpty(self.openqaModel.url);
        }
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        NSLog(@"%@",errorInfo);
    }];
}

#pragma mark - 活动
- (void)jumToWebView{
    QWWebviewController *webviewVC =[[QWWebviewController alloc]init];
    NSString *url = [NSString stringWithFormat:@"%@%@",QWM_BASE_API_URL,self.openqaModel.url];
    if([AppProfile isLogined]){
        url = [NSString stringWithFormat:@"%@?userName=%@&password=%@",url,AppProfile.qaUserInfo.phone,AppProfile.qaUserInfo.password];
    }
    webviewVC.url = url;
    webviewVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webviewVC animated:YES];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
