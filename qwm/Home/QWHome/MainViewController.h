//  qwm
//
//  Created by kevin on 2023/3/24.
//


#import <UIKit/UIKit.h>

@interface MainViewController : UITableViewController
+ (UIColor*)themeColor;

+ (NSInteger)sliderInsetY;
+ (NSUInteger)sliderDisplayHeight;

- (void)loadLatestData;
@end

