//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <Foundation/Foundation.h>

@interface QWCachedImage : NSObject
@property (copy, nonatomic) NSString* url;
@property (copy, nonatomic) NSString* fileName;

- (instancetype)initWithDic:(NSDictionary*)dic;
+ (instancetype)QWCachedImageWithDic:(NSDictionary*)dic;
@end
