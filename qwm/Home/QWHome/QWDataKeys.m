//  qwm
//
//  Created by kevin on 2023/3/24.
//

/*
 用于存放plist的key
 */

#import <Foundation/Foundation.h>

/*缓存图片*/
static NSString* const DATAKEY_QWCachedImageS = @"QWCachedImages";
static NSString* const DATAKEY_QWCachedImageS_URL = @"url";
static NSString* const DATAKEY_QWCachedImageS_FILENAME = @"fileName";

/*起始图片*/
static NSString* const DATAKEY_QWStartImage = @"QWStartImage";
static NSString* const DATAKEY_QWStartImage_AUTHOR = @"QWStartImageAuthor";
static NSString* const DATAKEY_QWStartImage_URL = @"QWStartImageUrl";

/*首页数据*/
static NSString* const DATAKEY_STORIES = @"StoryData";
static NSString* const DATAKEY_STORIES_SIGNATURE = @"signature";
static NSString* const DATAKEY_STORIES_DATE = @"date";
static NSString* const DATAKEY_STORIES_STORIES = @"stories";
static NSString* const DATAKEY_STORIES_TOPSTORIES = @"top_stories";
