//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <WebKit/WebKit.h>

@protocol QWStoryViewDelegate<NSObject>
/**
 *  当该控制器生命周期结束时调用该方法以释放QWSliderView
 */
- (void)releaseQWStoryView;
@end

@interface QWStoryView : WKWebView
@property (weak, nonatomic) id<QWStoryViewDelegate> QWStoryViewDelegate;
@property (strong, nonatomic) QWSliderViewController* QWSliderViewController;

@property (assign, nonatomic) NSUInteger identifier;
@end
