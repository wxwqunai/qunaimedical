//  qwm
//
//  Created by kevin on 2023/3/24.
//
#import "MainViewController.h"
#import "QWStoriesHeaderCell.h"

@interface QWStoriesHeaderCell ()
@property (strong, nonatomic) UILabel* label;

@end

@implementation QWStoriesHeaderCell
- (UILabel*)label
{
    if (_label == nil) {
        _label = [[UILabel alloc] init];
    }
    return _label;
}

- (void)setText:(NSString*)text
{
    if (_text != text) {
        _text = [text copy];
        [self buildHeaderView];
    }
}

- (void)buildHeaderView
{
    self.backgroundColor = [MainViewController themeColor];
    self.label.frame = self.frame;
    self.label.text = self.text;
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.textColor = [UIColor whiteColor];

    [self addSubview:self.label];
}
@end
