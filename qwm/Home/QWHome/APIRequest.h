//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <Foundation/Foundation.h>

@interface APIRequest : NSObject
+ (void)requestWithUrl:(NSString*)url;
+ (void)requestWithUrl:(NSString*)url completion:(void (^)(id data, NSString* md5))completion;

+ (NSDictionary*)objToDic:(id)object;
@end
