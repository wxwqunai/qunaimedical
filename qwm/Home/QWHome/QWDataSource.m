//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import "QWDataSource.h"
#import "APIRequest.h"
#import "CacheUtil.h"
#import "QWCachedImage.h"
#import "QWDataKeys.m"
#import "DateUtil.h"
#import "GCDUtil.h"
#import "QWStartImage.h"
#import "Stories.h"
#import "Story.h"

@interface
QWDataSource ()
@property (strong, nonatomic) CacheUtil* cache;
@end
@implementation QWDataSource
#pragma mark - init
+ (instancetype)dataSource
{
  static QWDataSource* datasource = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    datasource = [[QWDataSource alloc] init];
  });
  return datasource;
}
- (CacheUtil*)cache
{
  if (_cache == nil) {
    _cache = [CacheUtil cache];
  }
  return _cache;
}
#pragma mark - NewsLatest & NewsBefore
- (void)newsLatest:(void (^)(BOOL needsToReload))completion
{
  [APIRequest
    requestWithUrl:API_Url_NewsLatest
        completion:^(id data, NSString* md5) {
          BOOL needsToReload = false;

          NSDictionary* dic = [self loadStoriesData:data
                                          signature:md5
                                      needsToReload:&needsToReload];

          self.topStories = [Stories stories:dic[DATAKEY_STORIES_TOPSTORIES]];
          self.stories = [Stories stories:dic[DATAKEY_STORIES_STORIES]];
          self.date = dic[DATAKEY_STORIES_DATE];

          if (completion != nil)
            completion(needsToReload);
        }];
}
- (void)newsBefore:(NSString*)date completion:(void (^)(void))completion
{
  NSString* url = [NSString stringWithFormat:@"%@%@", API_Url_NewsBefore, date];

  [APIRequest requestWithUrl:url
                  completion:^(id data, NSString* md5) {
                    BOOL needsToReload = false;
                    NSDictionary* dic = [self loadStoriesData:data
                                                    signature:md5
                                                needsToReload:&needsToReload];
                    self.stories =
                      [Stories stories:dic[DATAKEY_STORIES_STORIES]];
                    self.date = dic[DATAKEY_STORIES_DATE];

                    if (completion != nil)
                      completion();
                  }];
}
- (NSDictionary*)loadStoriesData:(id)data
                       signature:(NSString*)md5
                   needsToReload:(BOOL*)needsToReload
{
  NSMutableDictionary* dic =
    [NSMutableDictionary dictionaryWithDictionary:[APIRequest objToDic:data]];

  NSString* date = dic[DATAKEY_STORIES_DATE];
  if (self.cache.dataDic[date] == nil) {
    [dic setValue:md5 forKey:DATAKEY_STORIES_SIGNATURE];
    [self.cache.dataDic setObject:dic forKey:date];

    *needsToReload = true;
  } else {
    NSDictionary* cachedDic = self.cache.dataDic[date];
    // compare signature
    if (![cachedDic[DATAKEY_STORIES_SIGNATURE] isEqualToString:md5]) {
      [self.cache.dataDic removeObjectForKey:date];

      [dic setValue:md5 forKey:DATAKEY_STORIES_SIGNATURE];
      [self.cache.dataDic setObject:dic forKey:date];

      *needsToReload = true;
    }
  }
  return dic;
}
#pragma mark - QWStartImage
- (UIImage*)QWStartImage
{
  if (_QWStartImage == nil) {
    _QWStartImage = [self.cache imageWithKey:DATAKEY_QWStartImage];
  }
  return _QWStartImage;
}
- (NSString*)QWStartImageAuthor
{
  if (_QWStartImageAuthor == nil) {
    _QWStartImageAuthor = self.cache.dataDic[DATAKEY_QWStartImage_AUTHOR];
  }
  return _QWStartImageAuthor;
}
- (void)QWStartImage:(void (^)(void))completion
{
  NSString* imageApiUrl =
    [NSString stringWithFormat:@"%@%@", API_Url_QWStartImage, @"1080*1776"];
  [APIRequest requestWithUrl:imageApiUrl
                  completion:^(id data, NSString* md5) {
                    QWStartImage* model =
                      [QWStartImage QWStartImageWithDic:[APIRequest objToDic:data]];
                    QWCachedImage* QWCachedImage =
                      [self.cache QWCachedImageWithKey:DATAKEY_QWStartImage];
                    if ([model.img isEqualToString:QWCachedImage.url])
                      return;

                    [self.cache cacheImageWithKey:DATAKEY_QWStartImage
                                           andUrl:model.img
                                       completion:nil];
                    [self.cache.dataDic setValue:model.text
                                          forKey:DATAKEY_QWStartImage_AUTHOR];
                  }];
}
#pragma mark - News
- (void)news:(NSUInteger)identifier completion:(void (^)(void))completion
{
  NSString* url =
    [NSString stringWithFormat:@"%@%lu", API_Url, (unsigned long)identifier];
  [APIRequest requestWithUrl:url
                  completion:^(id data, NSString* md5) {
                    self.story =
                      [Story storyWithDic:[APIRequest objToDic:data]];

                    if (completion != nil)
                      completion();
                  }];
}

@end
