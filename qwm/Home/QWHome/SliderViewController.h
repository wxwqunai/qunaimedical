//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <UIKit/UIKit.h>

@class Stories;
@class Story;
@class QWSliderView;

@interface QWSliderViewController : UIViewController
@property (strong, nonatomic) QWSliderView* QWSliderView;

- (instancetype)initWithFrame:(CGRect)frame
                   andStories:(NSArray<Stories*>*)stories;
- (instancetype)initWithFrame:(CGRect)frame andStory:(Story*)story;
@end
