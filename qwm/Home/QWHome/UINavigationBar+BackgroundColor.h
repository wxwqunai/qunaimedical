//
//  UINavigationBar+BackgroundColor.h
//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (BackgroundColor)
/**
 *  设置navigationbar的背景色
 *
 *  @param color <#color description#>
 */
- (void)setNavigationBackgroundColor:(UIColor*)color;
/**
 *  设置navigationbar的背景宽度
 *
 *  @param height <#height description#>
 */
- (void)setBackgroundLayerHeight:(CGFloat)height;
@end
