//  qwm
//
//  Created by kevin on 2023/3/24.
//

#define API_Url @"http://news-at.zhihu.com/api/4/news/"
#define API_Url_NewsLatest @"http://news-at.zhihu.com/api/4/news/latest"
#define API_Url_NewsBefore @"http://news.at.zhihu.com/api/4/news/before/"
#define API_Url_QWStartImage @"http://news-at.zhihu.com/api/4/start-image/"

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Stories;
@class Story;

@interface QWDataSource : NSObject
+ (instancetype)dataSource;

#pragma mark - NewsLatest & NewsBefore
@property (strong, nonatomic) NSArray<Stories*>* topStories;
@property (strong, nonatomic) NSString* date;
@property (strong, nonatomic) NSArray* stories;
@property (assign, nonatomic) BOOL isRequesting;

- (void)newsLatest:(void (^)(BOOL needsToReload))completion;
- (void)newsBefore:(NSString*)date completion:(void (^)(void))completion;

#pragma mark - QWStartImage
@property (strong, nonatomic) UIImage* QWStartImage;
@property (strong, nonatomic) NSString* QWStartImageAuthor;
- (void)QWStartImage:(void (^)(void))completion;

#pragma mark - News
@property (strong, nonatomic) Story* story;
- (void)news:(NSUInteger)identifier completion:(void (^)(void))completion;
@end
