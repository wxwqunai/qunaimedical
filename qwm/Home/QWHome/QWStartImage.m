//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import "QWStartImage.h"

@implementation QWStartImage
- (instancetype)initWithDic:(NSDictionary*)dic
{
  if (self = [super init]) {
    [self setValuesForKeysWithDictionary:dic];
  }
  return self;
}
+ (instancetype)QWStartImageWithDic:(NSDictionary*)dic
{
  return [[QWStartImage alloc] initWithDic:dic];
}
@end
