//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <UIKit/UIKit.h>

@class Stories;

@interface StoriesTableViewCell : UITableViewCell
@property (strong, nonatomic) Stories* stories;
@end
