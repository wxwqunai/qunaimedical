//
//  QWHomeController.m
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import "QWHomeController.h"
#import "QABannerView.h"
#import "QWHomeModel.h"
#import "QWWebviewController.h"
#import "QWBannerDetailController.h"

#import "QWDragView.h"
#import "QWWebviewController.h"
#import "QWTipsAlertController.h"
//#import "QWHomeCell.h"
//#import "QWHome2Cell.h"
#import "QWMenusCell.h"

#import "QWHomeNewsCell.h"
#import "QWCommentController.h"
#import "QWSpeakController.h"

@interface QWHomeController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showInfoArr;

@property (nonatomic, strong) QWDragView *logoView;//悬浮按钮
@property (nonatomic,strong) QWOpenqaModel *openqaModel;//悬浮按钮-数据
@end

@implementation QWHomeController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //        [self hiddenLeftBarButtonItem];
    if([self.openqaModel.type isEqualToString:@"1"] && self.openqaModel.isShowNoticeDialog) _logoView.hidden = NO; //悬浮按钮
//    [self logoData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if([self.openqaModel.type isEqualToString:@"1"] && self.openqaModel.isShowNoticeDialog) _logoView.hidden = YES; //悬浮按钮
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"首页";
    
    // 监测网络情况
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name: kReachabilityChangedNotification
                                               object: nil];
    //UI
    [self tableView];
    
    //数据
    [self loadAllRequest];
}
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
//        [_tableView registerNib:[UINib nibWithNibName:@"QWHomeCell" bundle:nil] forCellReuseIdentifier:@"QWHomeCell"];
//        [_tableView registerNib:[UINib nibWithNibName:@"QWHome2Cell" bundle:nil] forCellReuseIdentifier:@"QWHome2Cell"];
        [_tableView registerClass:[QWMenusCell class] forCellReuseIdentifier:@"QWMenusCell"];
        [_tableView registerClass:[QWHomeNewsCell class] forCellReuseIdentifier:@"QWHomeNewsCell"];
                
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 0? 250.0 : 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 0? 0 :_showInfoArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        QWMenusCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMenusCell"];
        return  cell;
    }else{
        QWHomeNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWHomeNewsCell"];
        QANewsModel *model = _showInfoArr[indexPath.row];
        cell.nameStr = model.title;
        cell.picStr =model.thumbnail_pic_s;
        cell.timeStr = model.date;
        cell.autherStr = model.author_name;
        
        __weak __typeof(self) weakSelf = self;
        cell.pinglunBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf addDiscussTopic:model];
        };
        
        cell.shareBlock = ^{
            [self jumToShare:model];
        };
        
        cell.voicePlayBlock = ^{
            [self JumpToSpeak:model.title withContent:model.vText];
        };
        return  cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 1){
        QANewsModel *newsModel = self.showInfoArr[indexPath.row];
        [self JumpToSpeak:newsModel.title withContent:newsModel.vText];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 0){
        QABannerView *bannerView =[[QABannerView alloc]init];
        NSArray *resArr = [self loadLocalBannerData];
        NSMutableArray *arr =[QABannerModel mj_objectArrayWithKeyValuesArray:resArr];
        bannerView.showArr = arr;
        bannerView.bannerClickBlock = ^(QABannerModel * _Nonnull bannerModel) {
            [self JumpToSpeak:bannerModel.title withContent:bannerModel.vText];
        };
        return bannerView;
    }
    return [UIView new];
}

#pragma mark -网络监听
- (void)reachabilityChanged:(NSNotification *)note {
    // 连接改变
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    //处理连接改变后的情况
    NetworkStatus status = [curReach currentReachabilityStatus];
    
    if (status != NotReachable) {
       if(self.openqaModel == nil) [self loadAllRequest];
    }
}

#pragma mark - 悬浮按钮
- (QWDragView *)logoView{
    if(!_logoView){
        _logoView = [[QWDragView alloc] init];
        _logoView.backgroundColor = [UIColor clearColor];
        _logoView.isKeepBounds = YES;
        _logoView.freeRect = CGRectMake(0, [UIDevice navigationFullHeight], kScreenWidth, kScreenHeight-[UIDevice navigationFullHeight]-[UIDevice tabBarFullHeight]);
        _logoView.imageView.image = [UIImage imageNamed:@"drawIcon"];
        _logoView.hidden = YES;
        [[UIApplication sharedApplication].delegate.window addSubview:_logoView];
        
        [_logoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_logoView.superview);
            make.bottom.equalTo(_logoView.superview).offset(-[UIDevice tabBarFullHeight]-20);
            make.size.mas_equalTo(CGSizeMake(70, 70));
        }];
        
        __weak __typeof(self)weakSelf = self;
        _logoView.clickDragViewBlock = ^(QWDragView *dragView){
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if(strongSelf.openqaModel.isCheckCode){
                [strongSelf showCheckCodeTipsAlert];
            }else{
                [strongSelf jumToWebView];
            }
        };
    }
    return _logoView;
}

#pragma mark - 活动 - 跳转
- (void)jumToWebView{
    QWWebviewController *webviewVC =[[QWWebviewController alloc]init];
    NSString *url = [NSString stringWithFormat:@"%@%@",QWM_BASE_API_URL,self.openqaModel.url];
    if([AppProfile isLogined]){
        url = [NSString stringWithFormat:@"%@?userName=%@&password=%@",url,AppProfile.qaUserInfo.phone,AppProfile.qaUserInfo.password];
    }
    webviewVC.url = url;
    webviewVC.isHiddenNav = NO;
    webviewVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webviewVC animated:YES];
}
#pragma mark - 语音播报 - 跳转
- (void)JumpToSpeak:(NSString *)title withContent:(NSString *)content{
    QWSpeakController *speakVC = [[QWSpeakController alloc]init];
    speakVC.titleStr = title;
    speakVC.contentStr = content;
    speakVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:speakVC animated:YES];
}

#pragma mark - 分享 - 跳转
-(void)jumToShare:(QANewsModel *)model{
    [[QWCommonMethod Instance] sharedFromSystem:model.title withUrl:model.url success:^{
        NSLog(@"分享成功");
    } failure:^{
        NSLog(@"分享失败");
    }];
}

#pragma mark - 讨论-添加话题 - 跳转
- (void)addDiscussTopic:(QANewsModel *)model{
    if([AppProfile isLoginedAndShowLogintAlert]){
        QWCommentController *plVC =[[QWCommentController alloc]init];
        plVC.sendType = SendType_addNew_juhe;
        plVC.juheTitleStr = model.title;
        plVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:plVC animated:YES];
    }
}

#pragma mark - 弹框- 验证码
- (void)showCheckCodeTipsAlert{
    QWTipsAlertController *activityVC =[[QWTipsAlertController alloc]init];
    activityVC.tipType = TipsAlertType_checkCode;
    activityVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    activityVC.modalPresentationStyle=UIModalPresentationOverFullScreen;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:activityVC animated:NO completion:nil];
    
    __weak __typeof(activityVC)weakActivityVC = activityVC;
    activityVC.checkCodeView.sureBlock = ^(NSString * _Nonnull code) {
        __strong __typeof(weakActivityVC)strongActivityVC = weakActivityVC;
        [strongActivityVC.checkCodeView checkInput:[code isEqualToString:self.openqaModel.checkCode]];
        if(![code isEqualToString:self.openqaModel.checkCode]) return;
        [self jumToWebView];
    };
}

#pragma mark - 弹框- 系统公告
- (void)showSystemNoticeTipsAlert{
    QWTipsAlertController *activityVC =[[QWTipsAlertController alloc]init];
    activityVC.tipType = TipsAlertType_home_picNotice;
//    activityVC.picNotice.picUrl = @"home_notice.png";
    activityVC.picNotice.picUrl = self.openqaModel.noticePicUrl;
//    activityVC.picNotice.picUrl =@"https://img0.baidu.com/it/u=3425868493,3104015061&fm=253&fmt=auto&app=120&f=JPEG?w=1199&h=800";
    activityVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    activityVC.modalPresentationStyle=UIModalPresentationOverFullScreen;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:activityVC animated:NO completion:nil];
    
    __weak __typeof(self)weakSelf = self;
    activityVC.picNotice.sureBlock = ^{
        if(IsStringEmpty(weakSelf.openqaModel.url)) return; // 没有链接，不跳转
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if(strongSelf.openqaModel.isCheckCode){
            [strongSelf showCheckCodeTipsAlert];
        }else{
            [strongSelf jumToWebView];
        }
    };
}

#pragma mark - 所有-请求
- (void) loadAllRequest{
    [self loadJuHeNewsData];
    [self logoData];
}

#pragma mark - 悬浮按钮-请求
- (void)logoData{
    NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
    if([AppProfile isLogined]){
        [dic setValue:AppProfile.qaUserInfo.phone forKey:@"userName"];
    }
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_Resfile_Openqa argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        if(success && [result[@"code"] intValue] != 0) return;
//        NSDictionary *result1 =@{
//            @"url":@"boke/index.jsp",
//            @"isShowNoticeDialog":@true,
//            @"isCheckCode":@false,
//            @"checkCode":@"888",
//            @"type":@"2",
//            @"noticePicUrl":@"http://gaiwo.belovedlive.com:8098/boke/resfile/qaEnter/SysWelcome_SX_meet_6.2.jpeg"
//
//        };
        
        QWOpenqaModel *model = [QWOpenqaModel mj_objectWithKeyValues:result];
        self.openqaModel = model;
        if([model.type isEqualToString:@"1"]){ //悬浮按钮
            self.logoView.hidden = !model.isShowNoticeDialog;
        }else if ([self.openqaModel.type isEqualToString:@"2"]){//图片公告
            if(model.isShowNoticeDialog) [self showSystemNoticeTipsAlert];
        }
         
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        NSLog(@"%@",errorInfo);
        [self repeatLoadLogoData];
    }];
}

/*
 *获取失败（网络异常/app首次安装）
 *循环加载数据
 *十次 每次五秒
 **/
- (void)repeatLoadLogoData{
    static NSInteger num = 10;
    if(self.openqaModel == nil && num >0){
        num = num-1;
        NSLog(@"num==%ld",num);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self logoData];
        });
    }
}

#pragma mark - 聚合数据 - 请求
-(NSMutableArray *)showInfoArr
{
    if (!_showInfoArr) {
        _showInfoArr=[[NSMutableArray alloc]init];
    }
    return _showInfoArr;
}
- (void)loadJuHeNewsData{
    
    [self loadLocalJuHeNewsData];
    
    NSString *dateStr = [[QWCommonMethod Instance] changeDateToString:[NSDate date] withFormatter:@"YYYY-MM-dd"];
    NSString *default_key = [NSString stringWithFormat:@"%@%@",URL_JUHE_News,dateStr];
    NSArray *dataArr = [[NSUserDefaults standardUserDefaults] objectForKey:default_key];
    if(dataArr.count >0){
        [SWHUDUtil hideHudView];
        //        self.showInfoArr = [QANewsModel mj_objectArrayWithKeyValuesArray:dataArr];
        //        [_tableView reloadData];
    }else {
        HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_JUHE_News argument:[NSDictionary dictionary]];
        [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
            if([result[@"error_code"] intValue] ==0){
                //                NSDictionary *resDic =result[@"result"];
                //                NSArray *arr =resDic[@"data"];
                //                [[NSUserDefaults standardUserDefaults] setObject:arr forKey:default_key];
                //                [self->_showInfoArr removeAllObjects];
                //                [self->_showInfoArr addObjectsFromArray:[QANewsModel mj_objectArrayWithKeyValuesArray:arr]];
                //                [self->_tableView reloadData];
            }
            [SWHUDUtil hideHudView];
        } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
            [SWHUDUtil hideHudView];
        }];
    }
}

-(void)loadLocalJuHeNewsData{
    // 获取文件路径
    NSString *path = [[NSBundle mainBundle] pathForResource:@"juheNews" ofType:@"json"];
    // 将文件数据化
    NSData *data = [[NSData alloc] initWithContentsOfFile:path];
    // 对数据进行JSON格式化并返回字典形式
    NSDictionary *result =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    NSDictionary *resDic =result[@"result"];
    NSArray *arr =resDic[@"data"];
    
    [self.showInfoArr removeAllObjects];
    [self.showInfoArr addObjectsFromArray:[QANewsModel mj_objectArrayWithKeyValuesArray:arr]];
    [self.tableView reloadData];
    
    
    //第一屏cell上的图片不显示，延迟重新绘制，暂时使用该方法解决
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

#pragma mark - banner数据-本地
-(NSArray *)loadLocalBannerData{
    // 获取文件路径
    NSString *path = [[NSBundle mainBundle] pathForResource:@"homeBanner" ofType:@"json"];
    // 将文件数据化
    NSData *data = [[NSData alloc] initWithContentsOfFile:path];
    // 对数据进行JSON格式化并返回字典形式
    NSArray *arr =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    return arr;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
