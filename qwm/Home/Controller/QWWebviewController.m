//
//  QWWebviewController.m
//  qwm
//
//  Created by kevin on 2023/3/30.
//

#import "QWWebviewController.h"
#import <WebKit/WebKit.h>

static NSString* const user_index_do = @"boke/user_index.do";

@interface QWWebviewController ()<WKNavigationDelegate>
@property (nonatomic,strong) WKWebView *webView;

@property (nonatomic, copy) NSString *rightBarButtonItem_reloadUrl; //导航栏右-点击回到web首页
@end

@implementation QWWebviewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = self.isHiddenNav;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"qa2023";
    self.isHiddenLeftBarButtonItem = YES;
    self.rightBarButtonItem_image = @"ic_web_home.png";
    self.isHiddenRightBarButtonItem = YES;
    
    [self webView];
}

#pragma mark-导航条左边按钮
-(void)navLeftBaseButtonClick
{
    if(self.isHiddenLeftBarButtonItem) return;
    if([self.webView canGoBack]){
        [self.webView goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
    if(self.isHiddenRightBarButtonItem) return;
        
    NSString *reloadurlStr = IsStringEmpty(self.rightBarButtonItem_reloadUrl)?self.url:self.rightBarButtonItem_reloadUrl;
    NSURLRequest *request =[[NSURLRequest alloc]initWithURL:[NSURL URLWithString:reloadurlStr] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
    [_webView loadRequest:request];
}


#pragma mark -网页
-(WKWebView *)webView
{
    if (!_webView) {
        _webView =[[WKWebView alloc]init];
        _webView.navigationDelegate=self;
        _webView.scrollView.showsVerticalScrollIndicator=NO;
        _webView.scrollView.showsHorizontalScrollIndicator=YES;
        NSURLRequest *request =[[NSURLRequest alloc]initWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
        [_webView loadRequest:request];
        [self.view addSubview:_webView];
        
        if ([_webView.scrollView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
            _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
             
        if(!self.isHiddenNav) {
            [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.view);
                make.right.equalTo(self.view);
                if (@available(iOS 13.0, *)) {
                    make.top.equalTo(self.view).offset([UIDevice navigationFullHeight]);
                }else{
                    make.top.equalTo(self.view);
                }
                make.bottom.equalTo(self.view).offset(-[UIDevice safeDistanceBottom]);
            }];
        }else{
            [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.view);
                make.right.equalTo(self.view);
                make.top.equalTo(self.view);
                make.bottom.equalTo(self.view).offset(-[UIDevice safeDistanceBottom]);
            }];
        }
    }
    return _webView;
}
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
//    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@""];
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
}
#pragma mark - KKWebViewDelegate

- (void)webView:(WKWebView*)webView decidePolicyForNavigationAction:(WKNavigationAction*)navigationAction
decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    self.isHiddenLeftBarButtonItem = NO;
    self.isHiddenRightBarButtonItem = NO;
    NSString *loadingUrl = navigationAction.request.URL.absoluteString;
    if([loadingUrl containsString:user_index_do] || [loadingUrl containsString:self.url]){
        self.isHiddenLeftBarButtonItem = YES;
        self.isHiddenRightBarButtonItem = YES;
        
        if([loadingUrl containsString:user_index_do]){
            self.rightBarButtonItem_reloadUrl = loadingUrl;
        }
    }

    decisionHandler(WKNavigationActionPolicyAllow);
}

// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
//    [SWHUDUtil hideHudView];
    
    
//    //获取所有的html
//
//    NSString *allHtml = @"document.documentElement.innerHTML";
//
//    //获取网页title
//
//    NSString *htmlTitle = @"document.title";
//
//    //获取网页的一个值
//
//    NSString *htmlNum = @"document.getElementById('title').innerText";
//
//    //执行JS方法获取导航栏标题
//      [webView evaluateJavaScript:allHtml completionHandler:^(id _Nullable title, NSError * _Nullable error) {
//
//          NSLog(@"%@",title);
//          self.title =title;
//      }];
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
{
//    [SWHUDUtil hideHudView];
}


@end
