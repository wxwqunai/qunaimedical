//
//  QWSpeakController.h
//  qwm
//
//  Created by kevin on 2023/5/5.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWSpeakController : QWBaseController
@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, copy) NSString *contentStr;
@end

NS_ASSUME_NONNULL_END
