//
//  QWBannerDetailController.h
//  qwm
//
//  Created by kevin on 2023/4/14.
//

#import "QWBaseController.h"
#import "QWHomeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWBannerDetailController : QWBaseController
@property (nonatomic, strong) QABannerModel *bannerModel;
@end

NS_ASSUME_NONNULL_END
