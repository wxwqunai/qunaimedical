//
//  QWBannerDetailController.m
//  qwm
//
//  Created by kevin on 2023/4/14.
//

#import "QWBannerDetailController.h"

@interface QWBannerDetailController ()<UIScrollViewDelegate>
@property (nonatomic,strong) UIScrollView *bgScroolView;
@property (nonatomic,strong) UIView *containerView;

@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *topImageView;
@end

@implementation QWBannerDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"详情";
    [self bgScroolView];
}
-(UIScrollView *)bgScroolView
{
    if (!_bgScroolView) {
        _bgScroolView =[[UIScrollView alloc]init];
        _bgScroolView.alwaysBounceVertical=YES;
        _bgScroolView.backgroundColor =[UIColor whiteColor];
        _bgScroolView.delegate = self;
//        _bgScroolView.contentSize=CGSizeMake(0, kScreenHeight-[UIDevice statusBarHeight]);
        [self.view addSubview:_bgScroolView];
        
        [_bgScroolView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view).offset([UIDevice statusBarHeight]);
            make.bottom.equalTo(self.view);
        }];
        
        //scrollview上覆盖一个view
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor clearColor];
        [_bgScroolView addSubview:_containerView];
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_bgScroolView);
            make.width.equalTo(_bgScroolView); // 需要设置宽度和scrollview宽度一样
        }];

        //覆盖view上添加
        [self topImageView];
        [self contentLabel];
        
        // 这里放最后一个view的底部
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
             make.bottom.equalTo(_contentLabel.mas_bottom).offset(20);
         }];
    }
    return _bgScroolView;
}

#pragma mark - 头像
- (UIImageView *)topImageView{
    if(!_topImageView){
        _topImageView = [[UIImageView alloc]init];
        UIImage *image =[UIImage imageNamed:self.bannerModel.image];
        _topImageView.image =image;
        [_containerView addSubview:_topImageView];
        
        CGFloat height = 736 == [[UIScreen mainScreen] bounds].size.height ? 330 : 300;
        [_topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_containerView);
            make.right.equalTo(_containerView);
            make.top.equalTo(_containerView);
            make.height.mas_equalTo(height);
        }];
    }
    return _topImageView;
}

//字数显示
-(UILabel *)contentLabel
{
    if (!_contentLabel) {
        _contentLabel=[[UILabel alloc]init];
        _contentLabel.textColor = [UIColor black25PercentColor];
        _contentLabel.font=[UIFont systemFontOfSize:15];
        _contentLabel.numberOfLines = 0;
//        _contentLabel.text=self.detailModel.content;
        [_containerView addSubview:_contentLabel];
        
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(18);
            make.right.mas_equalTo(-18);
            make.top.equalTo(_topImageView.mas_bottom);
            make.bottom.mas_equalTo(-20);
        }];
    }
    return _contentLabel;
}



@end
