//
//  QWWebviewController.h
//  qwm
//
//  Created by kevin on 2023/3/30.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWWebviewController : QWBaseController
@property (nonatomic,copy) NSString *shareTitle;
@property (nonatomic,copy) NSString *url;
@property (nonatomic, assign) BOOL isHiddenNav;

@end

NS_ASSUME_NONNULL_END
