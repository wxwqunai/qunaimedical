//
//  QWSpeakController.m
//  qwm
//
//  Created by kevin on 2023/5/5.
//

#import "QWSpeakController.h"
#import <WebKit/WebKit.h>
#import "SpeechSynthesizerManager.h"

@interface QWSpeakController ()<CAAnimationDelegate>
@property (nonatomic, strong) WKWebView *gifView;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView *txtView;
@property(nonatomic,strong)UILabel *txtLabel;

//显示到第几个
@property(nonatomic,assign)NSInteger arrNum;

@end

@implementation QWSpeakController
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!IsStringEmpty(self.contentStr)){
        [SpeechSynthesizerManager.shared stopSpeaking];
    }
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self customNavigationBarWithBgColor:[UIColor clearColor] withTitleColor:[UIColor clearColor]];
    self.leftBarButtonItem.tintColor = [UIColor blackColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = self.titleStr;
    [self gifView];
    
    [self titleLabel];
    [self txtView];
    
    [self start];
}

#pragma mark - gif图
- (WKWebView *)gifView{
    if(!_gifView){
        _gifView = [[WKWebView alloc] init];
        _gifView.scrollView.scrollEnabled = NO;
        _gifView.scrollView.contentInset = UIEdgeInsetsMake(-[UIDevice navigationFullHeight], 0, 0, 0);
        NSString *gifName = self.titleStr.length%2 == 1?@"zhuchia01_ani":@"zhuchia02_ani";
        NSString *filePath = [[NSBundle bundleWithPath:[[NSBundle mainBundle] bundlePath]] pathForResource:gifName ofType:@"gif"];
        NSData *imageData = [NSData dataWithContentsOfFile:filePath];
        [_gifView loadData:imageData MIMEType:@"image/gif" characterEncodingName:@"" baseURL:[NSURL URLWithString:@""]];
        
        [self.view addSubview:_gifView];

        [_gifView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.view);
        }];
    }
    return _gifView;
}

#pragma mark - 标题
- (UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.numberOfLines = 2;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = self.titleStr;
        _titleLabel.font = [UIFont systemFontOfSize:20];
        _titleLabel.textColor = [UIColor blackColor];
        [self.view addSubview:_titleLabel];

        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(30);
            make.right.equalTo(self.view).offset(-30);
            make.top.equalTo(self.view).offset([UIDevice statusBarHeight]);
            make.height.mas_greaterThanOrEqualTo(44.0);
        }];
    }
    return _titleLabel;
}

#pragma mark - 滚动文本
- (UIView *)txtView{
    if(!_txtView){
        _txtView = [[UIView alloc]init];
        _txtView.backgroundColor = [UIColor clearColor];
        _txtView.clipsToBounds = YES;
        [self.view addSubview:_txtView];
        
        [_txtView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.height.mas_equalTo(kScreenHeight/2.0);
        }];
        
        [self txtLabel];
    }
    return _txtView;
}

-(UILabel *)txtLabel{
    if (!_txtLabel) {
        _txtLabel=[UILabel new];
        _txtLabel.font=[UIFont systemFontOfSize:20];
        _txtLabel.textAlignment = NSTextAlignmentCenter;
        _txtLabel.numberOfLines=0;
       
        _txtLabel.textColor=[UIColor blackColor];
        [_txtView addSubview:_txtLabel];
        
        [_txtLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_txtView).offset(14);
            make.right.equalTo(_txtView).offset(-14);
            make.top.equalTo(_txtView.mas_bottom);
        }];
    }
    return _txtLabel;
}

- (void)scrollTxtAction {
    
    CGSize size =[[QWCommonMethod Instance] boundingRectWithSize:CGSizeMake(kScreenWidth, MAXFLOAT) withLabelStr:self.contentStr WithFontSize:20];

    //告知需要更改约束
    [self.view setNeedsUpdateConstraints];
    NSTimeInterval duration = self.contentStr.length/20.0 < 5.0?5.0:self.contentStr.length/20.0;
    if(size.height > kScreenHeight/2.0){
        [UIView animateWithDuration:duration delay:0 options:(UIViewAnimationOptionCurveLinear | UIViewAnimationOptionRepeat) animations:^{

            [self->_txtLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self->_txtView).offset(14);
                make.right.equalTo(self->_txtView).offset(-14);
                make.bottom.equalTo(self->_txtView.mas_bottom).offset(-100);
            }];
            //告知父类控件绘制，不添加注释的这两行的代码无法生效
            [self->_txtLabel.superview layoutIfNeeded];

        } completion:^(BOOL finished) {
            
    //        [self->_txtLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
    //            make.left.equalTo(self->_txtView).offset(14);
    //            make.right.equalTo(self->_txtView).offset(-14);
    //            make.top.equalTo(self->_txtView.mas_bottom);
    //        }];
    //        //告知父类控件绘制，不添加注释的这两行的代码无法生效
    //        [self->_txtLabel.superview layoutIfNeeded];
    //        [self scrollTxtAction];
        }];
    }else{
        CGFloat topOffset = kScreenHeight/2.0 - [UIDevice safeDistanceBottom]- size.height;
        [UIView animateWithDuration:duration delay:0 options:(UIViewAnimationOptionCurveLinear ) animations:^{

            [self->_txtLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self->_txtView).offset(14);
                make.right.equalTo(self->_txtView).offset(-14);
                if(topOffset>0){
                    //内容较少，居底
                    make.bottom.equalTo(self->_txtView).offset(-[UIDevice safeDistanceBottom]-10);
                }else{
                    //内容较多，居中
                    make.centerY.equalTo(self->_txtView);
                }
            }];
            //告知父类控件绘制，不添加注释的这两行的代码无法生效
            [self->_txtLabel.superview layoutIfNeeded];

        } completion:^(BOOL finished) {
        }];
    }
}

-(void)start{
    if(!IsStringEmpty(self.contentStr)){
        self.txtLabel.text =self.contentStr;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SpeechSynthesizerManager.shared speak:self.contentStr];
            [self scrollTxtAction];
        });
    }
}



@end
