//
//  UICollectionView+XX.m
//  qwm
//
//  Created by kevin on 2023/4/26.
//

#import "UICollectionView+XX.h"

@implementation UICollectionView (XX)
- (void)addLoadMoreFooterWithTarget:(id)target action:(SEL)action {
    
    NSMutableArray *loadMoreImages = [NSMutableArray array];
    
    //    for (NSUInteger i = 0; i < 25; i++) {
    //        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"load_%zd", i]];
    //        [loadMoreImages addObject:image];
    //    }
    
    if (loadMoreImages.count) { //>0表示gif,=0表示默认
        MJRefreshAutoGifFooter *footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:target refreshingAction:action];
        [footer setImages:loadMoreImages forState:MJRefreshStateRefreshing];
        self.mj_footer = footer;
    } else {
        MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:target refreshingAction:action];
        self.mj_footer = footer;
    }
    
}

- (void)addRefreshHeaderWithTarget:(id)target action:(SEL)action {
    //设置普通状态的动画图片
    NSMutableArray *idleImages = [NSMutableArray array];
    //设置即将刷新状态的动画图片（一松开就会刷新的状态）
    NSMutableArray *refreshingImages = [NSMutableArray array];
    //设置下拉到顶端时的动画图片
    for (NSUInteger i = 1; i<=20; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"dropdown_anim__000%zd", i]];
        if (image) {
            [idleImages addObject:image];
        }
    }
    for (NSUInteger i = 1; i<=1; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"dropdown_loading_0%zd", i]];
        if (image) {
            [refreshingImages addObject:image];
        }
    }
    
    if (idleImages.count==20) {
        MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingTarget:target refreshingAction:action];
        // 设置普通状态的动画图片
        [header setImages:refreshingImages forState:MJRefreshStateIdle];
        // 设置即将刷新状态的动画图片（一松开就会刷新的状态）
        [header setImages:@[[UIImage imageNamed:@"dropdown_loading_01"]] forState:MJRefreshStatePulling];
        // 设置正在刷新状态的动画图片
        //[header setImages:idleImages forState:MJRefreshStateRefreshing];
        [header setImages:idleImages duration:idleImages.count * 0.05 forState:MJRefreshStateRefreshing];
        // 设置header
        // 隐藏时间
        header.lastUpdatedTimeLabel.hidden = YES;
        // 隐藏状态
        header.stateLabel.hidden = YES;
        self.mj_header = header;
    }
    else {
        MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:target refreshingAction:action];
        // 隐藏时间
        header.lastUpdatedTimeLabel.hidden = YES;
        self.mj_header = header;
    }
}

- (void)beginRefresh {
    // 马上进入刷新状态
    [self.mj_header beginRefreshing];
}

-(void) setHeadRefreshTip:(NSString *)pull2Refresh release2Refresh:(NSString *)release2Refresh refreshing:(NSString *)refreshing
{
    [(MJRefreshStateHeader *)self.mj_header setTitle:pull2Refresh forState:MJRefreshStateIdle];
    [(MJRefreshStateHeader *)self.mj_header setTitle:release2Refresh forState:MJRefreshStatePulling];
    [(MJRefreshStateHeader *)self.mj_header setTitle:refreshing forState:MJRefreshStateRefreshing];
}

- (void)endRefreshing {
    [self.mj_footer endRefreshing];
    [self.mj_header endRefreshing];
}

- (void)removeHeader {
    self.mj_header.hidden=YES;
}

- (void)removeFooter {
    self.mj_footer.hidden = YES;
}
- (void)addFooter {
    self.mj_footer.hidden = NO;
}
#pragma mark - 刷新加载(消除“点击加载更多”)
-(void)MJRefresh:(UITableView *)table begin:(NSInteger)begin count:(NSInteger)count mutableArray:(NSMutableArray *)arr
{
    self.mj_footer.hidden =YES;//先消除“点击加载更多”
    //如果每次最大获取数正好为20的倍数，还需再加载，直到根据判断开始数量和最终获取的数量相同则不需要“点击加载更多”。
    if (begin ==arr.count) {
        self.mj_footer.hidden =YES;
    }else
    {
        if (arr.count ==count) {
            self.mj_footer.hidden =NO;
        }
    }
}

#pragma mark -判断删除/添加上拉刷新
- (void)judgeFooterState:(BOOL)isShouldRemove
{
    [self endRefreshing];
    [self addFooter];
    if (isShouldRemove) {
        [self removeFooter];
    }
}
@end
