//
//  UICollectionView+XX.h
//  qwm
//
//  Created by kevin on 2023/4/26.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"

NS_ASSUME_NONNULL_BEGIN

@interface UICollectionView (XX)

/**
 上拉加载
 */
- (void)addLoadMoreFooterWithTarget:(id)target action:(SEL)action;

/**
 下拉刷新
 */
- (void)addRefreshHeaderWithTarget:(id)target action:(SEL)action;

/**
 首次进controller自动刷新
 */
- (void)beginRefresh;

/**
 设置下拉刷新的提示文字
 */
-(void) setHeadRefreshTip:(NSString*) pull2Refresh release2Refresh:(NSString*) release2Refresh refreshing:(NSString*) refreshing;

/**
 结束动画
 */
- (void)endRefreshing;

- (void)removeHeader;
- (void)removeFooter;

- (void)addFooter;

#pragma mark - 刷新加载(消除“点击加载更多”)
//-(void)MJRefresh:(UITableView *)table begin:(NSInteger)begin count:(NSInteger)count mutableArray:(NSMutableArray *)arr;

#pragma mark -判断删除/添加上拉刷新
- (void)judgeFooterState:(BOOL)isShouldRemove;

@end

NS_ASSUME_NONNULL_END
