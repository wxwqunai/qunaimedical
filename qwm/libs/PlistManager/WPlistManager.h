//
//  WPlistManager.h
//  qwm
//
//  Created by kevin on 2023/5/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WPlistManager : NSObject
+(instancetype)SharedInstance;


//论坛
- (void)addConversationsWith:(NSDictionary *)dic; //增
- (NSArray *)getConversations;//查
- (void)deleteConversationsWith:(NSDictionary *)dic;//删

@end

NS_ASSUME_NONNULL_END
