//
//  WPlistManager.m
//  qwm
//
//  Created by kevin on 2023/5/9.
//

#import "WPlistManager.h"

static NSString* const conversationsPath = @"Conversations";

@implementation WPlistManager

static WPlistManager *_sharedInstance = nil;
static dispatch_once_t once_token = 0;
+(instancetype)SharedInstance {
    dispatch_once(&once_token, ^{
        if (_sharedInstance == nil) {
            _sharedInstance = [[super allocWithZone:NULL] init] ;
        }
    });
    return _sharedInstance;
}


- (NSString *)filepath:(NSString *)fileName{
    NSString * documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    //也可以这样添加后缀
    NSString * plistName = [[NSString stringWithFormat:@"%@", fileName]stringByAppendingPathExtension:@"plist"];
    NSString * plistPath =[documentPath stringByAppendingPathComponent:plistName];
    return plistPath;
}


//#pragma mark - 论坛-屏蔽内容-增
//- (void)addConversationsWith:(NSDictionary *)dic{
//    NSString *filePath= [self filepath:@"conversations"];
//
//    NSDictionary *plstDic = [self getConversations];
//    if(!plstDic){
//        //将数组写入文件
//        [dic writeToFile:filePath atomically:YES];
//
//    }else {
//        NSMutableDictionary *chanDic = [NSMutableDictionary dictionaryWithDictionary:plstDic];
//        [chanDic addEntriesFromDictionary:dic];
//
//        //将数组写入文件
//        [chanDic writeToFile:filePath atomically:YES];
//    }
//}
//
//#pragma mark - 论坛-屏蔽内容-查
//- (NSDictionary *)getConversations{
//    NSString *filePath= [self filepath:@"conversations"];
//    //读取文件
//    NSDictionary *lookdict = [NSDictionary dictionaryWithContentsOfFile:filePath];
//    return lookdict;
//}

#pragma mark - 论坛-屏蔽内容-增
- (void)addConversationsWith:(NSDictionary *)dic{
    NSString *filePathName = IsStringEmpty(AppProfile.qaUserInfo.name)?conversationsPath:AppProfile.qaUserInfo.name;
    NSString *filePath= [self filepath:filePathName];
    
    NSArray *plstArr = [self getConversations];
    if(!plstArr){
        //将数组写入文件
        NSMutableArray *newArr = [NSMutableArray new];
        [newArr addObject:dic];
        [newArr writeToFile:filePath atomically:YES];
    }else {
        NSMutableArray *mutableArray = [plstArr mutableCopy];
        //采用逆序遍历 防止删除元素时crash
        [mutableArray enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj[@"id"] == dic[@"id"]) {
                [mutableArray removeObject:obj];
            }
        }];
        [mutableArray addObject:dic];
        plstArr = mutableArray;
        [plstArr writeToFile:filePath atomically:YES];
    }
}

#pragma mark - 论坛-屏蔽内容-查
- (NSArray *)getConversations{
    NSString *filePathName = IsStringEmpty(AppProfile.qaUserInfo.name)?conversationsPath:AppProfile.qaUserInfo.name;
    NSString *filePath= [self filepath:filePathName];
    //读取文件
    NSArray *plistArr = [NSArray arrayWithContentsOfFile:filePath];
    
    return plistArr;
}

#pragma mark - 论坛-屏蔽内容-删
- (void)deleteConversationsWith:(NSDictionary *)dic{
    NSString *filePathName = IsStringEmpty(AppProfile.qaUserInfo.name)?conversationsPath:AppProfile.qaUserInfo.name;
    NSString *filePath= [self filepath:filePathName];
    
    NSArray *plstArr = [self getConversations];
    if(plstArr){
        NSMutableArray *mutableArray = [plstArr mutableCopy];
        //采用逆序遍历 防止删除元素时crash
        [mutableArray enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj[@"id"] == dic[@"id"]) {
                [mutableArray removeObject:obj];
            }
        }];
        plstArr = mutableArray;
        [plstArr writeToFile:filePath atomically:YES];
    }
}



@end
