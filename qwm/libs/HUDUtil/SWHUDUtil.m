//
//  SWHUDUtil.m
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import "SWHUDUtil.h"
#define ic_hud_success  @"ic_hud_success"

static MBProgressHUD *hud;
static MBProgressHUD *hudDelay;//定时隐藏
@implementation SWHUDUtil

+(void) showHudViewInSuperView:(UIView *)view withMessage:(NSString *)message
{
    
//    UIImage  *image=[UIImage sd_animatedGIFNamed:@"hudwait"];
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
//    imageView.image =image;
//    hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
//    hud.mode = MBProgressHUDModeCustomView;
//    hud.customView = imageView;
//    hud.labelText = message;
//    hud.labelFont=[ZBCOMMETH customFontWithName:FONTPingFangSCRegular size:15];
//    hud.animationType = MBProgressHUDAnimationZoomOut;
    
    hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = message;
}

+(void) showHudViewTipInSuperView:(UIView *)view withMessage:(NSString *)message
{
    if (!view) return;
    hudDelay = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hudDelay.mode = MBProgressHUDModeText;
    hudDelay.animationType = MBProgressHUDAnimationZoomOut;
    hudDelay.labelText = message;
    [hudDelay hide:YES afterDelay:kTipDelay];
}
//多行显示
+(void) showHudlinesWithMessage:(NSString *)message
{
    hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.animationType = MBProgressHUDAnimationZoomOut;
    hud.detailsLabelText = message;
    hud.square = NO;
    [hud hide:YES afterDelay:kTipDelay];
}
//全屏铺满
+(void) showFullScreenWithMessage:(NSString *)message
{
    hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    hud.labelText = message;
    [hud show:YES];
}

+(void) hideHudView
{
    if (hud)
    {
        [hud hide:YES];
        hud = nil;
    }
}

+(void) hideHudViewWithSuccessMessage:(NSString *)message
{
    if (!hud)
    {
        return;
    }
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:ic_hud_success]];
    hud.labelText = message;
    hud.animationType = MBProgressHUDAnimationZoomOut;
    [hud hide:YES afterDelay:kTipDelay];
}

+(void) hideHudViewWithSuccessMessageSuperView:(UIView *)view withMessage:(NSString *)message
{
    hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:ic_hud_success]];
    hud.labelText = message;
    hud.animationType = MBProgressHUDAnimationZoomOut;
    [hud hide:YES afterDelay:kTipDelay];
}

+(void) hideHudViewWithFailureMessage:(NSString *)message
{
    if (!hud)
    {
        return;
    }
    hud.mode = MBProgressHUDModeText;
    hud.labelText = message;
    hud.animationType = MBProgressHUDAnimationZoomOut;
    [hud hide:YES afterDelay:kTipDelay];
}
+(void) hideHudViewWithMessageSuperView:(UIView *)view withMessage:(NSString *)message
{
    if (!view) return;
    hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = message;
    hud.animationType = MBProgressHUDAnimationZoomOut;
    [hud hide:YES afterDelay:kTipDelay];
}

+(void) showCustomView:(UIView *)customView byReduceDeltaY:(CGFloat)deltaY message:(NSString *)message
{
    hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = customView;
    hud.labelText = message;
    hud.yOffset = deltaY;
    [hud show:YES];
}

+(void) updateLabelText:(NSString *)labelText
{
    if (hud)
    {
        hud.labelText = labelText;
    }
}
//+(void) showAnimationMessageSuperView:(UIView *)view withMessage:(NSString *)message
//{
//    UIImage  *image=[UIImage sd_animatedGIFNamed:@"hudwait"];
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
//    imageView.image =image;
//
//    hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
//    hud.mode = MBProgressHUDModeCustomView;
//    hud.customView = imageView;
//    hud.labelText = message;
//    hud.animationType = MBProgressHUDAnimationZoomOut;
//}
@end
