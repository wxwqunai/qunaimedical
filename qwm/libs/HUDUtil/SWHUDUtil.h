//
//  SWHUDUtil.h
//  qwm
//
//  Created by kevin on 2023/3/23.
//


#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#define kTipDelay           1.5

@interface SWHUDUtil : NSObject

/**
 @author wjd
 
 @brief  显示文字进度提示
 @param view    进度提示显示的容器
 @param message 进度提示消息
 */
+(void) showHudViewInSuperView:(UIView *) view withMessage:(NSString *)message;

/**
 @author wjd
 
 @brief  显示文字提示，并定时隐藏
 @param view    提示显示的容器
 @param message 提示消息
 */
+(void) showHudViewTipInSuperView:(UIView *) view withMessage:(NSString*) message;

/**
 @author wjd
 
 @brief  隐藏提示
 */
+(void) hideHudView;

/**
 @author wjd
 
 @brief  显示成功提示，并定时隐藏
 @param message 提示消息
 */
+(void) hideHudViewWithSuccessMessage:(NSString*) message;
+(void) hideHudViewWithSuccessMessageSuperView:(UIView *)view withMessage:(NSString *)message;
/**
 @author wjd
 
 @brief  显示失败提示，并定时隐藏
 @param message 提示消息
 */
+(void) hideHudViewWithFailureMessage:(NSString *)message;
+(void) hideHudViewWithMessageSuperView:(UIView *)view withMessage:(NSString *)message;
+(void) showHudlinesWithMessage:(NSString *)message;//多行显示
+(void) showFullScreenWithMessage:(NSString *)message;//全屏铺满
/**
 @author wjd
 
 @brief  显示自定义界面提示
 @param cursomView 自定义界面
 @param deltaY     y方向上的偏移量
 @param message    提示
 */
+(void) showCustomView:(UIView*) cursomView byReduceDeltaY:(CGFloat) deltaY message:(NSString*) message;

/**
 @author wjd
 
 @brief  更新提示
 @param labelText 提示
 */
+(void) updateLabelText:(NSString*) labelText;
///**
// @author wxw
// 
// @brief  显示动画gif
// @param view    提示显示的容器
// @param message 提示消息
// */
//+(void) showAnimationMessageSuperView:(UIView *)view withMessage:(NSString *)message;
@end
