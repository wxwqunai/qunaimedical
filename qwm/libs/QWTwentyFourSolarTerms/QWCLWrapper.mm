//
//  CLWrapper.m
//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import "QWCLWrapper.h"
#include "QWCalendartool.h"
#include "JieqiDataModel.h"

@implementation CLWrapper
+(NSMutableArray*)calculateAllJieqiByYear:(int)year fromJieqiId:(int)fromJieqiId{
    NSMutableArray* array = [NSMutableArray arrayWithCapacity:24];
    
    double jieqiTimes[25];
    CalendarTool::calculateAllJieqiTime(year, jieqiTimes, fromJieqiId);
    
    for(int i=0; i<24; i++){
        char buffer[128];
        double jdTime = jieqiTimes[i];
        CalendarTool::jdTimeToString(jdTime, buffer, 128);
        
        JieqiDataModel* jdModel = [[JieqiDataModel alloc] init];
        jdModel.jieqiTime    = [NSString stringWithCString:buffer encoding:NSUTF8StringEncoding];
        jdModel.jieqiName    = [NSString stringWithCString:CalendarTool::jieqiNameById(i+fromJieqiId) encoding:NSUTF8StringEncoding];
        jdModel.numOfAllJieqi=i;
        [array addObject:jdModel];
    }
    
    return array;
}
@end
