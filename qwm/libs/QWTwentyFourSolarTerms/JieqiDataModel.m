//
//  JieqiData.m
//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import "JieqiDataModel.h"

@implementation JieqiDataModel
-(void)setJieqiTime:(NSString *)jieqiTime
{
    _jieqiTime=jieqiTime;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"YYYY-MM-dd HH:mm:ss.S";
    _jieqiDate =[dateFormatter dateFromString:_jieqiTime];
}
@end
