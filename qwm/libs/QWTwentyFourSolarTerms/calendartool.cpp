#include "QWCalendartool.h"

#include "QWAlgorithm/calendar_func.h"
#include "QWAlgorithm/support.h"
#include "QWAlgorithm/calendar_const.h"
#include "QWAlgorithm/Names.h"

CalendarTool::CalendarTool()
{

}

double CalendarTool::calculateJieqiTimeById(int year, int jieqiId)
{
    double jd = CalculateSolarTerms(year, jieqiId * 15); // 360/24 = 15
    double localTime = JDTDtoLocalTime(jd);
    return localTime;
}

void CalendarTool::jdTimeToString(double jdTime, char *buffer, int bufferSize)
{
    JDtoString(jdTime, buffer, bufferSize);
}

// 从某一年的某个节气开始，连续计算25个节气，返回各节气的儒略日(jdTime)，本地时间
void CalendarTool::calculateAllJieqiTime(int year, double *jieqiTime, int start){
    int i = 0;
    int st = start;
    while(i < 25){
        double jd = CalculateSolarTerms(year, st * 15);
        jieqiTime[i++] = JDTDtoLocalTime(jd);
        if(st == WINTER_SOLSTICE){
            year++;
        }
        st = (st + 1) % SOLAR_TERMS_COUNT;
    }
}

const char *CalendarTool::jieqiNameById(int jieqiId)
{
    return nameOfJieQi[jieqiId % SOLAR_TERMS_COUNT];
}
