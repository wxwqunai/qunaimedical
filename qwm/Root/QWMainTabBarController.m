//
//  QWMainTabBarController.m
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import "QWMainTabBarController.h"
#import "QWNavgationController.h"

@interface QWMainTabBarController ()<UITabBarControllerDelegate>

@end

@implementation QWMainTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UITabBar appearance] setTintColor:Color_Main_Green]; // 修改系统默认的tabBarItem的颜色
    
    [self setTabbarBackGround];

    [self initUI];
}

- (void)initUI {
    
    self.delegate = self;
    
    NSArray *titles = @[@"首页",
//                        @"资讯",
                        @"消息",
                        @"我的"];
    
    NSArray *className = @[
        @"QWHomeController",
//        @"QWHealthController",
        @"QWConversationsController",
        @"QWMineController"
    ];
    NSArray *defaultImageNames = @[
        @"qw_tab_home",
//        @"qw_tab_quan",
        @"qw_tab_chat",
        @"qw_tab_me"
    ];
    NSArray *selectedImageNames = @[
        @"qw_tab_home",
//        @"qw_tab_quan",
        @"qw_tab_chat",
        @"qw_tab_me"
    ];
    
    
//    UIColor *def_color = [UIColor colorFromHexString:@"#000000DD"];
//    UIColor *sel_color = [UIColor colorFromHexString:@"#333333"];
    
    for (NSInteger i = 0; i < titles.count; i++) {
        UIViewController *viewController = [[NSClassFromString([className objectAtIndex:i]) alloc] init];
        QWNavgationController *navController  =[[QWNavgationController alloc]initWithRootViewController:viewController];

        UIImage *image =[UIImage imageNamed:defaultImageNames[i]];
        UIImage *selectimage =[UIImage imageNamed:selectedImageNames[i]];
        viewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:titles[i] image:image selectedImage:selectimage];
//        [viewController.tabBarItem setTitleTextAttributes:[NSDictionary
//                                               dictionaryWithObjectsAndKeys:sel_color,
//                                               NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
//        [viewController.tabBarItem setTitleTextAttributes:[NSDictionary
//                                               dictionaryWithObjectsAndKeys:def_color,
//                                               NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
        [self addChildViewController:navController];
    }
    
    
}

#pragma mark - 背景颜色
- (void)setTabbarBackGround{
    UIImage *tabbarImage = [self imageWithColor:[UIColor whiteColor]];
    self.tabBar.backgroundImage = tabbarImage;
    self.tabBar.shadowImage = tabbarImage;
}

- (UIImage *)imageWithColor:(UIColor *)color

{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [color setFill];
    UIRectFill(rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;

}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    NSLog(@"item.title1===%@",item.title);
}

#pragma mark UITabBarController  Delegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    NSLog(@"item.title2===%@---%@",[tabBarController class],[viewController class]);
    return YES;
}

@end
