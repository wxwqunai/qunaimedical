//
//  QWBaseTableViewCell.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWBaseTableViewCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
