//
//  QWBaseController.h
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWBaseController : UIViewController
@property (nonatomic,strong) UIBarButtonItem * _Nullable leftBarButtonItem;
@property (nonatomic,strong) UIBarButtonItem * _Nullable rightBarButtonItem;

@property (nonatomic,assign) BOOL isHiddenLeftBarButtonItem;
@property (nonatomic,assign) BOOL isHiddenRightBarButtonItem;

@property (nonatomic,copy) NSString * rightBarButtonItem_image;

@property (nonatomic,assign) BOOL showNoDataView;
           
#pragma mark- 自定义导航栏
-(void)customNavigationBarWithBgColor:(UIColor *)bgcolor withTitleColor:(UIColor *)titleColor;

#pragma mark - 返回到某个模块一级界面
- (void)turnToTabVCAtIndex:(NSUInteger)index;
@end

NS_ASSUME_NONNULL_END
