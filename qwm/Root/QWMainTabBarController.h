//
//  QWMainTabBarController.h
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMainTabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
