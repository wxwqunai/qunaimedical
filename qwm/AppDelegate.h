//
//  AppDelegate.h
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

