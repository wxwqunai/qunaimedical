//
//  QWRegPassTipsView.m
//  qwm
//
//  Created by kevin on 2023/3/27.
//

#import "QWRegPassTipsView.h"

@interface QWRegPassTipsView()
@property (nonatomic,strong) UILabel *label;

@end

@implementation QWRegPassTipsView

- (instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self agreementView];
        [self label];
    }
    return self;
}

- (UILabel *)label{
    if(!_label){
        _label = [[UILabel alloc]init];
        _label.font=[UIFont fontWithName:@"PingFangSC-Light" size:14];
        _label.text=@"请设置至少6位数的密码";
        [self addSubview:_label];
        
        [_label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(18);
            make.top.equalTo(self).offset(15);
        }];
    }
    return _label;
}

- (QWAgreementView *)agreementView{
    if(!_agreementView){
        _agreementView = [[QWAgreementView alloc]init];
        [self addSubview:_agreementView];
        
        [_agreementView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(14);
            make.right.equalTo(self).offset(-14);
            make.bottom.equalTo(self);
        }];
    }
    return _agreementView;
}


@end
