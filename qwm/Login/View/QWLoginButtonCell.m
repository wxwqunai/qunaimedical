//
//  QWLoginButtonCell.m
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWLoginButtonCell.h"

@implementation QWLoginButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.loginButton setBackgroundColor:Color_Main_Green];
}
- (IBAction)loginButtonClick:(id)sender {
    if(self.loginClickBlock){
        self.loginClickBlock();
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
