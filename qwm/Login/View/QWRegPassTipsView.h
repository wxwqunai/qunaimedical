//
//  QWRegPassTipsView.h
//  qwm
//
//  Created by kevin on 2023/3/27.
//

#import <UIKit/UIKit.h>
#import "QWAgreementView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWRegPassTipsView : UIView
@property (nonatomic,strong) QWAgreementView *agreementView;
@end

NS_ASSUME_NONNULL_END
