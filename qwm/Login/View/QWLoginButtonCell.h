//
//  QWLoginButtonCell.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^LoginClickBlock)(void);

@interface QWLoginButtonCell : QWBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@property (nonatomic, copy) LoginClickBlock loginClickBlock;

@end

NS_ASSUME_NONNULL_END
