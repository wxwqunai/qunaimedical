//
//  QWAgreementView.m
//  qwm
//
//  Created by kevin on 2023/5/8.
//

#import "QWAgreementView.h"
#import "QWAgreementController.h"
@interface QWAgreementView()
@property (nonatomic, strong) UILabel *aggrentLabel;
@property (nonatomic, strong) UIButton *aggrentButton;
@end

@implementation QWAgreementView

- (instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self aggrentButton];
        [self aggrentLabel];
    }
    return self;
}

#pragma mark- 协议
- (UILabel *)aggrentLabel{
    if(!_aggrentLabel){
        _aggrentLabel = [[UILabel alloc]init];
        _aggrentLabel.numberOfLines = 0;
        _aggrentLabel.font=[UIFont systemFontOfSize:14];
        [self loadLabelText];
        [self addSubview:_aggrentLabel];
        
        [_aggrentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.aggrentButton.mas_right).offset(2);
            make.right.equalTo(self);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _aggrentLabel;
}
- (void)loadLabelText{
    NSString * showText = @"阅读并同意「服务条款」和「隐私政策」";
    NSString * aggrementOneStr = @"「服务条款」";
    NSString * aggrementTwoStr = @"「隐私政策」";
    
    NSMutableAttributedString * attString = [[NSMutableAttributedString alloc] initWithString:showText];
    NSDictionary * attDic = @{
                              NSFontAttributeName : [UIFont systemFontOfSize:14],
                              NSForegroundColorAttributeName : [UIColor blackColor]
                              };
    [attString setAttributes:attDic range:NSMakeRange(0, showText.length)];
    [attString addAttributes:@{NSForegroundColorAttributeName : Color_Main_Green} range:[showText rangeOfString:aggrementOneStr]];
    [attString addAttributes:@{NSForegroundColorAttributeName : Color_Main_Green} range:[showText rangeOfString:aggrementTwoStr]];
    self.aggrentLabel.attributedText = attString;
    
    [self.aggrentLabel yb_addAttributeTapActionWithStrings:@[aggrementOneStr,aggrementTwoStr] tapClicked:^(UILabel *label, NSString *string, NSRange range, NSInteger index) {
        QWAgreementController *agreeVC = [[QWAgreementController alloc]init];
        agreeVC.url = [string isEqualToString:aggrementOneStr]?URL_Login_UserAgreement:URL_Login_PrivacyService;
        agreeVC.title = [string isEqualToString:aggrementOneStr]?@"服务条款":@"隐私政策";
        agreeVC.hidesBottomBarWhenPushed = YES;
        UIViewController *currentVC = [[QWCommonMethod Instance] getCurrentVC];
        [currentVC.navigationController pushViewController:agreeVC animated:YES];
    }];
}

#pragma mark - 同意-按钮
- (UIButton *)aggrentButton{
    if(!_aggrentButton){
        _aggrentButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_aggrentButton setImage:[UIImage imageNamed:@"qw_circle.png"] forState:UIControlStateNormal];
        _aggrentButton.tintColor = Color_Main_Green;
        [_aggrentButton addTarget:self action:@selector(aggrentButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_aggrentButton];

        [_aggrentButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.centerY.equalTo(self);
            make.size.width.mas_equalTo(25.0);
        }];
    }
    return _aggrentButton;
}
-(void)aggrentButtonClick:(UIButton *)sender{

    self.isSelected = !self.isSelected;
    [sender setImage:[UIImage imageNamed:self.isSelected?@"qw_circle_check":@"qw_circle"] forState:UIControlStateNormal];
    if(self.agreementClickBlock)self.agreementClickBlock(_isSelected);
}

@end
