//
//  QWAgreementView.h
//  qwm
//
//  Created by kevin on 2023/5/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^AgreementClickBlock)(BOOL isAgree);

@interface QWAgreementView : UIView

@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, copy) AgreementClickBlock agreementClickBlock;

@end

NS_ASSUME_NONNULL_END
