//
//  QWForRegCell.m
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWForRegCell.h"
#import "QWAgreementController.h"

@interface QWForRegCell()
{
    BOOL _isSelected;
}
@end
@implementation QWForRegCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.forgetButton.tintColor =Color_Main_Green;
    self.registButton.tintColor =Color_Main_Green;
    self.aggrentButton.tintColor = Color_Main_Green;

    
    [self loadLabelText];
}

- (void)loadLabelText{
    NSString * showText = @"阅读并同意「服务条款」和「隐私政策」";
    NSString * aggrementOneStr = @"「服务条款」";
    NSString * aggrementTwoStr = @"「隐私政策」";
    
    NSMutableAttributedString * attString = [[NSMutableAttributedString alloc] initWithString:showText];
    NSDictionary * attDic = @{
                              NSFontAttributeName : [UIFont systemFontOfSize:14],
                              NSForegroundColorAttributeName : [UIColor blackColor]
                              };
    [attString setAttributes:attDic range:NSMakeRange(0, showText.length)];
    [attString addAttributes:@{NSForegroundColorAttributeName : Color_Main_Green} range:[showText rangeOfString:aggrementOneStr]];
    [attString addAttributes:@{NSForegroundColorAttributeName : Color_Main_Green} range:[showText rangeOfString:aggrementTwoStr]];
    self.agreementLabel.attributedText = attString;
    
    [self.agreementLabel yb_addAttributeTapActionWithStrings:@[aggrementOneStr,aggrementTwoStr] tapClicked:^(UILabel *label, NSString *string, NSRange range, NSInteger index) {
        QWAgreementController *agreeVC = [[QWAgreementController alloc]init];
        agreeVC.url = [string isEqualToString:aggrementOneStr]?URL_Login_UserAgreement:URL_Login_PrivacyService;
        agreeVC.title = [string isEqualToString:aggrementOneStr]?@"服务条款":@"隐私政策";
        agreeVC.hidesBottomBarWhenPushed = YES;
        [self.superVC.navigationController pushViewController:agreeVC animated:YES];
    }];
}

- (IBAction)forgetButtonClick:(id)sender {
    NSLog(@"忘记密码");
    if(self.forgetClickBlock)self.forgetClickBlock();
}

- (IBAction)registButtonClick:(id)sender {
    if(self.registClickBlock)self.registClickBlock();
}

- (IBAction)aggrentButtonClick:(UIButton *)sender {
   
    _isSelected = !_isSelected;
    [sender setImage:[UIImage imageNamed:_isSelected?@"qw_circle_check":@"qw_circle"] forState:UIControlStateNormal];
    if(self.agreementClickBlock)self.agreementClickBlock(_isSelected);
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
