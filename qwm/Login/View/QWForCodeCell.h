//
//  QWForCodeCell.h
//  qwm
//
//  Created by kevin on 2023/3/27.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^CodeBtnClickBlock)(void);


@interface QWForCodeCell : QWBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIButton *codeButton;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (nonatomic, copy) CodeBtnClickBlock codeBtnClickBlock;
@end

NS_ASSUME_NONNULL_END
