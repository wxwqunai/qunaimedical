//
//  QWForRegCell.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^ForgetClickBlock)(void);
typedef void (^RegistClickBlock)(void);
typedef void (^AgreementClickBlock)(BOOL isAgree);

@interface QWForRegCell : QWBaseTableViewCell
@property (nonatomic, strong) UIViewController *superVC;
@property (weak, nonatomic) IBOutlet UIButton *forgetButton;
@property (weak, nonatomic) IBOutlet UIButton *registButton;
@property (weak, nonatomic) IBOutlet UILabel *agreementLabel;
@property (weak, nonatomic) IBOutlet UIButton *aggrentButton;


@property (nonatomic, copy) ForgetClickBlock forgetClickBlock;
@property (nonatomic, copy) RegistClickBlock registClickBlock;
@property (nonatomic, copy) AgreementClickBlock agreementClickBlock;


@end

NS_ASSUME_NONNULL_END
