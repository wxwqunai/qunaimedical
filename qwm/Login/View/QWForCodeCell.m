//
//  QWForCodeCell.m
//  qwm
//
//  Created by kevin on 2023/3/27.
//

#import "QWForCodeCell.h"

@implementation QWForCodeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.codeButton setTitle:@"" forState:UIControlStateNormal];
    self.codeLabel.textColor =[UIColor colorFromHexString:@"#007FFF"];
}
- (IBAction)codeButtonClick:(id)sender {
    if(self.codeBtnClickBlock){
        self.codeBtnClickBlock();
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
