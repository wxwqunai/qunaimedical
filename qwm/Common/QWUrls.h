//
//  QWUrls.h
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#ifndef QWUrls_h
#define QWUrls_h



#ifdef DEBUG//测试环境
//#define QWM_BASE_API_URL @"http://gaiwo.belovedlive.com:8098/"
#define QWM_BASE_API_URL @"http://124.115.217.70:8098/"

#else//正式环境
//#define QWM_BASE_API_URL @"http://gaiwo.belovedlive.com:8098/"
#define QWM_BASE_API_URL @"http://124.115.217.70:8098/"

#endif

//环信key
#define EaseIMAppKey_PROD  @"1145210608051818#medical-doctor"


#define URL_Error @"网络异常，请重试"

#define URL_login @"boke/applogin_mlogin.do" //登录
#define URL_register @"bokehall/user/submitRegister" //注册
#define URL_SEND_OTP @"bokehall/user/sendmessage" //获取验证码
#define URL_RESET_PWD @"bokehall/user/retrievePassword" //重置密码
#define URL_FEEDBACK @"boke/user_report.do" //后续反馈
#define URL_Health_List @"bokehall/health/healthlist"
#define URL_Health_Detail @"bokehall/health/detail" //资讯-详情-无用
#define URL_Resfile_Openqa @"boke/resfile/qaEnter/openqa.json"

#define URL_DOCTOR_LIST @"bokehall/all/doctorList"
#define URL_FindAllTopic @"boke/msg_findAllTopic.do" //所有评论
#define URL_AddmsgBBSanswer @"boke/msg_addmsgBBSanswer.do" //评论-新增发布、回复
#define URL_SaveNewTopic  @"boke/msg_saveNewTopic.do" //聚合新闻-新增发布

#define URL_Img_res @"boke/resfile/img_res/" //咨询-图片

/*
 *聚合数据-https://www.juhe.cn/
 */
#define URL_JUHE_Healthtip @"http://apis.juhe.cn/fapigx/healthtip/query?key=e37b0428981b4c55edab6ae3a769b413" //健康小提示
#define URL_JUHE_News @"http://v.juhe.cn/toutiao/index?type=top&key=c0cd317ebb666bdf9f9d76da451ff01b" //新闻头条


//隐私协议
#define URL_Login_PrivacyService QWM_BASE_API_URL@"boke/resfile/privacyServiceTermsBBS_0509.html"
//服务协议
#define URL_Login_UserAgreement QWM_BASE_API_URL@"boke/resfile/userAgreementBBS_0509.html"

#endif /* QWUrls_h */
