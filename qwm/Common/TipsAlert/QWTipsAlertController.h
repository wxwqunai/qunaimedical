//
//  QWTipsAlertController.h
//  qwm
//
//  Created by kevin on 2023/3/28.
//

#import <UIKit/UIKit.h>
#import "QWCheckCodeView.h"
#import "QWSystemNotice.h"
#import "QWPicNotice.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, TipsAlertType)
{
    
    TipsAlertType_defalut = 0,
    TipsAlertType_loginNot,   //未登录
    TipsAlertType_checkCode,  //检查验证码
    TipsAlertType_home_systemNotice,  //首页系统公告
    TipsAlertType_home_picNotice,  //首页系统公告-图片
};

@interface QWTipsAlertController : UIViewController

@property (nonatomic, assign) TipsAlertType tipType;

@property (nonatomic, strong) QWCheckCodeView *checkCodeView;
@property (nonatomic, strong) QWSystemNotice *systemNotice;
@property (nonatomic, strong) QWPicNotice *picNotice;

@end

NS_ASSUME_NONNULL_END
