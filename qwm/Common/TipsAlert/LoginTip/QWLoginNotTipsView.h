//
//  QWLoginNotTipsView.h
//  qwm
//
//  Created by kevin on 2023/3/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^ForkBlock)(void);

@interface QWLoginNotTipsView : UIView

@property (nonatomic,copy) ForkBlock forkBlock;

@end

NS_ASSUME_NONNULL_END
