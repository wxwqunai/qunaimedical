//
//  QWCheckCodeView.h
//  qwm
//
//  Created by kevin on 2023/4/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^ForkBlock)(void);
typedef void (^SureBlock)(NSString *code);

@interface QWCheckCodeView : UIView

@property (nonatomic,copy) ForkBlock forkBlock;
@property (nonatomic,copy) SureBlock sureBlock;


#pragma mark- 检验口令
- (void)checkInput:(BOOL)isCorrect;

@end

NS_ASSUME_NONNULL_END

