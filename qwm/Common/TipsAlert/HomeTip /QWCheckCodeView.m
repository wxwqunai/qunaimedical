//
//  QWCheckCodeView.m
//  qwm
//
//  Created by kevin on 2023/4/7.
//

#import "QWCheckCodeView.h"

@interface QWCheckCodeView()
@property (nonatomic, strong) UILabel *tishiLabel;
@property (nonatomic, strong) UIButton *forkButtun;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UILabel *errLabel;
@property (nonatomic, strong) UIButton *sureButtun;
@end

@implementation QWCheckCodeView

- (instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
        
        [self tishiLabel];
        [self forkButtun];
        
        [self textField];
        [self errLabel];
        [self sureButtun];
    }
    return self;
}

- (UILabel *)tishiLabel{
    if(!_tishiLabel){
        _tishiLabel = [[UILabel alloc]init];
        _tishiLabel.text =@"口令";
        _tishiLabel.font =[UIFont systemFontOfSize:20];
        
        [self addSubview:_tishiLabel];
        
        [_tishiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(20);
            make.left.mas_equalTo(20);
        }];
    }
    return _tishiLabel;
}



-(UIButton *)forkButtun
{
    if (!_forkButtun) {
        _forkButtun =[UIButton buttonWithType:UIButtonTypeCustom];
        [_forkButtun setImage:[UIImage imageNamed:@"WCFork.png"] forState:UIControlStateNormal];
        [_forkButtun addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_forkButtun];
        
        [_forkButtun mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_tishiLabel);
            make.right.equalTo(self);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(50);
        }];
    }
    return _forkButtun;
}
-(void)buttonClick:(UIButton *)sender
{
    if(self.forkBlock){
        self.forkBlock();
    }
}


- (UITextField *)textField{
    if(!_textField){
        _textField = [[UITextField alloc]init];
        _textField.placeholder =@"请输入口令";
//        _textField.textColor =[UIColor colorFromHexString:@"#A2A2A2"];
        _textField.borderStyle = UITextBorderStyleNone;
        _textField.font =[UIFont systemFontOfSize:16];
        
        //边框
        _textField.layer.borderColor = [UIColor colorFromHexString:@"#A2A2A2"].CGColor;
        _textField.layer.borderWidth = 1;
        
        //左边距
        _textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, 0)];
        _textField.leftViewMode = UITextFieldViewModeAlways;
        
        //清除样式
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self addSubview:_textField];
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_tishiLabel.mas_bottom).offset(20);
            make.left.mas_equalTo(20);
            make.right.mas_equalTo(-20);
            make.height.mas_equalTo(50);
        }];
    }
    return _textField;
}

#pragma mark - 错误提示
- (UILabel *)errLabel{
    if(!_errLabel){
        _errLabel = [[UILabel alloc]init];
        _errLabel.text =@"口令错误";
        _errLabel.textColor = [UIColor redColor];
        _errLabel.font =[UIFont systemFontOfSize:12];
        _errLabel.hidden = YES;
        
        [self addSubview:_errLabel];
        
        [_errLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_textField.mas_left);
            make.top.equalTo(_textField.mas_bottom).offset(2);
        }];
    }
    return _errLabel;
}

- (UIButton *)sureButtun
{
    if (!_sureButtun) {
        _sureButtun =[UIButton buttonWithType:UIButtonTypeCustom];
        [_sureButtun setTitle:@"确定" forState:UIControlStateNormal];
        [_sureButtun setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _sureButtun.titleLabel.font = [UIFont systemFontOfSize:20];
        _sureButtun.backgroundColor = Color_Main_Green;
        [_sureButtun addTarget:self action:@selector(sureButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _sureButtun.layer.cornerRadius = 6;
        _sureButtun.layer.masksToBounds = YES;
        
        [self addSubview:_sureButtun];
        
        [_sureButtun mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(20);
            make.right.equalTo(self).offset(-20);
            make.top.equalTo(_textField.mas_bottom).offset(40);
            make.height.mas_equalTo(55);
            make.bottom.mas_equalTo(-20);
        }];
    }
    return _sureButtun;
}
-(void)sureButtonClick:(UIButton *)sender
{
    if(self.sureBlock) {
        NSString *code = _textField.text;
        self.sureBlock(code);
    }
}


#pragma mark- 检验口令
- (void)checkInput:(BOOL)isCorrect{
    if(isCorrect){
        if(self.forkBlock) self.forkBlock();
        _textField.layer.borderColor = [UIColor colorFromHexString:@"#A2A2A2"].CGColor;
        _errLabel.hidden = YES;
    }else{
        //边框
        _textField.layer.borderColor = [UIColor redColor].CGColor;
        _errLabel.hidden = NO;
    }
}

@end
