//
//  QWPicNotice.h
//  qwm
//
//  Created by kevin on 2023/6/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^CloseBlock)(void);
typedef void (^NotiSureBlock)(void);

@interface QWPicNotice : UIView
@property (nonatomic,copy) CloseBlock closeBlock;
@property (nonatomic,copy) NotiSureBlock sureBlock;

@property (nonatomic, copy) NSString *picUrl;
@end

NS_ASSUME_NONNULL_END
