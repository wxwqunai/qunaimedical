//
//  QWSystemNotice.h
//  qwm
//
//  Created by kevin on 2023/5/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^CloseBlock)(void);
typedef void (^NotiSureBlock)(void);

@interface QWSystemNotice : UIView
@property (nonatomic,copy) CloseBlock closeBlock;
@property (nonatomic,copy) NotiSureBlock sureBlock;

@end

NS_ASSUME_NONNULL_END
