//
//  QWSystemNotice.m
//  qwm
//
//  Created by kevin on 2023/5/19.
//

#import "QWSystemNotice.h"
@interface QWSystemNotice()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UIButton *sureButtun;

@end

@implementation QWSystemNotice
- (instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = Color_Main_Green;
        self.layer.cornerRadius = 8.0;
        self.layer.masksToBounds = YES;
        
        [self titleLabel];
        [self picImageView];
        [self sureButtun];
    }
    return self;
}

- (UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text =@"系统公告";
        _titleLabel.font =[UIFont systemFontOfSize:20];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(20);
            make.centerX.equalTo(self);
        }];
    }
    return _titleLabel;
}

#pragma mark - 公告内容图片
- (UIImageView *)picImageView{
    if(!_picImageView){
        _picImageView = [[UIImageView alloc]init];
        _picImageView.backgroundColor = [UIColor colorFromHexString:@"#FFFFF0"];
        [self addSubview:_picImageView];
        
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_titleLabel.mas_bottom).offset(10.0);
            make.left.equalTo(self).offset(10.0);
            make.right.equalTo(self).offset(-10.0);
            make.height.mas_greaterThanOrEqualTo(self.mas_width);
        }];
    }
    return _picImageView;
}

#pragma mark - 确定
-(UIButton *)sureButtun
{
    if (!_sureButtun) {
        _sureButtun =[UIButton buttonWithType:UIButtonTypeCustom];
        [_sureButtun setTitle:@"确定" forState:UIControlStateNormal];
        [_sureButtun setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _sureButtun.backgroundColor = [UIColor colorFromHexString:@"#4169E1"];
        _sureButtun.layer.cornerRadius = 6.0;
        _sureButtun.layer.masksToBounds = YES;
        // 阴影的颜色
        _sureButtun.layer.shadowColor = [UIColor colorFromHexString:@"#4169E1"].CGColor;
        _sureButtun.layer.shadowOpacity = 0.8;
        // 阴影的圆角
        _sureButtun.layer.shadowRadius = 6.0;
        // 阴影偏离的位置 (100, 50) x 方向偏离 100，y 偏离 50 正向，如果是负数正好为相反的方向
        _sureButtun.layer.shadowOffset = CGSizeMake(0, 0);
        [_sureButtun addTarget:self action:@selector(sureButtunClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_sureButtun];
        
        [_sureButtun mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(_picImageView.mas_bottom).offset(15);
            make.bottom.equalTo(self).offset(-15.0);
            
            make.width.mas_equalTo(80);
        }];
    }
    return _sureButtun;
}
-(void)sureButtunClick:(UIButton *)sender
{
    if(self.closeBlock){
        self.closeBlock();
    }
    if(self.sureBlock){
        self.sureBlock();
    }
}

@end
