//
//  QWPicNotice.m
//  qwm
//
//  Created by kevin on 2023/6/2.
//

#import "QWPicNotice.h"

@interface QWPicNotice()
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UIButton *sureButtun;

@end

@implementation QWPicNotice

- (instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self picImageView];
        [self sureButtun];
    }
    return self;
}

#pragma mark - 公告内容图片
- (UIImageView *)picImageView{
    if(!_picImageView){
        _picImageView = [[UIImageView alloc]init];
        _picImageView.backgroundColor = [UIColor clearColor];
//        _picImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_picImageView];
        
        _picImageView.hidden = YES;
        
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
            make.height.mas_lessThanOrEqualTo(kScreenHeight*0.7);
        }];
    }
    return _picImageView;
}

#pragma mark - 确定
-(UIButton *)sureButtun
{
    if (!_sureButtun) {
        _sureButtun =[UIButton buttonWithType:UIButtonTypeCustom];
        [_sureButtun setTitle:@"确定" forState:UIControlStateNormal];
        [_sureButtun setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _sureButtun.backgroundColor = [UIColor colorFromHexString:@"#32C5FF"];
        _sureButtun.layer.cornerRadius = 6.0;
        _sureButtun.layer.masksToBounds = YES;
        // 阴影的颜色
        _sureButtun.layer.shadowColor = [UIColor colorFromHexString:@"#20E0DF"].CGColor;
        _sureButtun.layer.shadowOpacity = 0.8;
        // 阴影的圆角
        _sureButtun.layer.shadowRadius = 6.0;
        // 阴影偏离的位置 (100, 50) x 方向偏离 100，y 偏离 50 正向，如果是负数正好为相反的方向
        _sureButtun.layer.shadowOffset = CGSizeMake(0, 0);
        [_sureButtun addTarget:self action:@selector(sureButtunClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_sureButtun];
        
        _sureButtun.hidden = YES;
        
        [_sureButtun mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.bottom.equalTo(self).offset(-15.0);
            make.width.mas_equalTo(80);
        }];
    }
    return _sureButtun;
}
-(void)sureButtunClick:(UIButton *)sender
{
    if(self.closeBlock){
        self.closeBlock();
    }
    if(self.sureBlock){
        self.sureBlock();
    }
}

- (void)setPicUrl:(NSString *)picUrl{
    _picUrl = picUrl;

    if([_picUrl containsString:@"http"]){
        [_picImageView sd_setImageWithURL:[NSURL URLWithString:_picUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            self->_sureButtun.hidden = NO;
            self->_picImageView.hidden = NO;
        }];
    }else{
        _sureButtun.hidden = NO;
        _picImageView.hidden = NO;
        _picImageView.image = [UIImage imageNamed:_picUrl];
    }
}

@end
