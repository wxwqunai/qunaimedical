//
//  QWTipsAlertController.m
//  qwm
//
//  Created by kevin on 2023/3/28.
//

#import "QWTipsAlertController.h"
#import "QWLoginNotTipsView.h"

@interface QWTipsAlertController ()

@property (nonatomic, strong) QWLoginNotTipsView *loginNotView;

@end

@implementation QWTipsAlertController
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    [self beignAnimation];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.tipType == TipsAlertType_loginNot){
        [self loginNotView];
    }
    
    if(self.tipType == TipsAlertType_checkCode){
        [self checkCodeView];
    }
    
    //系统公告
    if(self.tipType == TipsAlertType_home_systemNotice){
        [self systemNotice];
    }
    
    //系统公告-图片
    if(self.tipType == TipsAlertType_home_systemNotice){
        [self picNotice];
    }
}

#pragma mark - 未登录
- (QWLoginNotTipsView *)loginNotView{
    if(!_loginNotView){
        _loginNotView = [[QWLoginNotTipsView alloc]init];
        __weak __typeof(self) weakSelf = self;
        _loginNotView.forkBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf dismissViewControllerAnimated:NO completion:nil];
        };
        [self.view addSubview:_loginNotView];
        
        [_loginNotView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.centerY.equalTo(self.view);
            make.width.equalTo(self.view).offset(-50);
        }];
    }
    return _loginNotView;
}

#pragma mark - 邀请码
- (QWCheckCodeView *)checkCodeView{
    if(!_checkCodeView){
        _checkCodeView = [[QWCheckCodeView alloc]init];
        __weak __typeof(self) weakSelf = self;
        _checkCodeView.forkBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf dismissViewControllerAnimated:NO completion:nil];
        };
        [self.view addSubview:_checkCodeView];
        
        [_checkCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.centerY.equalTo(self.view);
            make.width.equalTo(self.view).offset(-50);
        }];
    }
    return _checkCodeView;
}

#pragma mark - 系统公告
- (QWSystemNotice *)systemNotice{
    if(!_systemNotice){
        _systemNotice = [[QWSystemNotice alloc]init];
        __weak __typeof(self) weakSelf = self;
        _systemNotice.closeBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf dismissViewControllerAnimated:NO completion:nil];
        };
        [self.view addSubview:_systemNotice];
        
        [_systemNotice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.centerY.equalTo(self.view);
            make.width.mas_lessThanOrEqualTo(self.view).offset(-50);
        }];
    }
    return _systemNotice;
}

#pragma mark - 系统公告 - 图片
- (QWPicNotice *)picNotice{
    if(!_picNotice){
        _picNotice = [[QWPicNotice alloc]init];
        __weak __typeof(self) weakSelf = self;
        _picNotice.closeBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf dismissViewControllerAnimated:NO completion:nil];
        };
        [self.view addSubview:_picNotice];
        
        [_picNotice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.centerY.equalTo(self.view);
            make.width.mas_lessThanOrEqualTo(self.view).offset(-50);
        }];
    }
    return _picNotice;
}

//#pragma mark - 动画
//- (void)beignAnimation{
//    [self.view setNeedsUpdateConstraints];
//    //系统公告
//    if(self.tipType == TipsAlertType_home_systemNotice){
//        [self.systemNotice mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.width.equalTo(self.view).offset(-50);
//            make.height.mas_equalTo(300);
//        }];
//    }
//    
//    __weak __typeof(self) weakSelf = self;
//    [UIView animateWithDuration:0.1 delay:0.0 options:(UIViewAnimationOptionCurveLinear) animations:^{
//        __strong __typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf.view layoutIfNeeded];
//        } completion:^(BOOL finished) {
//            
//        }];
//}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

@end
