//
//  QWAppProfile.m
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWAppProfile.h"
#import "QWTipsAlertController.h"

@implementation QWAppProfile
static QWAppProfile *_sharedInstance = nil;
static dispatch_once_t once_token = 0;
+(instancetype)sharedInstance {
    dispatch_once(&once_token, ^{
        if (_sharedInstance == nil) {
            _sharedInstance = [[super allocWithZone:NULL] init] ;
        }
    });
    return _sharedInstance;
}

#pragma mark -登录定时器
-(SWLoginTimeModel *)timeModel
{
    if (!_timeModel) {
        _timeModel =[[SWLoginTimeModel alloc]init];
    }
    return _timeModel;
}

#pragma mark - 是否已登录
- (BOOL)isLogined{
    if (!_qaUserInfo && IsStringEmpty(_qaUserInfo.phone) && IsStringEmpty(_qaUserInfo.password)) {
        return false;
    }
    return true;
}
- (BOOL)isLoginedAndShowLogintAlert{
    if (!_qaUserInfo && IsStringEmpty(_qaUserInfo.phone) && IsStringEmpty(_qaUserInfo.password)) {
        QWTipsAlertController *activityVC =[[QWTipsAlertController alloc]init];
        activityVC.tipType = TipsAlertType_loginNot;
        activityVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        activityVC.modalPresentationStyle=UIModalPresentationOverFullScreen;
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:activityVC animated:NO completion:nil];
        return false;
    }
    return true;
}

#pragma mark - 保存／获取/修改/清除 用户信息 缓存
//-(QWqaUserModel *)qaUserInfo
//{
//    if (!_qaUserInfo) {
//        _qaUserInfo =[[QWqaUserModel alloc]init];
//    }
//    return _qaUserInfo;
//}
-(void)saveUserDefault:(QWqaUserModel *)userInfo
{
    [LKDBHelper clearTableData:[QWqaUserModel class]];
    [userInfo saveToDB];
}
-(QWqaUserModel *)GetUserInfo
{
    return [QWqaUserModel searchSingleWithWhere:nil orderBy:nil];
}
-(void)chageUserInfo:(QWqaUserModel *)userInfo
{
    [QWqaUserModel updateToDB:userInfo where:nil];
}
-(void)clearUserInfo
{
    if(_qaUserInfo){
        [LKDBHelper clearTableData:[QWqaUserModel class]];
        [LKDBHelper deleteToDB:_qaUserInfo];
        _qaUserInfo=nil;
    }
}



@end
