//
//  QWCommonMethod.m
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWCommonMethod.h"
#import "QWMainTabBarController.h"
#import "QWCLWrapper.h"
#import "JieqiDataModel.h"

@implementation QWCommonMethod
static QWCommonMethod *_sharedInstance = nil;
static dispatch_once_t once_token = 0;
+(instancetype)Instance {
    dispatch_once(&once_token, ^{
        if (_sharedInstance == nil) {
            _sharedInstance = [[super allocWithZone:NULL] init] ;
        }
    });
    return _sharedInstance;
}


#pragma mark -showAlert提示
-(void)ShowOldAlert:(NSString*)title Msg:(NSString*)msg withSure:(void (^)(void))completion
{    
    UIAlertController *genderAlert =[UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 =[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(completion)completion();
    }];
    UIAlertAction *action2 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [genderAlert addAction:action1];
    [genderAlert addAction:action2];
    [[self getCurrentVC] presentViewController:genderAlert animated:YES completion:nil];
}

#pragma mark - 当前版本号
- (NSString *)currentVersion{
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    return version;
}

#pragma mark - 获取当前屏幕显示的viewcontroller
- (UIViewController *)getCurrentVC {
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows) {
            if (tmpWin.windowLevel == UIWindowLevelNormal) {
                window = tmpWin;
                break;
            }
        }
    }
    
    if ([window subviews].count ==0 )return nil;
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]]) {
        result = nextResponder;
    } else {
        result = window.rootViewController;
    }
    
    /*
     *  在此判断返回的视图是不是你的根视图--我的根视图是tabbar
     */
    if ([result isKindOfClass:[QWMainTabBarController class]]) {
        QWMainTabBarController *mainTabBarVC = (QWMainTabBarController *)result;
        result = [mainTabBarVC selectedViewController];
        result = [result.childViewControllers lastObject];
    }
    
    return result;
}

#pragma mark- 时间转换格式 YYYY-MM-dd HH:mm:ss
-(NSString *)changeDateToString:(NSDate *)date withFormatter:(NSString *)formatterStr
{
    NSString *timeStr =@"";
    if (date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = formatterStr;
        timeStr = [dateFormatter stringFromDate:date];
    }
    return timeStr;
}

#pragma mark- 计算当前时间在哪个节气间
-(NSString *)calculateThisYearSolarTerms
{
    NSDate *nowDate=[NSDate date];
    NSString *year =[self changeDateToString:nowDate withFormatter:@"YYYY"];
    NSMutableArray *arr = [CLWrapper calculateAllJieqiByYear:[year intValue] fromJieqiId:0]; //21 是立春，详见 calendartool.h 中的定义
    
    JieqiDataModel* currentjdModel;
    for (int i=0; i<arr.count; i++) {
        JieqiDataModel* jdModel=arr[i];
        NSDate *laterDate = [jdModel.jieqiDate laterDate:nowDate];
        //当前时间处于两个节气之间
        if ([laterDate isEqualToDate:jdModel.jieqiDate]) {
            if (i==0) {
                currentjdModel=arr[arr.count-1];
                currentjdModel.numOfAllJieqi=arr.count;
            }else
            {
                currentjdModel=arr[i-1];
                currentjdModel.numOfAllJieqi=i;
            }
            return currentjdModel.jieqiName;
        }
    }
    return nil;
}

#pragma mark- 计算当前时间在哪个时辰-修改时辰状态
-(NSString *)calculateOneOfTheTwelveHourPeriods
{
    NSInteger hourNum =[[self changeDateToString:[NSDate date] withFormatter:@"H"] integerValue];
    
    NSInteger currentHourNum;
    if (hourNum%2==0) {
        currentHourNum=hourNum/2;
    }else
    {
        currentHourNum=hourNum/2+1;
    }
    if (currentHourNum>=12) {
        currentHourNum=0;
    }
    
    NSArray *arr =[[NSArray alloc]initWithObjects:
                            @"子时",@"丑时",@"寅时",@"卯时",
                            @"辰时",@"巳时",@"午时",@"未时",
                            @"申时",@"酉时",@"戌时",@"亥时",nil];
    NSString *showStr;
    if (currentHourNum<arr.count) {
        showStr=arr[currentHourNum];
    }
    return showStr;
}

#pragma mark - 系统分享
- (void)sharedFromSystem:(NSString *)title
                 withUrl:(NSString*)url
                 success:(void (^)(void))success
                 failure:(void (^)(void))failure
{
    //分享的标题
         NSString *textToShare = title;
        //分享的图片
         UIImage *imageToShare = [UIImage imageNamed:@"logo.png"];
         //分享的url
         NSURL *urlToShare = [NSURL URLWithString:url];
         //在这里呢 如果想分享图片 就把图片添加进去  文字什么的通上
         NSArray *activityItems = @[textToShare,imageToShare, urlToShare];
         UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];
          //不出现在活动项目
        activityVC.excludedActivityTypes = @[
             UIActivityTypePostToFacebook,
             UIActivityTypePostToTwitter,
             UIActivityTypePostToWeibo,
             UIActivityTypeMessage,
             UIActivityTypeMail,
             UIActivityTypePrint,
             UIActivityTypeCopyToPasteboard,
             UIActivityTypeAssignToContact,
             UIActivityTypeSaveToCameraRoll,
             UIActivityTypeAddToReadingList,
             UIActivityTypePostToFlickr,
             UIActivityTypePostToVimeo,
             UIActivityTypePostToTencentWeibo,
             UIActivityTypeAirDrop,
             UIActivityTypeOpenInIBooks,
             UIActivityTypeMarkupAsPDF,
//             UIActivityTypeSharePlay,
//             UIActivityTypeCollaborationInviteWithLink,
//             UIActivityTypeCollaborationCopyLink,
        ];
        [[self getCurrentVC] presentViewController:activityVC animated:YES completion:nil];
         // 分享之后的回调
            activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
                if (completed) {
                    //分享 成功
                    if(success)success();
                } else  {
                    //分享 取消
                    if(failure)failure();
                }
            };
}

#pragma mark -获得Label字符串size
-(CGRect)getLabelRect:(UILabel *)label withText:(NSString *)textStr
{
    CGRect boundsRect =[[UIScreen mainScreen] bounds];
    CGSize sizeFont =CGSizeMake(boundsRect.size.width-label.frame.origin.x, MAXFLOAT);
    CGRect rect = [textStr boundingRectWithSize:sizeFont
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSFontAttributeName:label.font}
                                        context:nil];
    return rect;
}
- (CGSize)boundingRectWithSize:(CGSize)size withLabelStr:(NSString *)labelStr WithFontSize:(NSInteger)fontSize
{
    NSDictionary *attribute = @{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]};
    CGSize retSize = [labelStr boundingRectWithSize:size
                                            options:
                      NSStringDrawingUsesLineFragmentOrigin|
                      NSStringDrawingUsesFontLeading
                                         attributes:attribute
                                            context:nil].size;
    return retSize;
}

@end
