#import <Foundation/Foundation.h>

@protocol SpeechSynthesizerManagerDelegate <NSObject>

- (void)speechSynthesizerDidFinishedSpeak;

@end

@interface SpeechSynthesizerManager : NSObject

@property(nonatomic, weak) id<SpeechSynthesizerManagerDelegate> delegate;

@property(nonatomic, assign) BOOL isSpeaking;

+ (instancetype)shared;

//播报
- (void)speak:(NSString *)string;
//停止播放
- (void)stopSpeaking;

@end

