#import "SpeechSynthesizerManager.h"
#import <AVFoundation/AVFoundation.h>

@interface SpeechSynthesizerManager() <AVSpeechSynthesizerDelegate>

@property (nonatomic, strong) AVSpeechSynthesizer *avSpeechSynthesizer;

@end

@implementation SpeechSynthesizerManager

+ (id)shared {
    static  dispatch_once_t once;
    static   id instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    return instance;
}

- (AVSpeechSynthesizer *)avSpeechSynthesizer {
    if (!_avSpeechSynthesizer) {
        _avSpeechSynthesizer = [[AVSpeechSynthesizer alloc] init];
        _avSpeechSynthesizer.delegate = self;
        //语音冲突处理
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionDuckOthers error:NULL];
    }
    return _avSpeechSynthesizer;
}
- (void)speak:(NSString *)string {
    AVSpeechUtterance *aUtterance = [AVSpeechUtterance speechUtteranceWithString:string];
    [aUtterance setVoice:[AVSpeechSynthesisVoice voiceWithLanguage:@"zh-CN"]];
    
    //iOS语音合成在iOS8及以下版本系统上语速异常
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        aUtterance.rate = 0.25;//iOS7设置为0.25
    } else if ([[[UIDevice currentDevice] systemVersion] floatValue] < 9.0) {
        aUtterance.rate = 0.15;//iOS8设置为0.15
    }
    
    [self.avSpeechSynthesizer speakUtterance:aUtterance];
    self.isSpeaking = YES;
}

//停止播放
- (void)stopSpeaking {
    self.isSpeaking = NO;
    [self.avSpeechSynthesizer stopSpeakingAtBoundary:AVSpeechBoundaryWord];
    [[AVAudioSession sharedInstance] setActive:NO withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
}

#pragma mark - AVSpeechSynthesizerDelegate
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance API_AVAILABLE(ios(7.0), watchos(1.0), tvos(7.0), macos(10.14)) {
    self.isSpeaking = NO;
    [[AVAudioSession sharedInstance] setActive:NO withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
    if ([self.delegate respondsToSelector:@selector(speechSynthesizerDidFinishedSpeak)]) {
        [self.delegate speechSynthesizerDidFinishedSpeak];
    }
}

@end

