//
//  QWCommonMethod.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWCommonMethod : NSObject
+(instancetype)Instance;



#pragma mark -showAlert提示
-(void)ShowOldAlert:(NSString*)title Msg:(NSString*)msg withSure:(void (^)(void))completion;

#pragma mark - 当前版本号
- (NSString *)currentVersion;

#pragma mark - 获取当前屏幕显示的viewcontroller
- (UIViewController *)getCurrentVC;

#pragma mark- 时间转换格式
-(NSString *)changeDateToString:(NSDate *)date withFormatter:(NSString *)formatterStr;

#pragma mark- 计算当前时间在哪个节气间
-(NSString *)calculateThisYearSolarTerms;

#pragma mark- 计算当前时间在哪个时辰-修改时辰状态
-(NSString *)calculateOneOfTheTwelveHourPeriods;


#pragma mark - 系统分享
- (void)sharedFromSystem:(NSString *)title
                 withUrl:(NSString*)url
                 success:(void (^)(void))success
                 failure:(void (^)(void))failure;

#pragma mark - 获得Label字符串size
-(CGRect)getLabelRect:(UILabel *)label withText:(NSString *)textStr;
#pragma mark - 文本尺寸
- (CGSize)boundingRectWithSize:(CGSize)size withLabelStr:(NSString *)labelStr WithFontSize:(NSInteger)fontSize;

@end

NS_ASSUME_NONNULL_END
