//
//  HXGetRequest.h
//  qwm
//
//  Created by kevin on 2023/3/31.
//

#import "HXRequest.h"

NS_ASSUME_NONNULL_BEGIN
 
@interface HXGetRequest : HXRequest
 
/**
 GET 请求
 
 @param url 网址
 @param argument 参数
 @return HXRequest
 */
- (id)initWithRequestUrl:(NSString *)url argument:(NSDictionary *)argument;
 
@end

NS_ASSUME_NONNULL_END
