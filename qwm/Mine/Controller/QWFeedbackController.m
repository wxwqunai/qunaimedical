//
//  QWFeedbackController.m
//  qwm
//
//  Created by kevin on 2023/3/27.
//

#import "QWFeedbackController.h"
#import "QWButtonView.h"

static NSString * const textViewPlaceholder = @"请描述您所遇到的困难";

@interface QWFeedbackController ()<UITextViewDelegate,UIScrollViewDelegate>
@property (nonatomic,strong) UIScrollView *bgScroolView;
@property (nonatomic,strong) UIView *containerView;

@property (nonatomic,strong) UIView *titleView;
@property (nonatomic,strong) UIImageView *lineImageView;
@property (nonatomic,strong)UILabel *titleLabel;

@property (nonatomic,strong) UIView *cnterView;
@property (nonatomic,strong) UITextView *importInfoView;
@property (nonatomic,strong) UILabel *numLabel;

@property (nonatomic,strong) QWButtonView *buttonView;

@end

@implementation QWFeedbackController
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"后续反馈";
    
    [self bgScroolView];
}

-(UIScrollView *)bgScroolView
{
    if (!_bgScroolView) {
        _bgScroolView =[[UIScrollView alloc]init];
        _bgScroolView.alwaysBounceVertical=YES;
        _bgScroolView.backgroundColor =[UIColor whiteColor];
        _bgScroolView.delegate = self;
//        _bgScroolView.contentSize=CGSizeMake(0, kScreenHeight-[UIDevice statusBarHeight]);
        [self.view addSubview:_bgScroolView];
        
        [_bgScroolView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.view);
        }];
        
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor clearColor];
        [_bgScroolView addSubview:_containerView];
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_bgScroolView);
            make.width.equalTo(_bgScroolView); // 需要设置宽度和scrollview宽度一样
        }];

        [self titleView];
        [self cnterView];
        [self buttonView];
        
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
             make.bottom.equalTo(_buttonView.mas_bottom).offset(20);// 这里放最后一个view的底部
         }];
    }
    return _bgScroolView;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma mark - 标题
- (UIView *)titleView{
    if(!_titleView){
        _titleView = [[UIView alloc]init];
        _titleView.backgroundColor =[UIColor clearColor];
        [_containerView addSubview:_titleView];
        
        [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(18);
            make.right.mas_equalTo(18);
            make.top.mas_equalTo(0);
            make.height.mas_equalTo(70.0);
        }];
        
        [self lineImageView];
        [self titleLabel];
        
    }
    return _titleView;
}

- (UIImageView *)lineImageView{
    if(!_lineImageView){
        _lineImageView = [[UIImageView alloc]init];
        _lineImageView.backgroundColor = Color_Main_Green;
        [_titleView addSubview:_lineImageView];
        
        [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_titleView);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(4);
            make.height.mas_equalTo(22);
        }];
    }
    return _lineImageView;
}

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel =[[UILabel alloc]init];
        _titleLabel.text = @"后续反馈";
        _titleLabel.font =[UIFont fontWithName:@"PingFangSC-Medium" size:20];
        _titleLabel.textColor =[UIColor blackColor];
        [_titleView addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_titleView);
            make.left.equalTo(_lineImageView).offset(15);
        }];
    }
    return _titleLabel;
}

#pragma mark - 输入
- (UIView *)cnterView{
    if(!_cnterView){
        _cnterView = [[UIView alloc]init];
        _cnterView.backgroundColor =[UIColor clearColor];
        [_containerView addSubview:_cnterView];
        
        [_cnterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(18);
            make.right.mas_equalTo(-18);
            make.top.mas_equalTo(_titleView.mas_bottom);
            make.height.mas_equalTo(220.0);
        }];
        
        [self importInfoView];
        [self numLabel];
    }
    return _cnterView;
}

//输入框
-(UITextView *)importInfoView
{
    if (!_importInfoView) {
        _importInfoView =[[UITextView alloc]init];
        _importInfoView.text= textViewPlaceholder;
        _importInfoView.returnKeyType=UIReturnKeyDone;
        _importInfoView.delegate=self;
        _importInfoView.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _importInfoView.textColor=[UIColor colorFromHexString:@"#707070"];
        _importInfoView.backgroundColor=[UIColor clearColor];
//        _importInfoView.scrollEnabled = NO;
        
//        _importInfoView.layer.borderWidth=1;
//        _importInfoView.layer.borderColor=[UIColor colorFromHexString:@"#B3B3B3"].CGColor;
        
        [_cnterView addSubview:_importInfoView];
        
        [_importInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_cnterView);
        }];
    }
    return _importInfoView;
}
#pragma mark- UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([textView.text isEqualToString:@"请描述您所遇到的困难"]) {
        textView.text=text;
    }
    
    if( [ @"\n" isEqualToString: text]){
        //你的响应方法
        [self.view endEditing:YES];
        return NO;
    }
    
    if (range.length + range.location > textView.text.length) {
        return NO;
    }
    
    NSUInteger length = textView.text.length + text.length - range.length;
    
    if (length<=400) {
        _numLabel.text=[NSString stringWithFormat:@"%ld/400",length];
        return YES;
    }
    
    return NO;
}

//字数显示
-(UILabel *)numLabel
{
    if (!_numLabel) {
        _numLabel=[[UILabel alloc]init];
        _numLabel.textAlignment=NSTextAlignmentRight;
        _numLabel.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _numLabel.text=[NSString stringWithFormat:@"%ld/400",_importInfoView.text.length];
        [_cnterView addSubview:_numLabel];
        
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_importInfoView);
            make.right.equalTo(_importInfoView).offset(-4);
        }];
    }
    return _numLabel;
}


#pragma mark - 提交
- (QWButtonView*)buttonView{
    if(!_buttonView){
        _buttonView= [[QWButtonView alloc]init];
        [_buttonView changeButtonTitle:@"提交"];
        __weak __typeof(self) weakSelf = self;
        _buttonView.buttonBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf commit];
        };
        
        [_containerView addSubview:_buttonView];
        
        [_buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(_cnterView.mas_bottom);
            make.height.mas_equalTo(95.0);
        }];
        
    }
    return _buttonView;
}

#pragma mark - 提交-请求
- (void)commit{
    NSString *reportctx = _importInfoView.text;
    if(IsStringEmpty(reportctx) || [reportctx isEqualToString: textViewPlaceholder]){
        [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"内容不能为空"];
        return;
    }
    
    NSMutableDictionary *dic =[[NSMutableDictionary alloc] initWithDictionary:@{
        @"autoTest": @"autoLogin",
        @"questionType": @"appOther",
    }];
    
    [dic setValue:AppProfile.qaUserInfo.phone forKey:@"userName"];
    [dic setValue:AppProfile.qaUserInfo.password forKey:@"password"];
    [dic setValue:reportctx forKey:@"reportctx"];

    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    HXPostRequest * request = [[HXPostRequest alloc] initWithRequestUrl:URL_FEEDBACK argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        if([result[@"message"] isEqualToString:@"OK"]){
            [SWHUDUtil hideHudViewWithSuccessMessage:@"提交成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });

        }else {
            [SWHUDUtil hideHudViewWithFailureMessage:@"网络异常，请重试！"];
        }
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {

        [SWHUDUtil hideHudView];
    }];
    
}

@end
