//
//  QWMineController.m
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import "QWMineController.h"
#import "QWMineInfoView.h"
#import "QWLoginController.h"
#import "QWMineMenusCell.h"
#import "QWButtonView.h"
#import "QWFeedbackController.h"
#import "QWTipsAlertController.h"
#import "QWAboutUsController.h"
#import "QWShieldController.h"

@interface QWMineController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QWMineInfoView *mineInfoView;
@property (nonatomic, strong) NSArray *menusArr;
@end

@implementation QWMineController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [_tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的";
        
    self.menusArr = @[
    @{
        @"name": @"后续反馈",
        @"icon": @"ic_feedback.png",
    },
    @{
        @"name": @"关于我们",
        @"icon": @"ic_about.png",
    },
    @{
        @"name": @"当前版本",
        @"icon": @"ic_version.png",
    },
    @{
        @"name": @"屏蔽用户",
        @"icon": @"ic_shield.png",
    }];
    [self tableView];
    [self mineInfoView];
}

#pragma mark- 个人信息
-(QWMineInfoView *)mineInfoView
{
    if (!_mineInfoView) {
        _mineInfoView =[[QWMineInfoView alloc]init];
        [self.view addSubview:_mineInfoView];
        [_mineInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.height.mas_equalTo(Mine_Header_Height);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _mineInfoView.loginBlock = ^{
            QWLoginController *loginVC =[[QWLoginController alloc]init];
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [loginVC setHidesBottomBarWhenPushed:YES];
            [strongSelf.navigationController pushViewController:loginVC animated:YES];
        };
    }
    return _mineInfoView;
}

- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.contentInset=UIEdgeInsetsMake(Mine_Header_Height, 0, 0, 0);
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerNib:[UINib nibWithNibName:@"QWMineMenusCell" bundle:nil] forCellReuseIdentifier:@"QWMineMenusCell"];
//        [_tableView registerNib:[UINib nibWithNibName:@"ZBMineHomeCell" bundle:nil] forCellReuseIdentifier:@"ZBMineHomeCell"];
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}


#pragma mark- UITableViewDelegate,UITableViewDataSource


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 85.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menusArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWMineMenusCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWMineMenusCell"];
    [cell changeRightShow:indexPath.row == 2];
    NSDictionary *dic = self.menusArr[indexPath.row];
    cell.iconImageView.image = [UIImage imageNamed:dic[@"icon"]];
    cell.nameLabel.text = dic[@"name"];
    if(indexPath.row == 2){
        cell.desLabel.text = [[QWCommonMethod Instance] currentVersion];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row == 1){        
        QWAboutUsController *aboutUsVC = [[QWAboutUsController alloc]init];
        aboutUsVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:aboutUsVC animated:YES];
    }
    if(indexPath.row == 0){
        if([AppProfile isLoginedAndShowLogintAlert]){
            QWFeedbackController *fdvc = [[QWFeedbackController alloc]init];
            fdvc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:fdvc animated:YES];
        }
    }
    
    if(indexPath.row == 3){
        if([AppProfile isLoginedAndShowLogintAlert]){
            QWShieldController *shieldVC = [[QWShieldController alloc]init];
            shieldVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:shieldVC animated:YES];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    QWButtonView *view = [[QWButtonView alloc]init];
    view.superVC = self;
    [view changeButtonState: AppProfile.qaUserInfo != nil];
    __weak __typeof(self) weakSelf = self;
    view.logoutBlock = ^{
        NSLog(@"退出登录");
        [AppProfile clearUserInfo];
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.tableView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:QWLogin_Change_State object:nil];
    };
    return  view;
}


#pragma mark- 个人信息view移动
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat scroll_y=-scrollView.contentOffset.y;
    if (scroll_y <= Mine_Header_Height) {
        if(scroll_y<= [UIDevice safeDistanceTop]){
            [_mineInfoView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo([UIDevice safeDistanceTop]-Mine_Header_Height);
                make.height.mas_equalTo(Mine_Header_Height);
            }];
        }else{
            [_mineInfoView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(scroll_y-Mine_Header_Height);
                make.height.mas_equalTo(Mine_Header_Height);
            }];
        }
    }else
    {
        [_mineInfoView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view);
            make.height.mas_equalTo(scroll_y<=Mine_Header_Height?Mine_Header_Height:scroll_y);
        }];
    }
}



@end
