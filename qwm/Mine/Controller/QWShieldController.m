//
//  QWShieldController.m
//  qwm
//
//  Created by kevin on 2023/5/9.
//

#import "QWShieldController.h"
#import "QWShieldCell.h"
#import "QWConversModel.h"

@interface QWShieldController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showInfoArr;
@end

@implementation QWShieldController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"屏蔽用户";

    [self tableView];
    [self loadData];
}

- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWShieldCell class] forCellReuseIdentifier:@"QWShieldCell"];
        _tableView.backgroundColor= Color_TableView_Gray;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}


#pragma mark- UITableViewDelegate,UITableViewDataSource


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.showNoDataView = self.showInfoArr.count == 0;
    return self.showInfoArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWShieldCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWShieldCell"];
    QWAllTopicModel *model = _showInfoArr[indexPath.row];
    cell.model = model;
    cell.cancelShieldBlock = ^{
        [self cancelShield:model];
    };
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - 解除
- (void)cancelShield:(QWAllTopicModel *)model{
    [[QWCommonMethod Instance] ShowOldAlert:@"提示" Msg:@"是否解除该用户的话题？" withSure:^{
        NSMutableDictionary *dic = model.mj_keyValues;
        [[WPlistManager SharedInstance] deleteConversationsWith:dic];
        
        [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SWHUDUtil hideHudView];
            NSArray *plistArr = [[WPlistManager SharedInstance] getConversations];
            [self.showInfoArr removeAllObjects];
            self.showInfoArr =[QWAllTopicModel mj_objectArrayWithKeyValuesArray:plistArr];
            [self.tableView reloadData];
        });
    }];
}

#pragma mark - 数据
- (void)loadData{
    NSArray *plistarr = [[WPlistManager SharedInstance] getConversations];
    [self.showInfoArr removeAllObjects];
    self.showInfoArr =[QWAllTopicModel mj_objectArrayWithKeyValuesArray:plistarr];
    [self.tableView reloadData];
}
-(NSMutableArray *)showInfoArr
{
    if (!_showInfoArr) {
        _showInfoArr=[[NSMutableArray alloc]init];
    }
    return _showInfoArr;
}

@end
