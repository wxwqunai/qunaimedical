//
//  QWMineMenusCell.m
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWMineMenusCell.h"

@implementation QWMineMenusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)changeRightShow:(BOOL)isShowDes{
    self.desLabel.hidden = !isShowDes;
    self.arrImageView.hidden = isShowDes;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
