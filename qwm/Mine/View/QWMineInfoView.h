//
//  QWMineInfoView.h
//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <UIKit/UIKit.h>
#define Mine_Header_Height 220

NS_ASSUME_NONNULL_BEGIN

typedef void (^LoginBlock)(void);

@interface QWMineInfoView : UIView

@property (nonatomic,copy)LoginBlock loginBlock;

@end

NS_ASSUME_NONNULL_END
