//
//  QWButtonView.m
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWButtonView.h"
@interface QWButtonView()
@property (nonatomic, strong) UIButton *logoutButton;

@end

@implementation QWButtonView

-(instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self logoutButton];
    }
    return self;
}

-(UIButton *)logoutButton
{
    if (!_logoutButton) {
        _logoutButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_logoutButton setTitle:@"退出" forState:UIControlStateNormal];
        [_logoutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _logoutButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _logoutButton.backgroundColor = Color_Main_Green;
        [_logoutButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _logoutButton.layer.cornerRadius = 6;
        _logoutButton.layer.masksToBounds = YES;
        
        [self addSubview:_logoutButton];
        
        [_logoutButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(18);
            make.right.equalTo(self).offset(-18);
            make.top.equalTo(self).offset(25);
            make.bottom.equalTo(self).offset(-10);
        }];
    }
    return _logoutButton;
}
-(void)buttonClick:(UIButton *)sender
{
    if(self.buttonBlock) self.buttonBlock();
    if(self.logoutBlock) [self showAlert];
}

- (void)changeButtonTitle:(NSString *)title{
    [_logoutButton setTitle:title forState:UIControlStateNormal];
}

#pragma mark - 弹框

- (void)showAlert{
    
    UIAlertController *genderAlert =[UIAlertController alertControllerWithTitle:@"确认退出？" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 =[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(self.logoutBlock){
            self.logoutBlock();
        }
    }];
    UIAlertAction *action2=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
//    //修改title
//    NSMutableAttributedString *alertControllerStr = [[NSMutableAttributedString alloc] initWithString:genderAlert.title];
//    [alertControllerStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorFromHexString:@"#333333"] range:NSMakeRange(0, genderAlert.title.length)];
//    [alertControllerStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(0, genderAlert.title.length)];
//    [genderAlert setValue:alertControllerStr forKey:@"attributedTitle"];
//
//    //修改按钮的颜色
//    [action1 setValue:[UIColor colorFromHexString:@"#333333"] forKey:@"titleTextColor"];
//    [action2 setValue:[UIColor colorFromHexString:@"#333333"] forKey:@"titleTextColor"];
    
    [genderAlert addAction:action1];
    [genderAlert addAction:action2];
    
    [self.superVC presentViewController:genderAlert animated:YES completion:nil];
}

#pragma mark - 切换按钮状态-可点击/不可点击
- (void)changeButtonState:(BOOL)isCanClick{
//    [self.logoutButton setEnabled:isCanClick];
    self.logoutButton.backgroundColor = isCanClick?Color_Main_Green:[UIColor lightGrayColor];
    self.logoutButton.hidden = !isCanClick;
}


@end
