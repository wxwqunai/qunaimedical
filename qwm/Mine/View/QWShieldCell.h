//
//  QWShieldCell.h
//  qwm
//
//  Created by kevin on 2023/5/9.
//

#import "QWBaseTableViewCell.h"
#import "QWConversModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^CancelShieldBlock)(void); //解除屏蔽

@interface QWShieldCell : QWBaseTableViewCell

@property (nonatomic,strong) QWAllTopicModel *model;

@property (nonatomic,copy)CancelShieldBlock cancelShieldBlock;

@end

NS_ASSUME_NONNULL_END
