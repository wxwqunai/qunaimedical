//
//  QWMineInfoView.m
//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import "QWMineInfoView.h"

@interface QWMineInfoView ()
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *logRegLabel;
@property (nonatomic, strong) UIButton *logRegButtun;

@end

@implementation QWMineInfoView

-(instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = Color_Main_Green;
        [self headImageView];
        [self logRegLabel];
        [self logRegButtun];
        
        
        [self loginStateChangeNotification];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginStateChangeNotification) name:QWLogin_Change_State object:nil];
    }
    return self;
}

#pragma mark - 头像
- (UIImageView *)headImageView{
    if(!_headImageView){
        _headImageView = [[UIImageView alloc]init];
        NSString *shichenName=[[QWCommonMethod Instance] calculateOneOfTheTwelveHourPeriods];
        NSString *imageNameStr =[NSString stringWithFormat:@"qw_%@.png",shichenName];
        UIImage *image =[UIImage imageNamed:imageNameStr];
        _headImageView.image =image;
        _headImageView.hidden = YES;
        [self addSubview:_headImageView];
        
        [_headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.mas_equalTo(20);
            make.width.mas_equalTo(60);
            make.height.mas_equalTo(60);
        }];
    }
    return _headImageView;
}

- (UILabel *)logRegLabel{
    if(!_logRegLabel){
        _logRegLabel = [[UILabel alloc]init];
        _logRegLabel.text =@"xxx";
        _logRegLabel.font =[UIFont boldSystemFontOfSize:24];
        _logRegLabel.textColor =[UIColor whiteColor];
        _logRegLabel.hidden =YES;
        
        [self addSubview:_logRegLabel];
        
        [_logRegLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(_headImageView.mas_right).offset(8);
            make.right.equalTo(self).offset(-20);
        }];
    }
    return _logRegLabel;
}

-(UIButton *)logRegButtun
{
    if (!_logRegButtun) {
        _logRegButtun =[UIButton buttonWithType:UIButtonTypeCustom];
        [_logRegButtun setTitle:@"登陆/注册" forState:UIControlStateNormal];
        [_logRegButtun setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _logRegButtun.titleLabel.font = [UIFont systemFontOfSize:24];
        _logRegButtun.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _logRegButtun.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 6);
        [_logRegButtun addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_logRegButtun];
        
        [_logRegButtun mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.mas_equalTo(20);
            make.width.equalTo(self).offset(-40);
        }];
    }
    return _logRegButtun;
}
-(void)buttonClick:(UIButton *)sender
{
    if(self.loginBlock){
        self.loginBlock();
    }
}


#pragma mark - 接受通知

- (void)loginStateChangeNotification {
 
//    NSLog(@"接受通知==%@",AppProfile.qaUserInfo);
    
    if(AppProfile.qaUserInfo){
        self.logRegLabel.hidden =NO;
        self.headImageView.hidden = NO;
        self.logRegButtun.hidden = YES;
        
        QWqaUserModel *qaUserInfo = AppProfile.qaUserInfo;
        self.logRegLabel.text = qaUserInfo.name;
        
    }else{
        self.logRegLabel.hidden = YES;
        self.headImageView.hidden = YES;
        self.logRegButtun.hidden = NO;
        
        
    }
}


- (void)dealloc{
    //移除观察者 self
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
