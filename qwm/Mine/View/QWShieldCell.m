//
//  QWShieldCell.m
//  qwm
//
//  Created by kevin on 2023/5/9.
//

#import "QWShieldCell.h"
@interface QWShieldCell ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *replyButton;//回复
@end

@implementation QWShieldCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.top.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView).offset(-10);
            
            make.height.mas_greaterThanOrEqualTo(80);
        }];
        
        [self picImageView];
        [self nameLabel];
        [self replyButton];
    }
    return _bgView;
}

#pragma mark - 头像
- (UIImageView *)picImageView{
    if(!_picImageView){
        CGFloat wid = 60.0;
        _picImageView = [[UIImageView alloc]init];
        _picImageView.image =[UIImage imageNamed:@"c_header_man"];
        _picImageView.backgroundColor = [UIColor lightGrayColor];
        _picImageView.layer.cornerRadius = wid/2.0;
        _picImageView.layer.masksToBounds = YES;
        [_bgView addSubview:_picImageView];
        
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(14);
            make.centerY.equalTo(_bgView);
            make.height.mas_equalTo(wid);
            make.width.mas_equalTo(wid);
        }];
    }
    return _picImageView;
}

#pragma mark - 用户
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.numberOfLines = 2;
        _nameLabel.font = [UIFont systemFontOfSize:17];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.text=@"";
//        _nameLabel.backgroundColor = [UIColor yellowColor];
        [_bgView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_bgView);
            make.left.equalTo(_picImageView.mas_right).offset(8.0);
        }];
    }
    return _nameLabel;
}

#pragma mark - 解除
- (UIButton *)replyButton{
    if(!_replyButton){
        _replyButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_replyButton setTitle:@"解除" forState:UIControlStateNormal];
        [_replyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _replyButton.titleLabel.font =[UIFont systemFontOfSize:17];
        _replyButton.backgroundColor = Color_Main_Green;
        _replyButton.layer.cornerRadius = 15.0f;
        _replyButton.layer.masksToBounds = YES;
        [_replyButton addTarget:self action:@selector(replyButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:_replyButton];
        
        [_replyButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_bgView);
            make.right.equalTo(_bgView).offset(-14);
            make.left.equalTo(_nameLabel.mas_right).offset(8);
            make.height.mas_equalTo(30);
            make.width.mas_equalTo(60);
        }];
        
    }
    return _replyButton;
}
-(void)replyButtonClick{
    if(self.cancelShieldBlock)self.cancelShieldBlock();
}

#pragma mark - 数据
- (void)setModel:(QWAllTopicModel *)model
{
    _model = model;
    if(!IsStringEmpty(model.msgfromname)) _nameLabel.text = model.msgfromname;
    _picImageView.image =[UIImage imageNamed:model.msgfromname.length%2==1?@"c_header_women":@"c_header_man"];
}

@end
