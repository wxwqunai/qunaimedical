//
//  QWComplaintCell.h
//  qwm
//
//  Created by kevin on 2023/5/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWComplaintCell : UICollectionViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, copy) NSString *nameStr;
@property (nonatomic, assign) BOOL isSelected;
@end

NS_ASSUME_NONNULL_END
