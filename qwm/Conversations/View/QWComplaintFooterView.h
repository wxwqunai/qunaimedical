//
//  QWComplaintFooterView.h
//  qwm
//
//  Created by kevin on 2023/5/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^CommitBlock)(void);

@interface QWComplaintFooterView : UICollectionReusableView
@property (nonatomic,copy) CommitBlock commitBlock;

@end

NS_ASSUME_NONNULL_END
