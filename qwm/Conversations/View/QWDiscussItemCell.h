//
//  QWDiscussItemCell.h
//  qwm
//
//  Created by kevin on 2023/4/18.
//

#import "QWBaseTableViewCell.h"
#import "QWConversModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^ReplyBlock)(void);
typedef void (^ShieldBlock)(void); //屏蔽
typedef void (^ComplaintBlock)(void); //投诉

@interface QWDiscussItemCell : QWBaseTableViewCell

@property (nonatomic,copy)ReplyBlock replyBlock;
@property (nonatomic,copy)ShieldBlock shieldBlock;
@property (nonatomic,copy)ComplaintBlock complaintBlock;

@property (nonatomic,strong) QWAllTopicModel *model;
@end

NS_ASSUME_NONNULL_END
