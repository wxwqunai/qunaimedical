//
//  QWComplaintFooterView.m
//  qwm
//
//  Created by kevin on 2023/5/10.
//

#import "QWComplaintFooterView.h"
#import "QWButtonView.h"

@interface QWComplaintFooterView()
@property (nonatomic,strong) QWButtonView *buttonView;
@end

@implementation QWComplaintFooterView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        [self buttonView];
    }
    return self;
}

#pragma mark - 提交
- (QWButtonView*)buttonView{
    if(!_buttonView){
        _buttonView= [[QWButtonView alloc]init];
        [_buttonView changeButtonTitle:@"提交"];
        _buttonView.buttonBlock = self.commitBlock;
        [self addSubview:_buttonView];
        
        [_buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
    }
    return _buttonView;
}

- (void)setCommitBlock:(CommitBlock)commitBlock{
    _commitBlock = commitBlock;
    _buttonView.buttonBlock = _commitBlock;
}


@end
