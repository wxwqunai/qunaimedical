//
//  QWCommentItemView.h
//  qwm
//
//  Created by kevin on 2023/4/18.
//

#import <UIKit/UIKit.h>
#import "QWConversModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWCommentItemView : UIView
@property (nonatomic,strong) QWTopicAnswersModel *answerModel;
@end

NS_ASSUME_NONNULL_END
