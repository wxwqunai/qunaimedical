//
//  QWComplaintHeaderView.m
//  qwm
//
//  Created by kevin on 2023/5/10.
//

#import "QWComplaintHeaderView.h"

@interface QWComplaintHeaderView()
@property (nonatomic, strong) UIView *tipsView;
@property (nonatomic, strong) UILabel *tipsLabel;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *topicLabel;
@end

@implementation QWComplaintHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        [self tipsView];
        
        [self titleLabel];
        [self nameLabel];
        [self topicLabel];
    }
    return self;
}
#pragma mark - 提示
- (UIView *)tipsView{
    if(!_tipsView){
        _tipsView = [[UIView alloc]init];
        _tipsView.backgroundColor =[UIColor colorFromHexString:@"#F9EEE0"];
        [self addSubview:_tipsView];
        
        [_tipsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.mas_equalTo(45);
        }];
        
        [self tipsLabel];

    }
    return _tipsView;
}
#pragma mark - 提示内容
- (UILabel *)tipsLabel{
    if(!_tipsLabel){
        _tipsLabel = [[UILabel alloc]init];
        _tipsLabel.font = [UIFont systemFontOfSize:18];
        _tipsLabel.textColor = [UIColor colorFromHexString:@"#CD8644"];
        _tipsLabel.text=@"您的投诉将在24小时内受理。";
        [_tipsView addSubview:_tipsLabel];
        
        [_tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_tipsView).offset(14.0);
            make.right.equalTo(_tipsView).offset(-14.0);
            make.centerY.equalTo(_tipsView);
        }];
    }
    return _tipsLabel;
}

#pragma mark - 投诉
- (UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.font = [UIFont systemFontOfSize:18];
        _titleLabel.text=@"投诉：";
        [self addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(14.0);
            make.top.equalTo(_tipsView.mas_bottom).offset(10.0);
        }];
    }
    return _titleLabel;
}

#pragma mark - 用户
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.numberOfLines = 0;
        _nameLabel.font = [UIFont systemFontOfSize:18];
        _nameLabel.textColor = [UIColor colorFromHexString:@"#5D6C8E"];
        _nameLabel.text=self.nameStr;
//        _nameLabel.backgroundColor = [UIColor yellowColor];
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_titleLabel);
            make.left.equalTo(_titleLabel.mas_right);
            make.right.equalTo(self).offset(-14);
        }];
    }
    return _nameLabel;
}
- (void)setNameStr:(NSString *)nameStr{
    _nameStr = nameStr;
    _nameLabel.text=_nameStr;

}

#pragma mark - 话题
- (UILabel *)topicLabel{
    if(!_topicLabel){
        _topicLabel = [[UILabel alloc]init];
        _topicLabel.numberOfLines = 1;
        _topicLabel.text = self.contentStr;
        _topicLabel.font = [UIFont systemFontOfSize:16];
        _topicLabel.textColor = [UIColor colorFromHexString:@"#aaaaaa"];
        [self addSubview:_topicLabel];
        
        [_topicLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(20.0);
            make.right.equalTo(self).offset(-14);
            make.top.equalTo(_nameLabel.mas_bottom).offset(4);
            make.bottom.equalTo(self);
        }];
    }
    return _topicLabel;
}
- (void)setContentStr:(NSString *)contentStr{
    _contentStr = contentStr;
    _topicLabel.text = _contentStr;
}

@end
