//
//  QWCommentItemView.m
//  qwm
//
//  Created by kevin on 2023/4/18.
//

#import "QWCommentItemView.h"
@interface QWCommentItemView ()
@property (nonatomic, strong) UILabel *nameLabel;
@end

@implementation QWCommentItemView

-(instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self nameLabel];
    }
    return self;
}

#pragma mark - 姓名
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.numberOfLines = 0;
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.textColor = [UIColor colorFromHexString:@"#5ab5ef"];
        _nameLabel.text=@"";
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self).offset(4);
            make.bottom.equalTo(self).offset(-4);
        }];
    }
    return _nameLabel;
}
- (void)setAnswerModel:(QWTopicAnswersModel *)answerModel
{
    _answerModel = answerModel;
    
    NSString *nameStr = _answerModel.msgfromname;
    NSString *replyStr = _answerModel.content;
    
    if(IsStringEmpty(nameStr) && IsStringEmpty(replyStr)) return;
    
    NSString * showText = [NSString stringWithFormat:@"%@：%@",nameStr,replyStr];
    NSString * aggrementStr = [NSString stringWithFormat:@"%@：",nameStr];
    
    NSMutableAttributedString * attString = [[NSMutableAttributedString alloc] initWithString:showText];
    NSDictionary * attDic = @{
                              NSFontAttributeName : [UIFont systemFontOfSize:14],
                              NSForegroundColorAttributeName : [UIColor blackColor]
                              };
    [attString setAttributes:attDic range:NSMakeRange(0, showText.length)];
    [attString addAttributes:@{NSForegroundColorAttributeName : [UIColor colorFromHexString:@"#5ab5ef"]} range:[showText rangeOfString:aggrementStr]];
    self.nameLabel.attributedText = attString;
    
    [self.nameLabel yb_addAttributeTapActionWithStrings:@[aggrementStr] tapClicked:^(UILabel *label, NSString *string, NSRange range, NSInteger index) {
        
        NSLog(@"%@",string);
            
    }];
}



@end
