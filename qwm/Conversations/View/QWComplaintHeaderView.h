//
//  QWComplaintHeaderView.h
//  qwm
//
//  Created by kevin on 2023/5/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWComplaintHeaderView : UICollectionReusableView
@property (nonatomic, copy) NSString *nameStr;
@property (nonatomic, copy) NSString *contentStr;
@end

NS_ASSUME_NONNULL_END
