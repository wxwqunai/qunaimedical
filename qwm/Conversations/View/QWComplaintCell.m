//
//  QWComplaintCell.m
//  qwm
//
//  Created by kevin on 2023/5/10.
//

#import "QWComplaintCell.h"

@implementation QWComplaintCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorFromHexString:@"#F4F4F4"];
        
//        [self replyButton];
        [self nameLabel];
    }
    return self;
}



//#pragma mark -
//- (UIButton *)replyButton{
//    if(!_replyButton){
//        _replyButton =[UIButton buttonWithType:UIButtonTypeCustom];
//        [_replyButton setTitle:self.nameStr forState:UIControlStateNormal];
//        [_replyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        _replyButton.titleLabel.font =[UIFont systemFontOfSize:17];
//        _replyButton.backgroundColor = Color_Main_Green;
//        _replyButton.layer.cornerRadius = 15.0f;
//        _replyButton.layer.masksToBounds = YES;
//        [_replyButton addTarget:self action:@selector(replyButtonClick) forControlEvents:UIControlEventTouchUpInside];
//        [self.contentView addSubview:_replyButton];
//
//        [_replyButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.edges.equalTo(self.contentView);
//        }];
//
//    }
//    return _replyButton;
//}
//-(void)replyButtonClick{
////    if(self.replyBlock)self.replyBlock();
//}

#pragma mark - 用户
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.text=self.nameStr;
//        _nameLabel.backgroundColor = [UIColor yellowColor];
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
    }
    return _nameLabel;
}

-(void)setNameStr:(NSString *)nameStr{
    _nameStr = nameStr;
    _nameLabel.text=self.nameStr;
}

-(void)setIsSelected:(BOOL)isSelected{
    _isSelected = isSelected;
    _nameLabel.textColor = _isSelected?[UIColor whiteColor]:[UIColor blackColor];
    self.backgroundColor = _isSelected?Color_Main_Green:[UIColor colorFromHexString:@"#F4F4F4"];
}

@end
