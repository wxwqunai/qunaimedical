//
//  QWDiscussItemCell.m
//  qwm
//
//  Created by kevin on 2023/4/18.
//

#import "QWDiscussItemCell.h"
#import "QWCommentItemView.h"

@interface QWDiscussItemCell ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UIButton *replyButton;//回复

@property (nonatomic, strong) UIView *topicView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *topicLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIButton *shieldButton; //屏蔽
@property (nonatomic, strong) UIButton *complaintButton; //投诉

@property (nonatomic, strong) UIView *commentView;
@end

@implementation QWDiscussItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.top.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView).offset(-10);
            
            make.height.mas_greaterThanOrEqualTo(110);
        }];
        
        [self picImageView];
        [self replyButton];
        [self topicView];
        [self commentView];
    }
    return _bgView;
}

#pragma mark - 头像
- (UIImageView *)picImageView{
    if(!_picImageView){
        CGFloat wid = 60.0;
        _picImageView = [[UIImageView alloc]init];
        _picImageView.image =[UIImage imageNamed:@"c_header_man"];
        _picImageView.backgroundColor = [UIColor lightGrayColor];
        _picImageView.layer.cornerRadius = wid/2.0;
        _picImageView.layer.masksToBounds = YES;
        [_bgView addSubview:_picImageView];
        
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(14);
            make.top.mas_equalTo(8.0);
            make.height.mas_equalTo(wid);
            make.width.mas_equalTo(wid);
        }];
    }
    return _picImageView;
}

#pragma mark - 回复
- (UIButton *)replyButton{
    if(!_replyButton){
        _replyButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_replyButton setTitle:@"回复" forState:UIControlStateNormal];
        [_replyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _replyButton.titleLabel.font =[UIFont systemFontOfSize:17];
        _replyButton.backgroundColor = Color_Main_Green;
        _replyButton.layer.cornerRadius = 15.0f;
        _replyButton.layer.masksToBounds = YES;
        [_replyButton addTarget:self action:@selector(replyButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:_replyButton];
        
        [_replyButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_picImageView);
            make.right.equalTo(_picImageView);
            make.top.equalTo(_picImageView.mas_bottom).offset(8);
            make.height.mas_equalTo(30);
        }];
        
    }
    return _replyButton;
}
-(void)replyButtonClick{
    if(self.replyBlock)self.replyBlock();
}



#pragma mark - 话题-view
- (UIView *)topicView{
    if(!_topicView){
        _topicView = [[UIView alloc]init];
        _topicView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_topicView];
        
        [_topicView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_picImageView.mas_right).offset(14);
            make.top.equalTo(_picImageView.mas_top);
            make.right.equalTo(self.contentView).offset(-14);
        }];

        [self nameLabel];
        [self shieldButton];
        [self complaintButton];
        
        [self topicLabel];
        [self timeLabel];
    }
    return _topicView;
}
#pragma mark - 用户
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.numberOfLines = 2;
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.textColor = [UIColor colorFromHexString:@"#aaaaaa"];
        _nameLabel.text=@"";
//        _nameLabel.backgroundColor = [UIColor yellowColor];
        [_topicView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_topicView);
            make.top.equalTo(_topicView);
            make.height.mas_greaterThanOrEqualTo(20);
        }];
    }
    return _nameLabel;
}

#pragma mark - 屏蔽
- (UIButton *)shieldButton{
    if(!_shieldButton){
        _shieldButton =[self createButton];
        [_shieldButton setTitle:@"屏蔽" forState:UIControlStateNormal];
        [_shieldButton addTarget:self action:@selector(shieldButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_topicView addSubview:_shieldButton];
        
        [_shieldButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_topicView);
            make.centerY.equalTo(_nameLabel);
            make.width.mas_equalTo(40.0);
        }];
        
    }
    return _shieldButton;
}
-(void)shieldButtonClick{
    if(self.shieldBlock)self.shieldBlock();
}

#pragma mark - 投诉
- (UIButton *)complaintButton{
    if(!_complaintButton){
        _complaintButton =[self createButton];
        [_complaintButton setTitle:@"投诉" forState:UIControlStateNormal];
        [_complaintButton addTarget:self action:@selector(complaintButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_topicView addSubview:_complaintButton];
        
        [_complaintButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_shieldButton.mas_left).offset(-8);
            make.centerY.equalTo(_nameLabel);
            make.left.equalTo(_nameLabel.mas_right).offset(8.0);
            make.width.mas_equalTo(40.0);
        }];
        
    }
    return _complaintButton;
}
-(void)complaintButtonClick{
    if(self.complaintBlock)self.complaintBlock();
}

- (UIButton *)createButton{
    UIButton *button =[UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.titleLabel.font =[UIFont systemFontOfSize:12];
    button.backgroundColor = [UIColor whiteColor];
    // 阴影的颜色
    button.layer.shadowColor = [UIColor blackColor].CGColor;
    button.layer.shadowOpacity = 0.8;
    // 阴影的圆角
    button.layer.shadowRadius = 1;
    // 阴影偏离的位置 (100, 50) x 方向偏离 100，y 偏离 50 正向，如果是负数正好为相反的方向
    button.layer.shadowOffset = CGSizeMake(0, 0);
    
    return button;
}

#pragma mark - 话题
- (UILabel *)topicLabel{
    if(!_topicLabel){
        _topicLabel = [[UILabel alloc]init];
        _topicLabel.numberOfLines = 0;
        _topicLabel.font = [UIFont systemFontOfSize:17];
        _topicLabel.textColor = [UIColor blackColor];
//        _topicLabel.backgroundColor = [UIColor redColor];
        [_topicView addSubview:_topicLabel];
        
        [_topicLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_topicView);
            make.right.equalTo(_topicView);
            make.top.equalTo(_nameLabel.mas_bottom).offset(8);
        }];
    }
    return _topicLabel;
}
#pragma mark - 时间
- (UILabel *)timeLabel{
    if(!_timeLabel){
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.numberOfLines = 1;
        _timeLabel.font = [UIFont systemFontOfSize:14];
        _timeLabel.textColor = [UIColor colorFromHexString:@"#aaaaaa"];
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [_topicView addSubview:_timeLabel];
        
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_topicView);
            make.right.equalTo(_topicView);
            make.top.equalTo(_topicLabel.mas_bottom).offset(8);
            make.bottom.equalTo(_topicView);
        }];
    }
    return _timeLabel;
}

#pragma mark - 评论-view
- (UIView *)commentView{
    if(!_commentView){
        _commentView = [[UIView alloc]init];
        _commentView.backgroundColor = [UIColor colorFromHexString:@"#eeeeee"];
        [_bgView addSubview:_commentView];
        
        [_commentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_picImageView.mas_right).offset(14);
            make.right.equalTo(_bgView).offset(-14);
            make.bottom.equalTo(_bgView).offset(-4);
            make.top.equalTo(_topicView.mas_bottom).offset(8.0);

        }];
    }
    return _commentView;
}

- (void)addAnswersView:(NSArray *)arr{
    [self.commentView removeFromSuperview];
    self.commentView = nil;
    
    QWCommentItemView *lastView= nil;
    for (int i = 0; i < arr.count; i++) {
        QWCommentItemView * commentItemView = [[QWCommentItemView alloc]init];
        QWTopicAnswersModel *model = arr[i];
        commentItemView.answerModel = model;
        [self.commentView addSubview:commentItemView];
        
        [commentItemView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_commentView).offset(8.0);
            make.right.equalTo(_commentView).offset(-8.0);
            if(lastView == nil){
                make.top.equalTo(_commentView).offset(5.0);
            }else{
                make.top.equalTo(lastView.mas_bottom);
            }
            if(i == arr.count-1) make.bottom.equalTo(_commentView).offset(-5.0);
        }];
        
        lastView = commentItemView;
        
    }
}


#pragma mark - 数据
- (void)setModel:(QWAllTopicModel *)model
{
    _model = model;
    if(!IsStringEmpty(model.msgfromname)) _nameLabel.text = model.msgfromname;
    if(!IsStringEmpty(model.content)) _topicLabel.text = model.content;
    if(!IsStringEmpty(model.chattime)) _timeLabel.text = model.chattime;
    
    _picImageView.image =[UIImage imageNamed:model.msgfromname.length%2==1?@"c_header_women":@"c_header_man"];

    _commentView.backgroundColor = [UIColor whiteColor];
    if(model.answersArr.count>0){
        _commentView.backgroundColor = [UIColor colorFromHexString:@"#eeeeee"];
        [self addAnswersView:model.answersArr];
    }
}

@end
