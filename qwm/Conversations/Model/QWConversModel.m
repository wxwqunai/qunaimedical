//
//  QWConversModel.m
//  qwm
//
//  Created by kevin on 2023/4/25.
//

#import "QWConversModel.h"

@implementation QWConversModel

@end

@implementation QWTopicAnswersModel

@end

@implementation QWAllTopicModel
- (void)setAnswers_json:(NSString *)answers_json{
    _answers_json = answers_json;
    NSArray *arr = [answers_json mj_JSONObject];
    self.answersArr = [QWTopicAnswersModel mj_objectArrayWithKeyValuesArray:arr];
}
@end

@implementation QWComplaintModel

@end
