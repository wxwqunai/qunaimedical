//
//  QWConversModel.h
//  qwm
//
//  Created by kevin on 2023/4/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWConversModel : NSObject

@end

@interface QWTopicAnswersModel : NSObject
@property (nonatomic, copy) NSString * chatroom;
@property (nonatomic, copy) NSString * chattime;
@property (nonatomic, copy) NSString * content;
@property (nonatomic, copy) NSString * extdata1;
@property (nonatomic, copy) NSString * extdata2;
@property (nonatomic, copy) NSString * extprop;
@property (nonatomic, assign) long  id;
@property (nonatomic, assign) long  id_sub;
@property (nonatomic, copy) NSString * msgfromid;
@property (nonatomic, copy) NSString * msgfromname;
@property (nonatomic, copy) NSString * msgtoid;
@property (nonatomic, copy) NSString * msgtoname;
@property (nonatomic, copy) NSString * msgtype;
@property (nonatomic, copy) NSString * remark;
@property (nonatomic, copy) NSString * reverse1;
@property (nonatomic, copy) NSString * reverse2;
@property (nonatomic, copy) NSString * state;

@end

@interface QWAllTopicModel : NSObject
@property (nonatomic, assign) long id;
@property (nonatomic, copy) NSString * msgfromname;
@property (nonatomic, copy) NSString * content;
@property (nonatomic, copy) NSString * chattime;
@property (nonatomic, copy) NSString * chatroom;
@property (nonatomic, copy) NSString * extdata1;
@property (nonatomic, copy) NSString * extdata2;
@property (nonatomic, copy) NSString * extprop;
@property (nonatomic, copy) NSString * msgToPost;
@property (nonatomic, copy) NSString * msgfromid;
@property (nonatomic, copy) NSString * msgtoname;
@property (nonatomic, copy) NSString * msgtype;
@property (nonatomic, copy) NSString * remark;
@property (nonatomic, copy) NSString * reverse1;
@property (nonatomic, copy) NSString * reverse2;
@property (nonatomic, copy) NSString * state;
@property (nonatomic, copy) NSString *answers_json;
@property (nonatomic, strong) NSMutableArray *answersArr;
@end

@interface QWComplaintModel : NSObject
@property (nonatomic, copy) NSString * name;
@property (nonatomic, assign) BOOL isSelected;
@end


NS_ASSUME_NONNULL_END
