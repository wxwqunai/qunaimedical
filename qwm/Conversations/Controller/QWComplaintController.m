//
//  QWComplaintController.m
//  qwm
//
//  Created by kevin on 2023/5/10.
//

#import "QWComplaintController.h"
#import "QWComplaintCell.h"
#import "QWConversModel.h"
#import "QWComplaintFooterView.h"
#import "QWComplaintHeaderView.h"

@interface QWComplaintController ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *showInfoArr;

@end

@implementation QWComplaintController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"投诉";
    
    [self flowLayout];
    [self collectionView];
    [self loadData];
}

#pragma mark - 对集合视图进行布局
-(UICollectionViewFlowLayout *)flowLayout
{
    if (!_flowLayout) {
        // 创建集合视图布局对象
        _flowLayout = [[UICollectionViewFlowLayout alloc] init];
        // 设置最小的左右间距
        _flowLayout.minimumInteritemSpacing = 15.0;
        // 设置最小的行间距（上下）
        _flowLayout.minimumLineSpacing = 15.0;
        _flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        // 设置边界范围
        _flowLayout.sectionInset = UIEdgeInsetsMake(10.0, 15.0, 10.0, 15.0);
        _flowLayout.headerReferenceSize =CGSizeMake(kScreenWidth, 105);
        _flowLayout.footerReferenceSize =CGSizeMake(kScreenWidth, 100);
        if (@available(iOS 9.0, *)) {
            _flowLayout.sectionHeadersPinToVisibleBounds = YES;
        }
    }
    return _flowLayout;
}
-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        _collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) collectionViewLayout:self.flowLayout];
        _collectionView.showsVerticalScrollIndicator=NO;
        _collectionView.showsHorizontalScrollIndicator=NO;
        _collectionView.backgroundColor=Color_TableView_Gray;
        [self.view addSubview:_collectionView];
        
        // 创建集合视图对象
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = YES;
        _collectionView.alwaysBounceVertical=YES;
        _collectionView.alwaysBounceHorizontal=NO;
        
        // 注册
        [_collectionView registerClass:[QWComplaintHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QWComplaintHeaderView"];
        [_collectionView registerClass:[QWComplaintFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"QWComplaintFooterView"];
        [_collectionView registerClass:[QWComplaintCell class] forCellWithReuseIdentifier:@"QWComplaintCell"];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    return _collectionView;
}
#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width =(kScreenWidth-15*2-15*2)/3.0;
    CGSize showSize=CGSizeMake(width, 50);
    return showSize;
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.showInfoArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QWComplaintCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"QWComplaintCell" forIndexPath:indexPath];
    QWComplaintModel *model = self.showInfoArr[indexPath.row];
    cell.nameStr = model.name;
    cell.isSelected = model.isSelected;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    QWComplaintModel *selModel = self.showInfoArr[indexPath.row];

    for (QWComplaintModel *model in  self.showInfoArr) {
        if([model.name isEqualToString:selModel.name]){
            model.isSelected = !model.isSelected;
        }else{
            model.isSelected = NO;
        }
    }
    
    [collectionView reloadData];
}

#pragma mark- 头部
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqual:UICollectionElementKindSectionHeader])
    {
        QWComplaintHeaderView *headerView= [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QWComplaintHeaderView" forIndexPath:indexPath];
        headerView.nameStr = self.topicModel.msgfromname;
        headerView.contentStr = self.topicModel.content;
        return headerView;
    }
    if ([kind isEqual:UICollectionElementKindSectionFooter])
    {
        QWComplaintFooterView *footerView= [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"QWComplaintFooterView" forIndexPath:indexPath];
        footerView.commitBlock = ^{
            [self commit];
        };
        return footerView;
    }
    return nil;
}
#pragma mark - 提交 - 请求
- (void)commit{
    NSMutableArray *upArr = [[NSMutableArray alloc]init];
    for (QWComplaintModel *model in  self.showInfoArr) {
        if(model.isSelected){
            [upArr addObject:model.mj_keyValues];
        }
    }
    
    if(upArr.count == 0){
        [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"请选择投诉分类"];
        return;
    }
    
    CGFloat value = ((arc4random() % 10) + 5)/10.0;
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(value * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SWHUDUtil hideHudViewWithSuccessMessage:@"投诉成功，我们将尽快处理！"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    });
}

#pragma mark - 数据
- (void)loadData{
    NSString *plistPath = [[NSBundle mainBundle]pathForResource:@"ReportInfo"ofType:@"plist"];
    NSArray *dataArr = [NSArray arrayWithContentsOfFile:plistPath];
    self.showInfoArr = [QWComplaintModel mj_objectArrayWithKeyValuesArray:dataArr];
    [_collectionView reloadData];
}

-(NSMutableArray *)showInfoArr
{
    if (!_showInfoArr) {
        _showInfoArr=[[NSMutableArray alloc]init];
    }
    return _showInfoArr;
}

@end
