//
//  QWConversationsController.m
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import "QWConversationsController.h"
#import "QWDiscussItemCell.h"
#import "QWConsDetalController.h"
#import "QWHealthyTipView.h"
#import "QWCommentController.h"
#import "QWConversModel.h"
#import "QWTipsAlertController.h"
#import "QWComplaintController.h"

static const int pageSize = 10;

@interface QWConversationsController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showInfoArr;

@end

@implementation QWConversationsController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
    self.isHiddenLeftBarButtonItem = YES;
    
//    if(self.showInfoArr.count == 0){
////        [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
//        [self loadPullDownRefreshDataInfo];
//    }
    [self loadPullDownRefreshDataInfo];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"消息";
    self.rightBarButtonItem.title=@"发布";

    [self tableView];
    
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    [self loadPullDownRefreshDataInfo];
}
#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
    if([AppProfile isLogined]){
        QWCommentController *plVC =[[QWCommentController alloc]init];
        plVC.sendType = SendType_addNew;
        plVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:plVC animated:YES];
    }else{
        QWTipsAlertController *activityVC =[[QWTipsAlertController alloc]init];
        activityVC.tipType = TipsAlertType_loginNot;
        activityVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        activityVC.modalPresentationStyle=UIModalPresentationOverFullScreen;
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:activityVC animated:NO completion:nil];
    }
}

- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWDiscussItemCell class] forCellReuseIdentifier:@"QWDiscussItemCell"];

        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;

        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
//        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return  0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.showNoDataView = self.showInfoArr.count == 0;
    return _showInfoArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWAllTopicModel *model = _showInfoArr[indexPath.row];
    NSString *MyIdentifier = [NSString stringWithFormat:@"QWDiscussItemCell_%ld_%@",indexPath.row,model.content];
    QWDiscussItemCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[QWDiscussItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    }
    cell.model = model;
    
    __weak __typeof(self) weakSelf = self;
    //回复
    cell.replyBlock = ^{
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if([AppProfile isLoginedAndShowLogintAlert]){
            QWCommentController *plVC =[[QWCommentController alloc]init];
            plVC.sendType = SendType_reply;
            plVC.replyModel = model;
            plVC.hidesBottomBarWhenPushed = YES;
            [strongSelf.navigationController pushViewController:plVC animated:YES];
        }
    };
    
    //投诉
    cell.complaintBlock = ^{
        [self complaint:model];
    };
    
    //屏蔽
    cell.shieldBlock = ^{
        [self shield:model];
    };
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark- 投诉
- (void)complaint:(QWAllTopicModel *)model{
    if([AppProfile isLoginedAndShowLogintAlert]){
        QWComplaintController *complaintVC =[[QWComplaintController alloc]init];
        complaintVC.topicModel = model;
        complaintVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:complaintVC animated:YES];
    }
}

#pragma mark- 屏蔽
- (void)shield:(QWAllTopicModel *)model{
    if([AppProfile isLoginedAndShowLogintAlert]){
        [[QWCommonMethod Instance] ShowOldAlert:@"提示" Msg:@"是否屏蔽该用户话题？" withSure:^{
            NSMutableDictionary *dic = model.mj_keyValues;
            [[WPlistManager SharedInstance] addConversationsWith:dic];
            [self deleteShieldData];
        }];
    }
}

- (void)deleteShieldData{
    NSArray *plistarr = [[WPlistManager SharedInstance] getConversations];
//    NSLog(@"plistarr====%@",plistarr);
    if(plistarr){
        NSMutableArray *mutableArray = [self.showInfoArr mutableCopy];
        for (NSDictionary *dic in plistarr) {
            //采用逆序遍历 防止删除元素时crash
            [mutableArray enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                QWAllTopicModel *oldModel = (QWAllTopicModel *)obj;
                if ([oldModel.msgfromname isEqualToString:dic[@"msgfromname"]]) {
                    [mutableArray removeObject:obj];
                }
            }];
        }
        
        [self.showInfoArr removeAllObjects];
        self.showInfoArr = mutableArray;
        [self.tableView reloadData];
    }
}


#pragma mark- 下拉刷新，上拉加载
-(void)loadPullDownRefreshDataInfo
{
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_FindAllTopic argument:[NSDictionary dictionary]];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        [self.showInfoArr removeAllObjects];
        NSArray *arr =(NSArray *)result;
        self->_showInfoArr =[QWAllTopicModel mj_objectArrayWithKeyValuesArray:arr];
        [self deleteShieldData];
        [self->_tableView reloadData];
        [self->_tableView judgeFooterState:arr.count<pageSize];
        [SWHUDUtil hideHudView];
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        [SWHUDUtil hideHudView];
    }];
}
-(void)loadPullUpRefreshDataInfo
{
//    NSDictionary *dic = @{
//        @"pageNum": @(_showInfoArr.count/pageSize+1),
//        @"pageSize": @(pageSize),
//    };
//    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_Health_List argument:dic];
//    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
//        NSArray *arr =result[@"list"];
//        [self->_showInfoArr addObjectsFromArray:[QWConsDetailModel mj_objectArrayWithKeyValuesArray:arr]];
//        [self->_tableView reloadData];
//        [self->_tableView judgeFooterState:arr.count<pageSize];
//        [SWHUDUtil hideHudView];
//    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
//
//        [SWHUDUtil hideHudView];
//    }];
}
-(NSMutableArray *)showInfoArr
{
    if (!_showInfoArr) {
        _showInfoArr=[[NSMutableArray alloc]init];
    }
    return _showInfoArr;
}



@end
