//
//  QWComplaintController.h
//  qwm
//
//  Created by kevin on 2023/5/10.
//

#import "QWBaseController.h"
#import "QWConversModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QWComplaintController : QWBaseController
@property (nonatomic, strong) QWAllTopicModel *topicModel;
@end

NS_ASSUME_NONNULL_END
