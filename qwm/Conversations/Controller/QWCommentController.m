//
//  QWCommentController.m
//  qwm
//
//  Created by kevin on 2023/4/18.
//

#import "QWCommentController.h"
#import "QWButtonView.h"
#import "QWAgreementView.h"

static NSString * const textViewPlaceholder = @"请输入回复内容";
static NSString * const huatiPlaceholder = @"请输入话题";
static NSInteger const largNum = 200;
static NSString * const tipStr = @"注意：系统将检查您的发布内容，对于发布的不良内容将被删除，并封禁作者账号!";

@interface QWCommentController ()<UITextViewDelegate,UIScrollViewDelegate>
@property (nonatomic,strong) UIScrollView *bgScroolView;
@property (nonatomic,strong) UIView *containerView;

@property (nonatomic,strong) UIView *titleView;
@property (nonatomic,strong) UIImageView *lineImageView;
@property (nonatomic,strong)UILabel *titleLabel;

@property (nonatomic,strong) UIView *cnterView;
@property (nonatomic,strong) UITextView *importInfoView;
@property (nonatomic,strong) UILabel *numLabel;

@property (nonatomic,strong) UILabel *tipLabel;
@property (nonatomic,strong) QWAgreementView *agreementView;
@property (nonatomic,strong) QWButtonView *buttonView;

@end

@implementation QWCommentController

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =  @"回复";
    
    [self bgScroolView];
    
    [self changeUI];
}

-(UIScrollView *)bgScroolView
{
    if (!_bgScroolView) {
        _bgScroolView =[[UIScrollView alloc]init];
        _bgScroolView.alwaysBounceVertical=YES;
        _bgScroolView.backgroundColor =[UIColor whiteColor];
        _bgScroolView.delegate = self;
//        _bgScroolView.contentSize=CGSizeMake(0, kScreenHeight-[UIDevice statusBarHeight]);
        [self.view addSubview:_bgScroolView];
        
        [_bgScroolView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.view);
        }];
        
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor clearColor];
        [_bgScroolView addSubview:_containerView];
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_bgScroolView);
            make.width.equalTo(_bgScroolView); // 需要设置宽度和scrollview宽度一样
        }];

        [self titleView];
        [self cnterView];
        [self tipLabel];
        [self agreementView];
        [self buttonView];
        
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
             make.bottom.equalTo(_buttonView.mas_bottom).offset(20);// 这里放最后一个view的底部
         }];
    }
    return _bgScroolView;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma mark - 标题
- (UIView *)titleView{
    if(!_titleView){
        _titleView = [[UIView alloc]init];
        _titleView.backgroundColor =[UIColor clearColor];
        [_containerView addSubview:_titleView];
        
        [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(18);
            make.right.mas_equalTo(-18);
            make.top.mas_equalTo(20.0);
            make.height.mas_greaterThanOrEqualTo(28.0);
        }];
        
        [self lineImageView];
        [self titleLabel];
        
    }
    return _titleView;
}

- (UIImageView *)lineImageView{
    if(!_lineImageView){
        _lineImageView = [[UIImageView alloc]init];
        _lineImageView.backgroundColor = Color_Main_Green;
        [_titleView addSubview:_lineImageView];
        
        [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_titleView);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(4);
            make.height.mas_equalTo(28);
        }];
    }
    return _lineImageView;
}

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel =[[UILabel alloc]init];
        _titleLabel.text = @"回复";
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.numberOfLines = 0;
        _titleLabel.font =[UIFont fontWithName:@"PingFangSC-Medium" size:20];
        _titleLabel.textColor =[UIColor blackColor];
        [_titleView addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_lineImageView).offset(15);
            make.right.equalTo(_titleView);
            make.top.equalTo(_titleView);
            make.bottom.equalTo(_titleView);
        }];
    }
    return _titleLabel;
}

#pragma mark - 输入
- (UIView *)cnterView{
    if(!_cnterView){
        _cnterView = [[UIView alloc]init];
        _cnterView.backgroundColor =[UIColor clearColor];
        [_containerView addSubview:_cnterView];
        
        [_cnterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(18);
            make.right.mas_equalTo(-18);
            make.top.mas_equalTo(_titleView.mas_bottom).offset(20.0);
            make.height.mas_equalTo(220.0);
        }];
        
        [self importInfoView];
        [self numLabel];
    }
    return _cnterView;
}

//输入框
-(UITextView *)importInfoView
{
    if (!_importInfoView) {
        _importInfoView =[[UITextView alloc]init];
        _importInfoView.text= textViewPlaceholder;
        _importInfoView.returnKeyType=UIReturnKeyDone;
        _importInfoView.delegate=self;
        _importInfoView.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _importInfoView.textColor=[UIColor colorFromHexString:@"#707070"];
        _importInfoView.backgroundColor=[UIColor clearColor];
//        _importInfoView.scrollEnabled = NO;
        
        _importInfoView.layer.borderWidth=1;
        _importInfoView.layer.borderColor=Color_Main_Green.CGColor;
        
        [_cnterView addSubview:_importInfoView];
        
        [_importInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_cnterView);
        }];
    }
    return _importInfoView;
}
#pragma mark- UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([textView.text isEqualToString:textViewPlaceholder]||[textView.text isEqualToString:huatiPlaceholder]) {
        textView.text=text;
    }
    
    if( [ @"\n" isEqualToString: text]){
        //你的响应方法
        [self.view endEditing:YES];
        return NO;
    }
    
    if (range.length + range.location > textView.text.length) {
        return NO;
    }
    
    NSUInteger length = textView.text.length + text.length - range.length;
    
    if (length<=largNum) {
        _numLabel.text=[NSString stringWithFormat:@"%ld/%ld",length,largNum];
        return YES;
    }
    
    return NO;
}

//字数显示
-(UILabel *)numLabel
{
    if (!_numLabel) {
        _numLabel=[[UILabel alloc]init];
        _numLabel.textAlignment=NSTextAlignmentRight;
        _numLabel.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _numLabel.text=[NSString stringWithFormat:@"%ld/%ld",_importInfoView.text.length,largNum];
        [_cnterView addSubview:_numLabel];
        
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_importInfoView);
            make.right.equalTo(_importInfoView).offset(-4);
        }];
    }
    return _numLabel;
}

#pragma mark - 警告提示
-(UILabel *)tipLabel
{
    if (!_tipLabel) {
        _tipLabel=[[UILabel alloc]init];
        _tipLabel.font=[UIFont fontWithName:@"PingFangSC-Medium" size:12];
        _tipLabel.numberOfLines = 0;
        _tipLabel.textColor = [UIColor redColor];
        _tipLabel.text = tipStr;
        [_containerView addSubview:_tipLabel];
        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_containerView).offset(18);
            make.right.equalTo(_containerView).offset(-18);
            make.top.mas_equalTo(_cnterView.mas_bottom).offset(2);
        }];
    }
    return _tipLabel;
}

#pragma mark - 协议
- (QWAgreementView *)agreementView{
    if(!_agreementView){
        _agreementView = [[QWAgreementView alloc]init];
        [_containerView addSubview:_agreementView];
        
        [_agreementView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_containerView).offset(18);
            make.right.equalTo(_containerView).offset(-18);
            make.top.mas_equalTo(_tipLabel.mas_bottom).offset(20);
        }];
    }
    return _agreementView;
}


#pragma mark - 提交
- (QWButtonView*)buttonView{
    if(!_buttonView){
        _buttonView= [[QWButtonView alloc]init];
        [_buttonView changeButtonTitle:@"提交"];
        __weak __typeof(self) weakSelf = self;
        _buttonView.buttonBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf commit];
        };
        
        [_containerView addSubview:_buttonView];
        
        [_buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(_agreementView.mas_bottom);
            make.height.mas_equalTo(95.0);
        }];
        
    }
    return _buttonView;
}

#pragma mark - 状态改变
- (void)changeUI{
    if(self.sendType == SendType_reply){
        self.title = @"回复";
        self.titleLabel.text = [NSString stringWithFormat:@"回复：%@",IsStringEmpty(self.replyModel.content)?@"":self.replyModel.content];

        [self.buttonView changeButtonTitle:@"回复"];
        _importInfoView.text= textViewPlaceholder;
    }else if (self.sendType == SendType_addNew){
        self.title = @"发布";
        self.titleLabel.text = @"话题";
        [self.buttonView changeButtonTitle:@"发布"];
        _importInfoView.text= huatiPlaceholder;
    }else if (self.sendType == SendType_addNew_juhe){
        self.title = @"发布";
        self.titleLabel.text = @"话题";
        [self.buttonView changeButtonTitle:@"发布"];
        _importInfoView.text= IsStringEmpty(self.juheTitleStr)?huatiPlaceholder:self.juheTitleStr;
        _numLabel.text=[NSString stringWithFormat:@"%ld/%ld",_importInfoView.text.length,largNum];
    }
}

#pragma mark - 提交-请求
- (void)commit{
    NSString *content = _importInfoView.text;
    if(IsStringEmpty(content) || [content isEqualToString: textViewPlaceholder] ||  [content isEqualToString: huatiPlaceholder]){
        [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"内容不能为空"];
        return;
    }
    
    if (!self.agreementView.isSelected){
        [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"请先勾选协议!"];
        return;
    }
      
    CGFloat value = ((arc4random() % 10) + 5)/10.0;
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(value * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SWHUDUtil hideHudView];
        UIAlertController *genderAlert =[UIAlertController alertControllerWithTitle:@"提示" message:@"您提交的内容正在审核中，待管理员审核通过后，予以展示！" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1 =[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];

        [genderAlert addAction:action1];
        [self presentViewController:genderAlert animated:YES completion:nil];
    });

    return;
    NSMutableDictionary *dic =[[NSMutableDictionary alloc] initWithDictionary:@{
        @"msgtype": @"topic",
        @"extprop": @"bbs",
        @"msgtoname": @"bbs",
        @"autoTest": @"autoLogin",
    }];

    [dic setValue:AppProfile.qaUserInfo.phone forKey:@"userName"];
    [dic setValue:AppProfile.qaUserInfo.password forKey:@"password"];
    [dic setValue:AppProfile.qaUserInfo.name forKey:@"msgfromname"];
    [dic setValue:AppProfile.qaUserInfo.id forKey:@"msgfromid"];
    NSString *timeStr = [[QWCommonMethod Instance] changeDateToString:[NSDate date] withFormatter:@"YYYY-MM-dd HH:mm:ss"];
    [dic setValue:timeStr forKey:@"chattime"];
    if (self.sendType == SendType_reply){
        [dic setValue:@(self.replyModel.id) forKey:@"msgtoid"];
        [dic setValue:@"answer" forKey:@"msgtype"];
//        [dic setValue:@"answer" forKey:@"msgtoname"];
    } else if(self.sendType == SendType_addNew || self.sendType == SendType_addNew_juhe ){
        [dic setValue:@"bbs" forKey:@"msgtoid"];
        [dic setValue:@"topic" forKey:@"msgtype"];
    }
    
    [dic setValue:content forKey:@"content"];
    
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    NSString *url = self.sendType == SendType_addNew_juhe ? URL_SaveNewTopic : URL_AddmsgBBSanswer;
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:url argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        if([result[@"message"] isEqualToString:@"ok"]){
            [SWHUDUtil hideHudViewWithSuccessMessage:@"提交成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (self.sendType == SendType_addNew_juhe){
                    [self turnToTabVCAtIndex:1];
                }else{
                    [self.navigationController popViewControllerAnimated:YES];
                }
            });

        }else {
            [SWHUDUtil hideHudViewWithFailureMessage:@"网络异常，请重试！"];
        }
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {

        [SWHUDUtil hideHudView];
    }];
    
}

@end
