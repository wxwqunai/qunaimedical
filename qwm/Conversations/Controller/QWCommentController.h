//
//  QWCommentController.h
//  qwm
//
//  Created by kevin on 2023/4/18.
//

#import "QWBaseController.h"
#import "QWConversModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, SendType)
{
    
    SendType_defalut = 0,
    SendType_reply,   //回复
    SendType_addNew,  //发布
    SendType_addNew_juhe, //聚合新闻发布
};

@interface QWCommentController : QWBaseController
@property (nonatomic,assign) SendType sendType;

@property (nonatomic,strong) QWAllTopicModel *replyModel;
@property (nonatomic,copy) NSString *juheTitleStr;
@end

NS_ASSUME_NONNULL_END
