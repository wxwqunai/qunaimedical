//
//  QWConsModel.h
//  qwm
//
//  Created by kevin on 2023/3/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWConsModel : NSObject

@end

@interface QWConsListModel : NSObject
@property (nonatomic,assign) NSInteger pageNum;
@property (nonatomic,assign) NSInteger pageSize;
@property (nonatomic,assign) NSInteger totalRecord;
@property (nonatomic,assign) NSInteger totalPage;
@property (nonatomic,assign) NSInteger startIndex;
@property (nonatomic,strong) NSArray *list;

@end

@interface QWConsDetailModel : NSObject
@property (nonatomic,copy) NSString *id;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *picUrl;
@end

NS_ASSUME_NONNULL_END
