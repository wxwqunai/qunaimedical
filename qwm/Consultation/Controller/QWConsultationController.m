//
//  QWConsultationController.m
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import "QWConsultationController.h"
#import "QWConsulationCell.h"
#import "QWConsDetalController.h"
#import "QWHealthyTipView.h"

static const int pageSize = 10;

@interface QWConsultationController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showInfoArr;
@property (nonatomic, copy) NSString *healthyTipStr;

@end

@implementation QWConsultationController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
    self.isHiddenLeftBarButtonItem = YES;
    
    if(self.showInfoArr.count == 0){
        [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
        [self loadPullDownRefreshDataInfo];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"资讯";

    [self tableView];
    
    [self loadPullDownRefreshDataInfo];
    [self loadJuHeHealthyTipData];
}

- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerNib:[UINib nibWithNibName:@"QWConsulationCell" bundle:nil] forCellReuseIdentifier:@"QWConsulationCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;

        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return  IsStringEmpty(_healthyTipStr)?0.0000001:50;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _showInfoArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWConsulationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWConsulationCell"];
    QWConsDetailModel *model = _showInfoArr[indexPath.row];
    cell.nameLabel.text = model.title;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    QWConsDetalController *consDetailVC =[[QWConsDetalController alloc]init];
    QWConsDetailModel *model = _showInfoArr[indexPath.row];
    consDetailVC.detailModel = model;
    consDetailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:consDetailVC animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(!IsStringEmpty(_healthyTipStr)){
        QWHealthyTipView *view = [[QWHealthyTipView alloc]init];
        view.tipStr = _healthyTipStr;
        return view;
    }
    return [UIView new];
}

#pragma mark- 下拉刷新，上拉加载
-(void)loadPullDownRefreshDataInfo
{
    NSDictionary *dic = @{
        @"pageNum": @1,
        @"pageSize": @(pageSize),
    };
    
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_Health_List argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        [self.showInfoArr removeAllObjects];
        NSArray *arr =result[@"list"];
        self->_showInfoArr =[QWConsDetailModel mj_objectArrayWithKeyValuesArray:arr];
        [self->_tableView reloadData];
        [self->_tableView judgeFooterState:arr.count<pageSize];
        [SWHUDUtil hideHudView];
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        [SWHUDUtil hideHudView];
    }];
}
-(void)loadPullUpRefreshDataInfo
{
    NSDictionary *dic = @{
        @"pageNum": @(_showInfoArr.count/pageSize+1),
        @"pageSize": @(pageSize),
    };
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_Health_List argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        NSArray *arr =result[@"list"];
        [self->_showInfoArr addObjectsFromArray:[QWConsDetailModel mj_objectArrayWithKeyValuesArray:arr]];
        [self->_tableView reloadData];
        [self->_tableView judgeFooterState:arr.count<pageSize];
        [SWHUDUtil hideHudView];
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {

        [SWHUDUtil hideHudView];
    }];
}
-(NSMutableArray *)showInfoArr
{
    if (!_showInfoArr) {
        _showInfoArr=[[NSMutableArray alloc]init];
    }
    return _showInfoArr;
}

- (void)loadJuHeHealthyTipData{
    NSString *dateStr = [[QWCommonMethod Instance] changeDateToString:[NSDate date] withFormatter:@"YYYY-MM-dd"];
    NSString *default_key = [NSString stringWithFormat:@"%@%@",URL_JUHE_Healthtip,dateStr];
    NSString *content = [[NSUserDefaults standardUserDefaults] objectForKey:default_key];
    if(IsStringEmpty(content)){
        HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_JUHE_Healthtip argument:[NSDictionary dictionary]];
        [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
            if([result[@"reason"] isEqualToString:@"success"] && [result[@"error_code"] intValue] ==0){
                NSDictionary *resDic =result[@"result"];
                [[NSUserDefaults standardUserDefaults] setObject:resDic[@"content"] forKey:default_key];
                self->_healthyTipStr = resDic[@"content"];
                [self->_tableView reloadData];
            }
        } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        }];
    }else {
        _healthyTipStr = content;
        [_tableView reloadData];
    }
}


@end
