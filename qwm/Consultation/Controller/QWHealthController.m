//
//  QWHealthController.m
//  qwm
//
//  Created by kevin on 2023/4/25.
//

#import "QWHealthController.h"
#import "QWHealthItemCell.h"
#import "QWConsDetalController.h"
#import "QWCollHealthyTipView.h"
#import "QWCommentController.h"
#import "QWSpeakController.h"

static const int pageSize = 10;

@interface QWHealthController ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *showInfoArr;
@property (nonatomic, copy) NSString *healthyTipStr;
@end

@implementation QWHealthController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
    self.isHiddenLeftBarButtonItem = YES;
    
    if(self.showInfoArr.count == 0){
        [self loadPullDownRefreshDataInfo];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"资讯";
    
    [self flowLayout];
    [self collectionView];
    
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    [self loadPullDownRefreshDataInfo];
    [self loadJuHeHealthyTipData];
}
#pragma mark - 对集合视图进行布局
-(UICollectionViewFlowLayout *)flowLayout
{
    if (!_flowLayout) {
        // 创建集合视图布局对象
        _flowLayout = [[UICollectionViewFlowLayout alloc] init];
        // 设置最小的左右间距
        _flowLayout.minimumInteritemSpacing = 10.0;
        // 设置最小的行间距（上下）
        _flowLayout.minimumLineSpacing = 10.0;
        _flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        // 设置边界范围
        _flowLayout.sectionInset = UIEdgeInsetsMake(10.0, 15.0, 10.0, 15.0);
        if (@available(iOS 9.0, *)) {
            _flowLayout.sectionHeadersPinToVisibleBounds = YES;
        }
    }
    return _flowLayout;
}
-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        _collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) collectionViewLayout:self.flowLayout];
        _collectionView.showsVerticalScrollIndicator=NO;
        _collectionView.showsHorizontalScrollIndicator=NO;
        _collectionView.backgroundColor=Color_TableView_Gray;
        [self.view addSubview:_collectionView];
        
        // 创建集合视图对象
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = YES;
        _collectionView.alwaysBounceVertical=YES;
        _collectionView.alwaysBounceHorizontal=NO;
        
        // 注册
        [_collectionView registerClass:[QWCollHealthyTipView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QWCollHealthyTipView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"QWHealthItemCell" bundle:nil] forCellWithReuseIdentifier:@"QWHealthItemCell"];
        
        [_collectionView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        [_collectionView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    return _collectionView;
}
#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width =(kScreenWidth-45)/2.0;
    CGFloat height = width*(9.0/16.0);
    CGSize showSize=CGSizeMake(width, height+40+35);
    return showSize;
}
// 设定页眉的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(kScreenWidth, IsStringEmpty(_healthyTipStr)?0:50);
}
#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.showInfoArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QWHealthItemCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"QWHealthItemCell" forIndexPath:indexPath];
    QWConsDetailModel *model = _showInfoArr[indexPath.row];
    model.picUrl = [self splicePicUrl:indexPath.row];
    [cell.topImgeView sd_setImageWithURL:[NSURL URLWithString:[self splicePicUrl:indexPath.row]]];
    cell.nameLabel.text = model.title;
    cell.taoLunBlock = ^{
        [self addDiscussTopic:model];
    };
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    QWConsDetalController *consDetailVC =[[QWConsDetalController alloc]init];
//    QWConsDetailModel *model = _showInfoArr[indexPath.row];
//    consDetailVC.detailModel = model;
//    consDetailVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:consDetailVC animated:YES];
    
    QWSpeakController *speakVC = [[QWSpeakController alloc]init];
    QWConsDetailModel *model = _showInfoArr[indexPath.row];
    speakVC.titleStr = model.title;
    speakVC.contentStr = model.content;
    speakVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:speakVC animated:YES];
}
#pragma mark- 头部
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqual:UICollectionElementKindSectionHeader] && !IsStringEmpty(_healthyTipStr))
    {
        QWCollHealthyTipView *headerView= [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QWCollHealthyTipView" forIndexPath:indexPath];
        headerView.tipStr = _healthyTipStr;
        return headerView;
    }
    return nil;
}

#pragma mark - 页面跳转-添加话题-请求
- (void)addDiscussTopic:(QWConsDetailModel *)model{
    if([AppProfile isLoginedAndShowLogintAlert]){
        QWCommentController *plVC =[[QWCommentController alloc]init];
        plVC.sendType = SendType_addNew_juhe;
        plVC.juheTitleStr = model.title;
        plVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:plVC animated:YES];
    }
}

#pragma mark- 下拉刷新，上拉加载
-(void)loadPullDownRefreshDataInfo
{
    NSDictionary *dic = @{
        @"pageNum": @1,
        @"pageSize": @(pageSize),
    };
    
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_Health_List argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        [self.showInfoArr removeAllObjects];
        NSArray *arr =result[@"list"];
        self->_showInfoArr =[QWConsDetailModel mj_objectArrayWithKeyValuesArray:arr];
        [self->_collectionView reloadData];
        [self->_collectionView judgeFooterState:arr.count<pageSize];
        [SWHUDUtil hideHudView];
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        [SWHUDUtil hideHudView];
    }];
}
-(void)loadPullUpRefreshDataInfo
{
    NSDictionary *dic = @{
        @"pageNum": @(_showInfoArr.count/pageSize+1),
        @"pageSize": @(pageSize),
    };
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_Health_List argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        NSArray *arr =result[@"list"];
        [self->_showInfoArr addObjectsFromArray:[QWConsDetailModel mj_objectArrayWithKeyValuesArray:arr]];
        [self->_collectionView reloadData];
        [self->_collectionView judgeFooterState:arr.count<pageSize];
        [SWHUDUtil hideHudView];
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {

        [SWHUDUtil hideHudView];
    }];
}
-(NSMutableArray *)showInfoArr
{
    if (!_showInfoArr) {
        _showInfoArr=[[NSMutableArray alloc]init];
    }
    return _showInfoArr;
}

- (void)loadJuHeHealthyTipData{
    NSString *dateStr = [[QWCommonMethod Instance] changeDateToString:[NSDate date] withFormatter:@"YYYY-MM-dd"];
    NSString *default_key = [NSString stringWithFormat:@"%@%@",URL_JUHE_Healthtip,dateStr];
    NSString *content = [[NSUserDefaults standardUserDefaults] objectForKey:default_key];
    if(IsStringEmpty(content)){
        HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_JUHE_Healthtip argument:[NSDictionary dictionary]];
        [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
            if([result[@"reason"] isEqualToString:@"success"] && [result[@"error_code"] intValue] ==0){
                NSDictionary *resDic =result[@"result"];
                [[NSUserDefaults standardUserDefaults] setObject:resDic[@"content"] forKey:default_key];
                self->_healthyTipStr = resDic[@"content"];
                [self->_collectionView reloadData];
            }
        } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        }];
    }else {
        _healthyTipStr = content;
        [_collectionView reloadData];
    }
}

#pragma mark - 拼接图片链接-随机
- (NSString *)splicePicUrl:(NSInteger)num{
    NSInteger picNum = num%20+1;
    NSString *numStr = [NSString stringWithFormat:@"a%02ld.png",picNum];
    NSString *urlStr = [NSString stringWithFormat:@"%@%@%@",QWM_BASE_API_URL,URL_Img_res,numStr];
    return urlStr;
}

@end
