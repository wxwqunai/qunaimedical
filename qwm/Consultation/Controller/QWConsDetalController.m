//
//  QWConsDetalController.m
//  qwm
//
//  Created by kevin on 2023/3/28.
//

#import "QWConsDetalController.h"

@interface QWConsDetalController ()<UIScrollViewDelegate>
@property (nonatomic,strong) UIScrollView *bgScroolView;
@property (nonatomic,strong) UIView *containerView;

@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UIImageView *picImageView;
@property (nonatomic,strong) UILabel *contentLabel;

@end

@implementation QWConsDetalController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"详情";
    
    [self bgScroolView];
}

-(UIScrollView *)bgScroolView
{
    if (!_bgScroolView) {
        _bgScroolView =[[UIScrollView alloc]init];
        _bgScroolView.alwaysBounceVertical=YES;
        _bgScroolView.backgroundColor =[UIColor whiteColor];
        _bgScroolView.delegate = self;
//        _bgScroolView.contentSize=CGSizeMake(0, kScreenHeight-[UIDevice statusBarHeight]);
        [self.view addSubview:_bgScroolView];
        
        [_bgScroolView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.view);
        }];
        
        //scrollview上覆盖一个view
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor clearColor];
        [_bgScroolView addSubview:_containerView];
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_bgScroolView);
            make.width.equalTo(_bgScroolView); // 需要设置宽度和scrollview宽度一样
        }];

        //覆盖view上添加
        [self titleLabel];
        [self picImageView];
        [self contentLabel];
        
        // 这里放最后一个view的底部
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
             make.bottom.equalTo(_contentLabel.mas_bottom).offset(20);
         }];
    }
    return _bgScroolView;
}

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel=[[UILabel alloc]init];
        _titleLabel.textColor = [UIColor black25PercentColor];
        _titleLabel.font=[UIFont boldSystemFontOfSize:18];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text=self.detailModel.title;
        [_containerView addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(25);
            make.right.mas_equalTo(-25);
            make.top.equalTo(_containerView).offset(20);
        }];
    }
    return _titleLabel;
}

- (UIImageView *)picImageView{
    if(!_picImageView){
        _picImageView = [[UIImageView alloc] initWithFrame:CGRectMake(18, 15, 20, 20)];
        [_picImageView sd_setImageWithURL:[NSURL URLWithString:self.detailModel.picUrl]];
        [_containerView addSubview:_picImageView];
        
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(18);
            make.right.mas_equalTo(-18);
            make.top.equalTo(_titleLabel.mas_bottom).offset(20);
            make.height.mas_equalTo(_containerView.mas_width).multipliedBy(9.0/16.0);
        }];
    }
    return _picImageView;
}

-(UILabel *)contentLabel
{
    if (!_contentLabel) {
        _contentLabel=[[UILabel alloc]init];
        _contentLabel.textColor = [UIColor black25PercentColor];
        _contentLabel.font=[UIFont systemFontOfSize:15];
        _contentLabel.numberOfLines = 0;
        _contentLabel.text=self.detailModel.content;
        [_containerView addSubview:_contentLabel];
        
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(18);
            make.right.mas_equalTo(-18);
            make.top.equalTo(_picImageView.mas_bottom).offset(20);
            make.bottom.mas_equalTo(-20);
        }];
    }
    return _contentLabel;
}

@end
