//
//  QWConsDetalController.h
//  qwm
//
//  Created by kevin on 2023/3/28.
//

#import "QWBaseController.h"
#import "QWConsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWConsDetalController : QWBaseController
@property (nonatomic, strong) QWConsDetailModel *detailModel;
@end

NS_ASSUME_NONNULL_END
