//
//  QWHealthyTipView.h
//  qwm
//
//  Created by kevin on 2023/4/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWHealthyTipView : UIView
@property (nonatomic,copy) NSString * tipStr;
@end

NS_ASSUME_NONNULL_END
