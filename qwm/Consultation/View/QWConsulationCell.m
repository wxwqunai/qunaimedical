//
//  QWConsulationCell.m
//  qwm
//
//  Created by kevin on 2023/3/28.
//

#import "QWConsulationCell.h"

@implementation QWConsulationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.nameLabel.text = @"";
    
    [self.tlButton setTitleColor:[UIColor colorFromHexString:@"#515151"] forState:UIControlStateNormal];
    [self.plButton setTitleColor:[UIColor colorFromHexString:@"#515151"] forState:UIControlStateNormal];
    [self.gzButton setTitleColor:[UIColor colorFromHexString:@"#515151"] forState:UIControlStateNormal];
}
- (IBAction)gzButtonClick:(id)sender {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
