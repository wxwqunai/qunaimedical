//
//  QWCollHealthyTipView.m
//  qwm
//
//  Created by kevin on 2023/4/26.
//

#import "QWCollHealthyTipView.h"
#import "LMJHorizontalScrollText.h"

@interface QWCollHealthyTipView()
@property (nonatomic,strong) UIImageView *iconImageView;
@property (nonatomic,strong) LMJHorizontalScrollText *scrollText;

@end

@implementation QWCollHealthyTipView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor whiteColor];
        [self iconImageView];
        [self scrollText];
    }
    return self;
}

- (UIImageView *)iconImageView{
    if(!_iconImageView){
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(18, 15, 20, 20)];
        _iconImageView.image =[UIImage imageNamed:@"qw_healthyTip.png"];
        [self addSubview:_iconImageView];
    }
    return _iconImageView;
}

- (LMJHorizontalScrollText *)scrollText{
    if(!_scrollText){
        _scrollText = [[LMJHorizontalScrollText alloc]initWithFrame:CGRectMake(46, 10, kScreenWidth-46-18, 30)];
        _scrollText.textFont=[UIFont fontWithName:@"PingFangSC-Light" size:16];
        _scrollText.textColor = [UIColor colorFromHexString:@"#3232CD"];
        _scrollText.moveMode = LMJTextScrollWandering;
        [self addSubview:_scrollText];
    }
    return _scrollText;
}

- (void)setTipStr:(NSString *)tipStr
{
    _tipStr = tipStr;
    _scrollText.text=tipStr;
}

@end

