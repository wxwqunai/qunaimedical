//
//  QWCollHealthyTipView.h
//  qwm
//
//  Created by kevin on 2023/4/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface QWCollHealthyTipView : UICollectionReusableView
@property (nonatomic,copy) NSString * tipStr;
@end

NS_ASSUME_NONNULL_END
