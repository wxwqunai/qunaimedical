//
//  QWHealthItemCell.h
//  qwm
//
//  Created by kevin on 2023/4/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^TaoLunBlock)(void);

@interface QWHealthItemCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *topImgeView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *tlButton;

@property (nonatomic,copy)TaoLunBlock taoLunBlock;

@end

NS_ASSUME_NONNULL_END
