//
//  QWHealthItemCell.m
//  qwm
//
//  Created by kevin on 2023/4/26.
//

#import "QWHealthItemCell.h"

@implementation QWHealthItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bgView.layer.cornerRadius = 4.0;
    self.bgView.layer.masksToBounds = YES;
    
    [self.tlButton setTitleColor:[UIColor colorFromHexString:@"#515151"] forState:UIControlStateNormal];

}
- (IBAction)tlButtonClick:(id)sender {
    if(self.taoLunBlock)self.taoLunBlock();
}

@end
