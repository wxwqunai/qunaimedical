//
//  QWConsulationCell.h
//  qwm
//
//  Created by kevin on 2023/3/28.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWConsulationCell : QWBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *tlButton;
@property (weak, nonatomic) IBOutlet UIButton *plButton;
@property (weak, nonatomic) IBOutlet UIButton *gzButton;

@end

NS_ASSUME_NONNULL_END
